PGDMP     2    
                t            QuanLyQuanAn    9.5.1    9.5.1 <    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    16406    QuanLyQuanAn    DATABASE     �   CREATE DATABASE "QuanLyQuanAn" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Vietnamese_Vietnam.1258' LC_CTYPE = 'Vietnamese_Vietnam.1258';
    DROP DATABASE "QuanLyQuanAn";
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    6            �           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    6                        3079    12355    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16420 
   Cetificate    TABLE        CREATE TABLE "Cetificate" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "Name" character varying(255)
);
     DROP TABLE public."Cetificate";
       public         postgres    false    6            �            1259    16556 	   DinerItem    TABLE     �   CREATE TABLE "DinerItem" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "Name" character varying(255),
    "Price" real,
    "OutOfOrder" boolean,
    "Catelogy" integer
);
    DROP TABLE public."DinerItem";
       public         postgres    false    6            �            1259    16564    DinerItemCatelogy    TABLE     �   CREATE TABLE "DinerItemCatelogy" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "Name" character varying(255),
    "MenuDisplayOrder" integer
);
 '   DROP TABLE public."DinerItemCatelogy";
       public         postgres    false    6            �            1259    16577    DinerSaleOrderItem    TABLE     �   CREATE TABLE "DinerSaleOrderItem" (
    "ID" integer NOT NULL,
    "Item" integer,
    "Quality" real,
    "Price" real,
    "Status" character varying(255),
    "DinerSaleOrder" integer
);
 (   DROP TABLE public."DinerSaleOrderItem";
       public         postgres    false    6            �            1259    16465    DinerSalesOrder    TABLE     �   CREATE TABLE "DinerSalesOrder" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "IssueDate" date,
    "IssueBy" integer,
    "WorkingShift" integer,
    "IsPaid" boolean,
    "DinerTable" integer,
    "NumOfGuest" integer
);
 %   DROP TABLE public."DinerSalesOrder";
       public         postgres    false    6            �            1259    16470    DinnerTable    TABLE     �   CREATE TABLE "DinnerTable" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "Name" character varying(255),
    "MaximumGuest" integer
);
 !   DROP TABLE public."DinnerTable";
       public         postgres    false    6            �            1259    16541    EmployWorkingShift    TABLE     u   CREATE TABLE "EmployWorkingShift" (
    "ID" integer NOT NULL,
    "Employee" integer,
    "WorkingShift" integer
);
 (   DROP TABLE public."EmployWorkingShift";
       public         postgres    false    6            �            1259    16407    Employee    TABLE     �  CREATE TABLE "Employee" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "FullName" character varying(255),
    "UserName" character varying(255),
    "Password" character varying(10),
    "Birthday" date,
    "Position" character varying(255),
    "Phone" character varying(15),
    "Address" character varying(60),
    "IDNumber" character varying(255),
    "EmployeeStatus" character varying(255)
);
    DROP TABLE public."Employee";
       public         postgres    false    6            �            1259    16415    EmployeeCertificate    TABLE     t   CREATE TABLE "EmployeeCertificate" (
    "ID" integer NOT NULL,
    "Employee" integer,
    "Cetificate" integer
);
 )   DROP TABLE public."EmployeeCertificate";
       public         postgres    false    6            �            1259    16442    EmployeeToolsAndSupplies    TABLE        CREATE TABLE "EmployeeToolsAndSupplies" (
    "ID" integer NOT NULL,
    "Employee" integer,
    "ToolsAndSupplies" integer
);
 .   DROP TABLE public."EmployeeToolsAndSupplies";
       public         postgres    false    6            �            1259    16496    EmployeeWorkingShedule    TABLE     U  CREATE TABLE "EmployeeWorkingShedule" (
    "ID" integer NOT NULL,
    "Employee" integer,
    "Date" date,
    "FromTime" date,
    "ToTime" date,
    "LoginTime" date,
    "LogoutTime" date,
    "OvertimeInMinutes" integer,
    "Salary" real,
    "Status" character varying(255),
    "WorkingShift" integer,
    "LateInMinutes" integer
);
 ,   DROP TABLE public."EmployeeWorkingShedule";
       public         postgres    false    6            �            1259    16447    ToolsAndSupplies    TABLE     �   CREATE TABLE "ToolsAndSupplies" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "Name" character varying
);
 &   DROP TABLE public."ToolsAndSupplies";
       public         postgres    false    6            �            1259    16483    WorkingShift    TABLE     �   CREATE TABLE "WorkingShift" (
    "ID" integer NOT NULL,
    "Code" character varying(255),
    "Name" character varying(255),
    "FromTime" date,
    "ToTime" date,
    "Salary" real
);
 "   DROP TABLE public."WorkingShift";
       public         postgres    false    6            �          0    16420 
   Cetificate 
   TABLE DATA               5   COPY "Cetificate" ("ID", "Code", "Name") FROM stdin;
    public       postgres    false    183   �L       �          0    16556 	   DinerItem 
   TABLE DATA               W   COPY "DinerItem" ("ID", "Code", "Name", "Price", "OutOfOrder", "Catelogy") FROM stdin;
    public       postgres    false    191    M       �          0    16564    DinerItemCatelogy 
   TABLE DATA               P   COPY "DinerItemCatelogy" ("ID", "Code", "Name", "MenuDisplayOrder") FROM stdin;
    public       postgres    false    192   M       �          0    16577    DinerSaleOrderItem 
   TABLE DATA               e   COPY "DinerSaleOrderItem" ("ID", "Item", "Quality", "Price", "Status", "DinerSaleOrder") FROM stdin;
    public       postgres    false    193   :M       �          0    16465    DinerSalesOrder 
   TABLE DATA               �   COPY "DinerSalesOrder" ("ID", "Code", "IssueDate", "IssueBy", "WorkingShift", "IsPaid", "DinerTable", "NumOfGuest") FROM stdin;
    public       postgres    false    186   WM       �          0    16470    DinnerTable 
   TABLE DATA               F   COPY "DinnerTable" ("ID", "Code", "Name", "MaximumGuest") FROM stdin;
    public       postgres    false    187   tM       �          0    16541    EmployWorkingShift 
   TABLE DATA               I   COPY "EmployWorkingShift" ("ID", "Employee", "WorkingShift") FROM stdin;
    public       postgres    false    190   �M       �          0    16407    Employee 
   TABLE DATA               �   COPY "Employee" ("ID", "Code", "FullName", "UserName", "Password", "Birthday", "Position", "Phone", "Address", "IDNumber", "EmployeeStatus") FROM stdin;
    public       postgres    false    181   �M       �          0    16415    EmployeeCertificate 
   TABLE DATA               H   COPY "EmployeeCertificate" ("ID", "Employee", "Cetificate") FROM stdin;
    public       postgres    false    182   �M       �          0    16442    EmployeeToolsAndSupplies 
   TABLE DATA               S   COPY "EmployeeToolsAndSupplies" ("ID", "Employee", "ToolsAndSupplies") FROM stdin;
    public       postgres    false    184   �M       �          0    16496    EmployeeWorkingShedule 
   TABLE DATA               �   COPY "EmployeeWorkingShedule" ("ID", "Employee", "Date", "FromTime", "ToTime", "LoginTime", "LogoutTime", "OvertimeInMinutes", "Salary", "Status", "WorkingShift", "LateInMinutes") FROM stdin;
    public       postgres    false    189   N       �          0    16447    ToolsAndSupplies 
   TABLE DATA               ;   COPY "ToolsAndSupplies" ("ID", "Code", "Name") FROM stdin;
    public       postgres    false    185   "N       �          0    16483    WorkingShift 
   TABLE DATA               W   COPY "WorkingShift" ("ID", "Code", "Name", "FromTime", "ToTime", "Salary") FROM stdin;
    public       postgres    false    188   ?N       �           2606    16431    Cetificate_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "Cetificate"
    ADD CONSTRAINT "Cetificate_pkey" PRIMARY KEY ("ID");
 H   ALTER TABLE ONLY public."Cetificate" DROP CONSTRAINT "Cetificate_pkey";
       public         postgres    false    183    183            	           2606    16571    DinerItemCatelogy_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY "DinerItemCatelogy"
    ADD CONSTRAINT "DinerItemCatelogy_pkey" PRIMARY KEY ("ID");
 V   ALTER TABLE ONLY public."DinerItemCatelogy" DROP CONSTRAINT "DinerItemCatelogy_pkey";
       public         postgres    false    192    192                       2606    16563    DinerItem_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY "DinerItem"
    ADD CONSTRAINT "DinerItem_pkey" PRIMARY KEY ("ID");
 F   ALTER TABLE ONLY public."DinerItem" DROP CONSTRAINT "DinerItem_pkey";
       public         postgres    false    191    191                       2606    16581    DinerSaleOrderItem_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY "DinerSaleOrderItem"
    ADD CONSTRAINT "DinerSaleOrderItem_pkey" PRIMARY KEY ("ID");
 X   ALTER TABLE ONLY public."DinerSaleOrderItem" DROP CONSTRAINT "DinerSaleOrderItem_pkey";
       public         postgres    false    193    193            �           2606    16469    DinerSalesOrder_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "DinerSalesOrder"
    ADD CONSTRAINT "DinerSalesOrder_pkey" PRIMARY KEY ("ID");
 R   ALTER TABLE ONLY public."DinerSalesOrder" DROP CONSTRAINT "DinerSalesOrder_pkey";
       public         postgres    false    186    186            �           2606    16477    DinnerTable_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY "DinnerTable"
    ADD CONSTRAINT "DinnerTable_pkey" PRIMARY KEY ("ID");
 J   ALTER TABLE ONLY public."DinnerTable" DROP CONSTRAINT "DinnerTable_pkey";
       public         postgres    false    187    187                       2606    16545    EmployWorkingShift_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY "EmployWorkingShift"
    ADD CONSTRAINT "EmployWorkingShift_pkey" PRIMARY KEY ("ID");
 X   ALTER TABLE ONLY public."EmployWorkingShift" DROP CONSTRAINT "EmployWorkingShift_pkey";
       public         postgres    false    190    190            �           2606    16419    EmployeeCertificate_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY "EmployeeCertificate"
    ADD CONSTRAINT "EmployeeCertificate_pkey" PRIMARY KEY ("ID");
 Z   ALTER TABLE ONLY public."EmployeeCertificate" DROP CONSTRAINT "EmployeeCertificate_pkey";
       public         postgres    false    182    182            �           2606    16446    EmployeeToolsAndSupplies_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY "EmployeeToolsAndSupplies"
    ADD CONSTRAINT "EmployeeToolsAndSupplies_pkey" PRIMARY KEY ("ID");
 d   ALTER TABLE ONLY public."EmployeeToolsAndSupplies" DROP CONSTRAINT "EmployeeToolsAndSupplies_pkey";
       public         postgres    false    184    184                       2606    16500    EmployeeWorkingShedule_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY "EmployeeWorkingShedule"
    ADD CONSTRAINT "EmployeeWorkingShedule_pkey" PRIMARY KEY ("ID");
 `   ALTER TABLE ONLY public."EmployeeWorkingShedule" DROP CONSTRAINT "EmployeeWorkingShedule_pkey";
       public         postgres    false    189    189            �           2606    16414    Employee_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "Employee"
    ADD CONSTRAINT "Employee_pkey" PRIMARY KEY ("ID");
 D   ALTER TABLE ONLY public."Employee" DROP CONSTRAINT "Employee_pkey";
       public         postgres    false    181    181            �           2606    16454    ToolsAndSupplies_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY "ToolsAndSupplies"
    ADD CONSTRAINT "ToolsAndSupplies_pkey" PRIMARY KEY ("ID");
 T   ALTER TABLE ONLY public."ToolsAndSupplies" DROP CONSTRAINT "ToolsAndSupplies_pkey";
       public         postgres    false    185    185                       2606    16490    WorkingShift_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY "WorkingShift"
    ADD CONSTRAINT "WorkingShift_pkey" PRIMARY KEY ("ID");
 L   ALTER TABLE ONLY public."WorkingShift" DROP CONSTRAINT "WorkingShift_pkey";
       public         postgres    false    188    188                       2606    16572    DinerItem_Catelogy_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "DinerItem"
    ADD CONSTRAINT "DinerItem_Catelogy_fkey" FOREIGN KEY ("Catelogy") REFERENCES "DinerItemCatelogy"("ID");
 O   ALTER TABLE ONLY public."DinerItem" DROP CONSTRAINT "DinerItem_Catelogy_fkey";
       public       postgres    false    192    191    2057                       2606    16582 &   DinerSaleOrderItem_DinerSaleOrder_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "DinerSaleOrderItem"
    ADD CONSTRAINT "DinerSaleOrderItem_DinerSaleOrder_fkey" FOREIGN KEY ("DinerSaleOrder") REFERENCES "DinerSalesOrder"("ID");
 g   ALTER TABLE ONLY public."DinerSaleOrderItem" DROP CONSTRAINT "DinerSaleOrderItem_DinerSaleOrder_fkey";
       public       postgres    false    193    186    2045                       2606    16587    DinerSaleOrderItem_Item_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "DinerSaleOrderItem"
    ADD CONSTRAINT "DinerSaleOrderItem_Item_fkey" FOREIGN KEY ("Item") REFERENCES "DinerItem"("ID");
 ]   ALTER TABLE ONLY public."DinerSaleOrderItem" DROP CONSTRAINT "DinerSaleOrderItem_Item_fkey";
       public       postgres    false    2055    193    191                       2606    16478    DinerSalesOrder_DinerTable_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "DinerSalesOrder"
    ADD CONSTRAINT "DinerSalesOrder_DinerTable_fkey" FOREIGN KEY ("DinerTable") REFERENCES "DinnerTable"("ID");
 ]   ALTER TABLE ONLY public."DinerSalesOrder" DROP CONSTRAINT "DinerSalesOrder_DinerTable_fkey";
       public       postgres    false    187    2047    186                       2606    16491 !   DinerSalesOrder_WorkingShift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "DinerSalesOrder"
    ADD CONSTRAINT "DinerSalesOrder_WorkingShift_fkey" FOREIGN KEY ("WorkingShift") REFERENCES "WorkingShift"("ID");
 _   ALTER TABLE ONLY public."DinerSalesOrder" DROP CONSTRAINT "DinerSalesOrder_WorkingShift_fkey";
       public       postgres    false    188    2049    186                       2606    16546     EmployWorkingShift_Employee_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployWorkingShift"
    ADD CONSTRAINT "EmployWorkingShift_Employee_fkey" FOREIGN KEY ("Employee") REFERENCES "Employee"("ID");
 a   ALTER TABLE ONLY public."EmployWorkingShift" DROP CONSTRAINT "EmployWorkingShift_Employee_fkey";
       public       postgres    false    190    181    2035                       2606    16551 $   EmployWorkingShift_WorkingShift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployWorkingShift"
    ADD CONSTRAINT "EmployWorkingShift_WorkingShift_fkey" FOREIGN KEY ("WorkingShift") REFERENCES "WorkingShift"("ID");
 e   ALTER TABLE ONLY public."EmployWorkingShift" DROP CONSTRAINT "EmployWorkingShift_WorkingShift_fkey";
       public       postgres    false    188    2049    190                       2606    16437 #   EmployeeCertificate_Cetificate_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployeeCertificate"
    ADD CONSTRAINT "EmployeeCertificate_Cetificate_fkey" FOREIGN KEY ("Cetificate") REFERENCES "Cetificate"("ID");
 e   ALTER TABLE ONLY public."EmployeeCertificate" DROP CONSTRAINT "EmployeeCertificate_Cetificate_fkey";
       public       postgres    false    2039    183    182                       2606    16432 !   EmployeeCertificate_Employee_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployeeCertificate"
    ADD CONSTRAINT "EmployeeCertificate_Employee_fkey" FOREIGN KEY ("Employee") REFERENCES "Employee"("ID");
 c   ALTER TABLE ONLY public."EmployeeCertificate" DROP CONSTRAINT "EmployeeCertificate_Employee_fkey";
       public       postgres    false    2035    181    182                       2606    16455 &   EmployeeToolsAndSupplies_Employee_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployeeToolsAndSupplies"
    ADD CONSTRAINT "EmployeeToolsAndSupplies_Employee_fkey" FOREIGN KEY ("Employee") REFERENCES "Employee"("ID");
 m   ALTER TABLE ONLY public."EmployeeToolsAndSupplies" DROP CONSTRAINT "EmployeeToolsAndSupplies_Employee_fkey";
       public       postgres    false    181    2035    184                       2606    16460 .   EmployeeToolsAndSupplies_ToolsAndSupplies_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployeeToolsAndSupplies"
    ADD CONSTRAINT "EmployeeToolsAndSupplies_ToolsAndSupplies_fkey" FOREIGN KEY ("ToolsAndSupplies") REFERENCES "ToolsAndSupplies"("ID");
 u   ALTER TABLE ONLY public."EmployeeToolsAndSupplies" DROP CONSTRAINT "EmployeeToolsAndSupplies_ToolsAndSupplies_fkey";
       public       postgres    false    184    185    2043                       2606    16536 $   EmployeeWorkingShedule_Employee_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployeeWorkingShedule"
    ADD CONSTRAINT "EmployeeWorkingShedule_Employee_fkey" FOREIGN KEY ("Employee") REFERENCES "Employee"("ID");
 i   ALTER TABLE ONLY public."EmployeeWorkingShedule" DROP CONSTRAINT "EmployeeWorkingShedule_Employee_fkey";
       public       postgres    false    189    181    2035                       2606    16531 (   EmployeeWorkingShedule_WorkingShift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY "EmployeeWorkingShedule"
    ADD CONSTRAINT "EmployeeWorkingShedule_WorkingShift_fkey" FOREIGN KEY ("WorkingShift") REFERENCES "WorkingShift"("ID");
 m   ALTER TABLE ONLY public."EmployeeWorkingShedule" DROP CONSTRAINT "EmployeeWorkingShedule_WorkingShift_fkey";
       public       postgres    false    189    2049    188            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �     