
    create table Certificate (
        id integer not null auto_increment,
        code varchar(255) not null,
        name varchar(255),
        primary key (id)
    );

    create table DinerItem (
        id integer not null auto_increment,
        code varchar(255) not null,
        name varchar(255),
        outOfOrder bit,
        price double precision,
        category_id integer,
        primary key (id)
    );

    create table DinerItemCategory (
        id integer not null auto_increment,
        code varchar(255) not null,
        menuDisplayOrder integer,
        name varchar(255),
        primary key (id)
    );

    create table DinerSalesOrder (
        id integer not null auto_increment,
        cancelReason varchar(255),
        code varchar(255) not null,
        isPaid bit,
        issueDate datetime,
        numOfGuest bit,
        total double precision,
        dinerTable_id integer,
        issueBy_id integer,
        workingShift_id integer,
        primary key (id)
    );

    create table DinerSalesOrderItem (
        id integer not null auto_increment,
        cancelReason varchar(255),
        issueDate datetime,
        price double precision,
        quantity double precision,
        status integer,
        item_id integer,
        salesOrder_id integer,
        primary key (id)
    );

    create table DinerTable (
        id integer not null auto_increment,
        code varchar(255) not null,
        maximumGuest integer,
        name varchar(255),
        primary key (id)
    );

    create table Employee (
        id integer not null auto_increment,
        address varchar(255),
        birthday varchar(255),
        code varchar(255),
        fullName varchar(255),
        idNumber varchar(255),
        password varchar(255),
        phone varchar(255),
        status varchar(255),
        userName varchar(255),
        manageBy_id integer,
        primary key (id)
    );

    create table EmployeeCertificate (
        id integer not null auto_increment,
        certificate_id integer,
        employee_id integer,
        primary key (id)
    );

    create table EmployeeToolsAndSupplies (
        id integer not null auto_increment,
        returned bit,
        employee_id integer,
        toolsAndSupplies_id integer,
        primary key (id)
    );

    create table EmployeeWorkingSchedule (
        id integer not null auto_increment,
        date datetime,
        fromTime datetime,
        lateInMinutes integer,
        loginTime datetime,
        logoutEarlyInMinutes integer,
        logoutTime datetime,
        overtimeInMinutes integer,
        salary double precision,
        status integer,
        toTime datetime,
        employee_id integer,
        workingShift_id integer,
        primary key (id)
    );

    create table EmployeeWorkingShift (
        id integer not null auto_increment,
        employee_id integer,
        workingShift_id integer,
        primary key (id)
    );

    create table Resource (
        name varchar(255) not null,
        primary key (name)
    );

    create table Role (
        name varchar(255) not null,
        primary key (name)
    );

    create table ToolsAndSupplies (
        id integer not null auto_increment,
        code varchar(255) not null,
        name varchar(255),
        primary key (id)
    );

    create table WorkingShift (
        id integer not null auto_increment,
        code varchar(255) not null,
        fromTime datetime,
        name varchar(255),
        toTime datetime,
        primary key (id)
    );

    create table roleresourcenm (
        role varchar(255) not null,
        resource varchar(255) not null,
        primary key (resource, role)
    );

    create table rolerolenm (
        childrole varchar(255) not null,
        parentrole varchar(255) not null,
        primary key (parentrole, childrole)
    );

    create table userrolenm (
        role varchar(255) not null,
        user integer not null,
        primary key (user, role)
    );

    alter table Certificate 
        add constraint UK_rb2ctphcnfs9lgoup29y4963p  unique (code);

    alter table DinerItem 
        add constraint UK_fi3dld5rinfhvfj9x50uwmx0y  unique (code);

    alter table DinerItemCategory 
        add constraint UK_7pmfv7dhnhi1p07v6rrca9ov8  unique (code);

    alter table DinerSalesOrder 
        add constraint UK_k1uydv8q0pefqufb5suaacaak  unique (code);

    alter table DinerTable 
        add constraint UK_9062fspg7dtqwsw6y1oeetgag  unique (code);

    alter table ToolsAndSupplies 
        add constraint UK_novb0v8ysrnsh1wxf5yfsdu6u  unique (code);

    alter table WorkingShift 
        add constraint UK_axbimmf6rkap8nxtedy8oqmpt  unique (code);

    alter table DinerItem 
        add constraint FK_2n135xm7ium5r20fkvusl3uuu 
        foreign key (category_id) 
        references DinerItemCategory (id);

    alter table DinerSalesOrder 
        add constraint FK_r8kmu1j614vqn7c41unqbqvgl 
        foreign key (dinerTable_id) 
        references DinerTable (id);

    alter table DinerSalesOrder 
        add constraint FK_6r2v1kk7a2xmk693urn9xglqg 
        foreign key (issueBy_id) 
        references Employee (id);

    alter table DinerSalesOrder 
        add constraint FK_kgg78sitmhbp3dg02ue3tod54 
        foreign key (workingShift_id) 
        references WorkingShift (id);

    alter table DinerSalesOrderItem 
        add constraint FK_oodmrqy2sb74bhmr3rv0bjr0w 
        foreign key (item_id) 
        references DinerItem (id);

    alter table DinerSalesOrderItem 
        add constraint FK_k087r9uyav7g293q8eurup485 
        foreign key (salesOrder_id) 
        references DinerSalesOrder (id);

    alter table Employee 
        add constraint FK_70lu67ngqofe2nefs0ec5ntsa 
        foreign key (manageBy_id) 
        references Employee (id);

    alter table EmployeeCertificate 
        add constraint FK_nlgxdkqd1eo6p6ys7xq0q9rj1 
        foreign key (certificate_id) 
        references Certificate (id);

    alter table EmployeeCertificate 
        add constraint FK_o9e10ga7jw2tymuw7j89s35s1 
        foreign key (employee_id) 
        references Employee (id);

    alter table EmployeeToolsAndSupplies 
        add constraint FK_bna4el9rcagnw2hmumtppo461 
        foreign key (employee_id) 
        references Employee (id);

    alter table EmployeeToolsAndSupplies 
        add constraint FK_nnsfocbvf4l6mw5xxtmpcif0r 
        foreign key (toolsAndSupplies_id) 
        references ToolsAndSupplies (id);

    alter table EmployeeWorkingSchedule 
        add constraint FK_s7l3pj2k52l6ecunomylml3kn 
        foreign key (employee_id) 
        references Employee (id);

    alter table EmployeeWorkingSchedule 
        add constraint FK_3t0leowh80h1htho0msbxy4y8 
        foreign key (workingShift_id) 
        references WorkingShift (id);

    alter table EmployeeWorkingShift 
        add constraint FK_75cy5lb7w3nojx196qd0nfe6o 
        foreign key (employee_id) 
        references Employee (id);

    alter table EmployeeWorkingShift 
        add constraint FK_hvbfax5i3tii0h0rg9i1kxuk3 
        foreign key (workingShift_id) 
        references WorkingShift (id);

    alter table roleresourcenm 
        add constraint FK_2nmu0urlnlwb6or7pv4l2b37d 
        foreign key (resource) 
        references Resource (name);

    alter table roleresourcenm 
        add constraint FK_g7kpun69oorakk361y3xe454f 
        foreign key (role) 
        references Role (name);

    alter table rolerolenm 
        add constraint FK_2ktivh97rgpc1mqhw7vjsg4g8 
        foreign key (parentrole) 
        references Role (name);

    alter table rolerolenm 
        add constraint FK_2vftx3kns1if099i5g439ia95 
        foreign key (childrole) 
        references Role (name);

    alter table userrolenm 
        add constraint FK_16evy309uhevxrg5da2w2qow8 
        foreign key (user) 
        references Employee (id);

    alter table userrolenm 
        add constraint FK_5b4vuo6yxfybqc0cf2jjll5kf 
        foreign key (role) 
        references Role (name);
