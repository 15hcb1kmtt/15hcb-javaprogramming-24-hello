package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.WorkingShift;
import com.xdev.dal.JPADAO;

public class WorkingShiftDAO extends JPADAO<WorkingShift, Integer> {

	public WorkingShiftDAO() {
		super(WorkingShift.class);
	}

}
