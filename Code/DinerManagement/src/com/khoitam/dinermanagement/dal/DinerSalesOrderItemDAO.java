package com.khoitam.dinermanagement.dal;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.khoitam.dinermanagement.entities.DinerSalesOrder;
import com.khoitam.dinermanagement.entities.DinerSalesOrderItem;
import com.xdev.dal.JPADAO;

public class DinerSalesOrderItemDAO extends JPADAO<DinerSalesOrderItem, Integer> {

	public DinerSalesOrderItemDAO() {
		super(DinerSalesOrderItem.class);
	}
	
	public List<DinerSalesOrderItem> GetItemFromSalesOrder(DinerSalesOrder salesOrder) {		
		Criteria items = this
				.buildHibernateCriteriaQuery(DinerSalesOrderItem.class)
				.add(Restrictions.eq("salesOrder.id", salesOrder.getId()));
		return items.list();
	}
}
