package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.Role;
import com.xdev.dal.JPADAO;

public class RoleDAO extends JPADAO<Role, Integer> {

	public RoleDAO() {
		super(Role.class);
	}

}
