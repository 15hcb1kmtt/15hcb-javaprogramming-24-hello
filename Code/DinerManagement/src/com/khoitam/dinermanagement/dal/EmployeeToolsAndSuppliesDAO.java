package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.EmployeeToolsAndSupplies;
import com.xdev.dal.JPADAO;

public class EmployeeToolsAndSuppliesDAO extends JPADAO<EmployeeToolsAndSupplies, Integer> {

	public EmployeeToolsAndSuppliesDAO() {
		super(EmployeeToolsAndSupplies.class);
	}

}
