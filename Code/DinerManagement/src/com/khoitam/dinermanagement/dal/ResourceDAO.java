package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.Resource;
import com.xdev.dal.JPADAO;

public class ResourceDAO extends JPADAO<Resource, Integer> {

	public ResourceDAO() {
		super(Resource.class);
	}

}
