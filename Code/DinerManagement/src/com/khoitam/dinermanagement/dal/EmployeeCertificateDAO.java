package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.EmployeeCertificate;
import com.xdev.dal.JPADAO;

public class EmployeeCertificateDAO extends JPADAO<EmployeeCertificate, Integer> {

	public EmployeeCertificateDAO() {
		super(EmployeeCertificate.class);
	}

}
