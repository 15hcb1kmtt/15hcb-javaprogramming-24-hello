package com.khoitam.dinermanagement.dal;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.khoitam.dinermanagement.entities.DinerSalesOrder;
import com.xdev.dal.JPADAO;

public class DinerSalesOrderDAO extends JPADAO<DinerSalesOrder, Integer> {

	public DinerSalesOrderDAO() {
		super(DinerSalesOrder.class);
	}
	
	public List<DinerSalesOrder> GetInProgressSalesOrder() {		
		Criteria inProgressSalesOrderQuery = this
				.buildHibernateCriteriaQuery(DinerSalesOrder.class)
				.add(Restrictions.eq("isPaid", false))
				.add(Restrictions.eq("isCancelled", false));
		return inProgressSalesOrderQuery.list();
	}
	
	public List<DinerSalesOrder> GetCompletedSalesOrder(Date fromDate, Date toDate) {		
		Criteria inProgressSalesOrderQuery = this
				.buildHibernateCriteriaQuery(DinerSalesOrder.class)
				.add(Restrictions.ge("issueDate", fromDate))
				.add(Restrictions.le("issueDate", toDate))
				.add(Restrictions.eq("isPaid", true));
		return inProgressSalesOrderQuery.list();
	}

	public DinerSalesOrder GetInProgressSalesOrderForTable(int tableId) {		
		Criteria inProgressSalesOrderQuery = this
				.buildHibernateCriteriaQuery(DinerSalesOrder.class)
				.add(Restrictions.eq("isPaid", false))
				.add(Restrictions.eq("isCancelled", false));
		inProgressSalesOrderQuery.createCriteria("dinerTable").add(Restrictions.eq("id", tableId));
		return (DinerSalesOrder)inProgressSalesOrderQuery.uniqueResult();
	}
}
