package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.business.enumeration.EmployeeWorkingScheduleStatusEnum;
import com.khoitam.dinermanagement.entities.EmployeeWorkingSchedule;
import com.xdev.dal.JPADAO;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class EmployeeWorkingScheduleDAO extends JPADAO<EmployeeWorkingSchedule, Integer> {

	public EmployeeWorkingScheduleDAO() {
		super(EmployeeWorkingSchedule.class);
	}

	public EmployeeWorkingSchedule GetScheduleOfDateRange(int employeeId, Date fromDate, Date toDate) {		
		Criteria crit = this.buildHibernateCriteriaQuery(EmployeeWorkingSchedule.class)
		.add(Restrictions.between("date", fromDate, toDate));
		crit.createCriteria("employee").add(Restrictions.eq("id", employeeId));
		return (EmployeeWorkingSchedule)crit.uniqueResult();
	}
	
	public List<EmployeeWorkingSchedule> GetApproveDataForEmployee(int employeeId) {		
		Criteria crit = this.buildHibernateCriteriaQuery(EmployeeWorkingSchedule.class)
		.add(Restrictions.eq("status", EmployeeWorkingScheduleStatusEnum.AskForAbsent));
		crit.createCriteria("employee").add(Restrictions.eq("id", employeeId));
		return crit.list();
	}
}
