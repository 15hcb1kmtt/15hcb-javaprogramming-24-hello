package com.khoitam.dinermanagement.dal;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.khoitam.dinermanagement.entities.DinerSalesOrder;
import com.khoitam.dinermanagement.entities.Employee;
import com.xdev.dal.JPADAO;

public class EmployeeDAO extends JPADAO<Employee, Integer> {

	public EmployeeDAO() {
		super(Employee.class);
	}
	
	public Employee GetEmployeeByUsername(String username) {
		Criteria resultQuery = this
				.buildHibernateCriteriaQuery(Employee.class)
				.add(Restrictions.eq("userName", username));
		return (Employee)resultQuery.uniqueResult();
	}

}
