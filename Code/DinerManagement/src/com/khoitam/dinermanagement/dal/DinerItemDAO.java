package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.DinerItem;
import com.xdev.dal.JPADAO;

public class DinerItemDAO extends JPADAO<DinerItem, Integer> {

	public DinerItemDAO() {
		super(DinerItem.class);
	}

}
