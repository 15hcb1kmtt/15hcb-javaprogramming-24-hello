package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.EmployeeWorkingShift;
import com.xdev.dal.JPADAO;

public class EmployeeWorkingShiftDAO extends JPADAO<EmployeeWorkingShift, Integer> {

	public EmployeeWorkingShiftDAO() {
		super(EmployeeWorkingShift.class);
	}

}
