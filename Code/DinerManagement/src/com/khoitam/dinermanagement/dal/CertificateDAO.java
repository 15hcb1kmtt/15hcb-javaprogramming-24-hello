package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.Certificate;
import com.xdev.dal.JPADAO;

public class CertificateDAO extends JPADAO<Certificate, Integer> {

	public CertificateDAO() {
		super(Certificate.class);
	}

}
