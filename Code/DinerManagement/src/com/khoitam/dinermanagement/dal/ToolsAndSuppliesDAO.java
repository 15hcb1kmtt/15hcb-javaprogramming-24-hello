package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.ToolsAndSupplies;
import com.xdev.dal.JPADAO;

public class ToolsAndSuppliesDAO extends JPADAO<ToolsAndSupplies, Integer> {

	public ToolsAndSuppliesDAO() {
		super(ToolsAndSupplies.class);
	}

}
