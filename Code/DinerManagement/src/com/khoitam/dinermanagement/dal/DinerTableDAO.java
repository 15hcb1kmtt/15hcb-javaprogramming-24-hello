package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.DinerTable;
import com.xdev.dal.JPADAO;

public class DinerTableDAO extends JPADAO<DinerTable, Integer> {

	public DinerTableDAO() {
		super(DinerTable.class);
	}

}
