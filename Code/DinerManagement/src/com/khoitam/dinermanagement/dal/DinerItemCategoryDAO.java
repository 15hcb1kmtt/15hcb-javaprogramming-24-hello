package com.khoitam.dinermanagement.dal;

import com.khoitam.dinermanagement.entities.DinerItemCategory;
import com.xdev.dal.JPADAO;

public class DinerItemCategoryDAO extends JPADAO<DinerItemCategory, Integer> {

	public DinerItemCategoryDAO() {
		super(DinerItemCategory.class);
	}

}
