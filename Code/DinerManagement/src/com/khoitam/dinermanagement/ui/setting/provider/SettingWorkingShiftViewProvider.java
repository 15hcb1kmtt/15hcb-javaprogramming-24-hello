package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.WorkingShiftDAO;
import com.khoitam.dinermanagement.entities.WorkingShift;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingWorkingShiftDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingWorkingShiftViewProvider extends SettingViewProviderBase<WorkingShift> {

	@Override
	public Class<WorkingShift> GetClassType() {
		return WorkingShift.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingWorkingShiftDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		WorkingShift editObjectCert = (WorkingShift)editObject;
		return new SettingWorkingShiftDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("name", "Tên");
		map.put("fromTime", "Giờ bắt đầu");
		map.put("toTime", "Giờ kết thúc");
		return map;
	}

	@Override
	public XdevTable<WorkingShift> GetTable() {
		return new XdevTable<WorkingShift>();
	}

	@Override
	public Class GetDAOClassType() {
		return WorkingShiftDAO.class;
	}
}
