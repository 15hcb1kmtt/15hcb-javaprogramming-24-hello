package com.khoitam.dinermanagement.ui.setting.detailview;

import com.khoitam.dinermanagement.dal.DinerItemCategoryDAO;
import com.khoitam.dinermanagement.entities.DinerItem;
import com.khoitam.dinermanagement.entities.DinerItemCategory;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.dal.DAOs;
import com.xdev.ui.XdevAbsoluteLayout;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevCheckBox;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;

public class SettingDinerItemDetailView extends XdevView {

	/**
	 * 
	 */
	public SettingDinerItemDetailView() {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(new DinerItem());
	}
	public SettingDinerItemDetailView(DinerItem dineritem) {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(dineritem);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdReset_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.discard();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.close();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdSave_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.save();
		// Lay popup window chua man hinh Certificate
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.setData(this.fieldGroup.getItemDataSource().getBean());
			// Dong popup
			popupWindow.close();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.absoluteLayout = new XdevAbsoluteLayout();
		this.form = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
		this.label2 = new XdevLabel();
		this.textField2 = new XdevTextField();
		this.label3 = new XdevLabel();
		this.textField3 = new XdevTextField();
		this.label4 = new XdevLabel();
		this.checkBox = new XdevCheckBox();
		this.label6 = new XdevLabel();
		this.checkBox2 = new XdevCheckBox();
		this.label5 = new XdevLabel();
		this.comboBox = new XdevComboBox<>();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.cmdReset = new XdevButton();
		this.cmdSave = new XdevButton();
		this.fieldGroup = new XdevFieldGroup<>(DinerItem.class);
	
		this.label.setValue("Mã");
		this.textField.setTabIndex(1);
		this.label2.setValue("Tên");
		this.textField2.setTabIndex(2);
		this.label3.setValue("Giá");
		this.textField3.setTabIndex(3);
		this.label4.setValue("Hết món");
		this.checkBox.setCaption("");
		this.checkBox.setTabIndex(4);
		this.label6.setValue("Có thể nấu");
		this.checkBox2.setCaption("");
		this.label5.setValue("Loại");
		this.comboBox.setTabIndex(5);
		this.comboBox.setContainerDataSource(DinerItemCategory.class, DAOs.get(DinerItemCategoryDAO.class).findAll());
		this.comboBox.setItemCaptionPropertyId("code");
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.cmdReset.setCaption("Huỷ");
		this.cmdReset.setTabIndex(6);
		this.cmdSave.setCaption("Lưu");
		this.fieldGroup.bind(this.textField, "code");
		this.fieldGroup.bind(this.textField2, "name");
		this.fieldGroup.bind(this.textField3, "price");
		this.fieldGroup.bind(this.checkBox, "outOfOrder");
		this.fieldGroup.bind(this.comboBox, "category");
		this.fieldGroup.bind(this.checkBox2, "isCookable");
	
		this.cmdReset.setSizeUndefined();
		this.horizontalLayout.addComponent(this.cmdReset);
		this.horizontalLayout.setComponentAlignment(this.cmdReset, Alignment.MIDDLE_LEFT);
		CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.form.setColumns(2);
		this.form.setRows(8);
		this.label.setSizeUndefined();
		this.form.addComponent(this.label, 0, 0);
		this.textField.setWidth(100, Unit.PERCENTAGE);
		this.textField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField, 1, 0);
		this.label2.setSizeUndefined();
		this.form.addComponent(this.label2, 0, 1);
		this.textField2.setWidth(100, Unit.PERCENTAGE);
		this.textField2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField2, 1, 1);
		this.label3.setSizeUndefined();
		this.form.addComponent(this.label3, 0, 2);
		this.textField3.setWidth(100, Unit.PERCENTAGE);
		this.textField3.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField3, 1, 2);
		this.label4.setSizeUndefined();
		this.form.addComponent(this.label4, 0, 3);
		this.checkBox.setWidth(100, Unit.PERCENTAGE);
		this.checkBox.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.checkBox, 1, 3);
		this.label6.setSizeUndefined();
		this.form.addComponent(this.label6, 0, 4);
		this.checkBox2.setWidth(100, Unit.PERCENTAGE);
		this.checkBox2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.checkBox2, 1, 4);
		this.label5.setSizeUndefined();
		this.form.addComponent(this.label5, 0, 5);
		this.comboBox.setWidth(100, Unit.PERCENTAGE);
		this.comboBox.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.comboBox, 1, 5);
		this.horizontalLayout.setSizeUndefined();
		this.form.addComponent(this.horizontalLayout, 0, 6);
		this.cmdSave.setSizeUndefined();
		this.form.addComponent(this.cmdSave, 1, 6);
		this.form.setComponentAlignment(this.cmdSave, Alignment.MIDDLE_RIGHT);
		this.form.setColumnExpandRatio(1, 1.0F);
		CustomComponent form_vSpacer = new CustomComponent();
		form_vSpacer.setSizeFull();
		this.form.addComponent(form_vSpacer, 0, 7, 1, 7);
		this.form.setRowExpandRatio(7, 1.0F);
		this.form.setWidth(100, Unit.PERCENTAGE);
		this.form.setHeight(400, Unit.PERCENTAGE);
		this.absoluteLayout.addComponent(this.form, "left:0px; top:0px");
		this.absoluteLayout.setSizeFull();
		this.setContent(this.absoluteLayout);
		this.setSizeFull();
	
		cmdReset.addClickListener(event -> this.cmdReset_buttonClick(event));
		cmdSave.addClickListener(event -> this.cmdSave_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevAbsoluteLayout absoluteLayout;
	private XdevButton cmdReset, cmdSave;
	private XdevComboBox<DinerItemCategory> comboBox;
	private XdevGridLayout form;
	private XdevCheckBox checkBox, checkBox2;
	private XdevFieldGroup<DinerItem> fieldGroup;
	private XdevLabel label, label2, label3, label4, label6, label5;
	private XdevHorizontalLayout horizontalLayout;
	private XdevTextField textField, textField2, textField3; // </generated-code>


}
