package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.CertificateDAO;
import com.khoitam.dinermanagement.dal.DinerItemCategoryDAO;
import com.khoitam.dinermanagement.entities.Certificate;
import com.khoitam.dinermanagement.entities.DinerItemCategory;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingCertificateDetailView;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingDinerItemCategoryDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingDinerItemCategoryViewProvider extends SettingViewProviderBase<DinerItemCategory> {

	@Override
	public Class<DinerItemCategory> GetClassType() {
		return DinerItemCategory.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingDinerItemCategoryDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		DinerItemCategory editObjectCert = (DinerItemCategory)editObject;
		return new SettingDinerItemCategoryDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("name", "Tên");
		map.put("menuDisplayOrder", "Thứ tự hiển thị");
		return map;
	}

	@Override
	public XdevTable<DinerItemCategory> GetTable() {
		return new XdevTable<DinerItemCategory>();
	}

	@Override
	public Class GetDAOClassType() {
		return DinerItemCategoryDAO.class;
	}
}
