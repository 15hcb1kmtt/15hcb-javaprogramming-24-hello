package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.EmployeeDAO;
import com.khoitam.dinermanagement.entities.Employee;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingEmployeeDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingEmployeeViewProvider extends SettingViewProviderBase<Employee> {

	@Override
	public Class<Employee> GetClassType() {
		return Employee.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingEmployeeDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		Employee editObjectCert = (Employee)editObject;
		return new SettingEmployeeDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("fullName", "Họ tên");
		map.put("userName", "Tên đăng nhập");
		map.put("birthday", "Ngày sinh");
		map.put("phone", "Điện thoại");
		map.put("address", "Địa chỉ");
		map.put("idNumber", "CMND");
		map.put("status", "Trạng thái");
		map.put("manageBy", "Quản lý bởi");
		return map;
	}

	@Override
	public XdevTable<Employee> GetTable() {
		return new XdevTable<Employee>();
	}

	@Override
	public Class GetDAOClassType() {
		return EmployeeDAO.class;
	}
}
