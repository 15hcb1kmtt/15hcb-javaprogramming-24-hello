package com.khoitam.dinermanagement.ui.setting.detailview;

import com.khoitam.dinermanagement.entities.DinerTable;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;

public class SettingDinerTableDetailView extends XdevView {

	/**
	 * Construction to create new table
	 */
	public SettingDinerTableDetailView() {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(new DinerTable());
	}
	
	// Flag to check whether it's edit or create new
	private boolean isEdit = false;
	
	// Construction to edit table
	public SettingDinerTableDetailView(DinerTable dinerTable) {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(dinerTable);
		// Is edit
		isEdit = true;
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdSave_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.save();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			if (!isEdit) {
				popupWindow.setData(this.fieldGroup.getItemDataSource().getBean());
			}
			popupWindow.close();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdCancel}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdCancel_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.discard();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.close();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.form = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
		this.label2 = new XdevLabel();
		this.textField2 = new XdevTextField();
		this.label3 = new XdevLabel();
		this.textField3 = new XdevTextField();
		this.cmdCancel = new XdevButton();
		this.cmdSave = new XdevButton();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.fieldGroup = new XdevFieldGroup<>(DinerTable.class);
	
		this.label.setValue("Mã");
		this.textField.setTabIndex(1);
		this.label2.setValue("Tên");
		this.textField2.setTabIndex(2);
		this.label3.setValue("Khách tối đa");
		this.textField3.setTabIndex(3);
		this.cmdCancel.setCaption("Hủy");
		this.cmdSave.setCaption("Lưu");
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.fieldGroup.bind(this.textField, "code");
		this.fieldGroup.bind(this.textField2, "name");
		this.fieldGroup.bind(this.textField3, "maximumGuest");
	
		this.form.setColumns(2);
		this.form.setRows(6);
		this.label.setSizeUndefined();
		this.form.addComponent(this.label, 0, 0);
		this.textField.setWidth(100, Unit.PERCENTAGE);
		this.textField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField, 1, 0);
		this.label2.setSizeUndefined();
		this.form.addComponent(this.label2, 0, 1);
		this.textField2.setWidth(100, Unit.PERCENTAGE);
		this.textField2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField2, 1, 1);
		this.label3.setSizeUndefined();
		this.form.addComponent(this.label3, 0, 2);
		this.textField3.setWidth(100, Unit.PERCENTAGE);
		this.textField3.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField3, 1, 2);
		this.cmdCancel.setSizeUndefined();
		this.form.addComponent(this.cmdCancel, 0, 3);
		this.form.setComponentAlignment(this.cmdCancel, Alignment.MIDDLE_CENTER);
		this.cmdSave.setSizeUndefined();
		this.form.addComponent(this.cmdSave, 1, 3);
		this.form.setComponentAlignment(this.cmdSave, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout.setSizeUndefined();
		this.form.addComponent(this.horizontalLayout, 0, 4);
		this.form.setColumnExpandRatio(1, 1.0F);
		CustomComponent form_vSpacer = new CustomComponent();
		form_vSpacer.setSizeFull();
		this.form.addComponent(form_vSpacer, 0, 5, 1, 5);
		this.form.setRowExpandRatio(5, 1.0F);
		this.form.setSizeFull();
		this.setContent(this.form);
		this.setSizeFull();
	
		cmdCancel.addClickListener(event -> this.cmdCancel_buttonClick(event));
		cmdSave.addClickListener(event -> this.cmdSave_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton cmdCancel, cmdSave;
	private XdevGridLayout form;
	private XdevLabel label, label2, label3;
	private XdevHorizontalLayout horizontalLayout;
	private XdevFieldGroup<DinerTable> fieldGroup;
	private XdevTextField textField, textField2, textField3; // </generated-code>


}
