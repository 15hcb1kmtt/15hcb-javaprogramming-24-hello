package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.Map;

import com.xdev.ui.XdevView;

public interface ISettingViewProvider {
	XdevView GetEditDetailView(Object editObject);
	XdevView GetNewDetailView();
	Map<String, String> GetListViewColumns();
}
