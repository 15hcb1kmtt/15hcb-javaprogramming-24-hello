package com.khoitam.dinermanagement.ui.setting.detailview;

import com.khoitam.dinermanagement.entities.WorkingShift;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevInlineDateField;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;

public class SettingWorkingShiftDetailView extends XdevView {

	/**
	 * 
	 */
	public SettingWorkingShiftDetailView() {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(new WorkingShift());
	}
	
	public SettingWorkingShiftDetailView(WorkingShift workingshift) {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(workingshift);
	}


	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdReset_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.discard();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.close();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdSave_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.save();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.setData(this.fieldGroup.getItemDataSource().getBean());
			// Dong popup
			popupWindow.close();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.form = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
		this.label2 = new XdevLabel();
		this.textField2 = new XdevTextField();
		this.label3 = new XdevLabel();
		this.inlineDateFieldBegin = new XdevInlineDateField();
		this.label4 = new XdevLabel();
		this.inlineDateFieldEnd = new XdevInlineDateField();
		this.label5 = new XdevLabel();
		this.textField3 = new XdevTextField();
		this.cmdReset = new XdevButton();
		this.cmdSave = new XdevButton();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.fieldGroup = new XdevFieldGroup<>(WorkingShift.class);
	
		this.label.setValue("Mã");
		this.textField.setTabIndex(1);
		this.label2.setValue("Tên");
		this.textField2.setTabIndex(2);
		this.label3.setValue("Thời gian bắt đầu");
		this.inlineDateFieldBegin.setStyleName("time-only");
		this.inlineDateFieldBegin.setResolution(Resolution.MINUTE);
		this.label4.setValue("Thời gian kết thúc");
		this.inlineDateFieldEnd.setStyleName("time-only");
		this.inlineDateFieldEnd.setResolution(Resolution.MINUTE);
		this.label5.setValue("Lương");
		this.textField3.setTabIndex(5);
		this.cmdReset.setCaption("Huỷ");
		this.cmdSave.setCaption("Lưu");
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.fieldGroup.bind(this.textField, "code");
		this.fieldGroup.bind(this.textField2, "name");
		this.fieldGroup.bind(this.inlineDateFieldBegin, "fromTime");
		this.fieldGroup.bind(this.inlineDateFieldEnd, "toTime");
		this.fieldGroup.bind(this.textField3, "salary");
	
		this.form.setColumns(2);
		this.form.setRows(8);
		this.label.setSizeUndefined();
		this.form.addComponent(this.label, 0, 0);
		this.textField.setWidth(100, Unit.PERCENTAGE);
		this.textField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField, 1, 0);
		this.label2.setSizeUndefined();
		this.form.addComponent(this.label2, 0, 1);
		this.textField2.setWidth(100, Unit.PERCENTAGE);
		this.textField2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField2, 1, 1);
		this.label3.setSizeUndefined();
		this.form.addComponent(this.label3, 0, 2);
		this.inlineDateFieldBegin.setSizeUndefined();
		this.form.addComponent(this.inlineDateFieldBegin, 1, 2);
		this.label4.setSizeUndefined();
		this.form.addComponent(this.label4, 0, 3);
		this.inlineDateFieldEnd.setSizeUndefined();
		this.form.addComponent(this.inlineDateFieldEnd, 1, 3);
		this.label5.setSizeUndefined();
		this.form.addComponent(this.label5, 0, 4);
		this.textField3.setWidth(100, Unit.PERCENTAGE);
		this.textField3.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField3, 1, 4);
		this.cmdReset.setSizeUndefined();
		this.form.addComponent(this.cmdReset, 0, 5);
		this.form.setComponentAlignment(this.cmdReset, Alignment.MIDDLE_LEFT);
		this.cmdSave.setSizeUndefined();
		this.form.addComponent(this.cmdSave, 1, 5);
		this.form.setComponentAlignment(this.cmdSave, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout.setSizeUndefined();
		this.form.addComponent(this.horizontalLayout, 0, 6, 1, 6);
		this.form.setColumnExpandRatio(1, 1.0F);
		CustomComponent form_vSpacer = new CustomComponent();
		form_vSpacer.setSizeFull();
		this.form.addComponent(form_vSpacer, 0, 7, 1, 7);
		this.form.setRowExpandRatio(7, 1.0F);
		this.form.setSizeFull();
		this.setContent(this.form);
		this.setSizeFull();
	
		cmdReset.addClickListener(event -> this.cmdReset_buttonClick(event));
		cmdSave.addClickListener(event -> this.cmdSave_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton cmdReset, cmdSave;
	private XdevGridLayout form;
	private XdevFieldGroup<WorkingShift> fieldGroup;
	private XdevLabel label, label2, label3, label4, label5;
	private XdevHorizontalLayout horizontalLayout;
	private XdevInlineDateField inlineDateFieldBegin, inlineDateFieldEnd;
	private XdevTextField textField, textField2, textField3; // </generated-code>


}
