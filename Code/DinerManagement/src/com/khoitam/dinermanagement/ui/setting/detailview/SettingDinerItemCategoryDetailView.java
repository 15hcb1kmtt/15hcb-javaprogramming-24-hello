package com.khoitam.dinermanagement.ui.setting.detailview;

import com.khoitam.dinermanagement.entities.DinerItemCategory;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.ui.XdevAbsoluteLayout;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;

public class SettingDinerItemCategoryDetailView extends XdevView {

	/**
	 * 
	 */
	public SettingDinerItemCategoryDetailView() {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(new DinerItemCategory());
	}
	
	public SettingDinerItemCategoryDetailView(DinerItemCategory dineritemcategory) {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(dineritemcategory);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdReset_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.discard();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.close();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdSave_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.save();
		// Lay popup window chua man hinh Certificate
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.setData(this.fieldGroup.getItemDataSource().getBean());
			// Dong popup
			popupWindow.close();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.absoluteLayout = new XdevAbsoluteLayout();
		this.form = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
		this.label2 = new XdevLabel();
		this.textField2 = new XdevTextField();
		this.label3 = new XdevLabel();
		this.textField3 = new XdevTextField();
		this.cmdReset = new XdevButton();
		this.cmdSave = new XdevButton();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.fieldGroup = new XdevFieldGroup<>(DinerItemCategory.class);
	
		this.label.setValue("Mã");
		this.textField.setTabIndex(1);
		this.label2.setValue("Tên");
		this.textField2.setTabIndex(2);
		this.label3.setValue("Thứ tự hiển thị");
		this.textField3.setTabIndex(3);
		this.cmdReset.setCaption("Huỷ");
		this.cmdSave.setCaption("Lưu");
		this.horizontalLayout.setMargin(new MarginInfo(false));
		this.fieldGroup.bind(this.textField, "code");
		this.fieldGroup.bind(this.textField2, "name");
		this.fieldGroup.bind(this.textField3, "menuDisplayOrder");
	
		this.form.setColumns(2);
		this.form.setRows(6);
		this.label.setSizeUndefined();
		this.form.addComponent(this.label, 0, 0);
		this.textField.setWidth(100, Unit.PERCENTAGE);
		this.textField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField, 1, 0);
		this.label2.setSizeUndefined();
		this.form.addComponent(this.label2, 0, 1);
		this.textField2.setWidth(100, Unit.PERCENTAGE);
		this.textField2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField2, 1, 1);
		this.label3.setSizeUndefined();
		this.form.addComponent(this.label3, 0, 2);
		this.textField3.setWidth(100, Unit.PERCENTAGE);
		this.textField3.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField3, 1, 2);
		this.cmdReset.setSizeUndefined();
		this.form.addComponent(this.cmdReset, 0, 3);
		this.form.setComponentAlignment(this.cmdReset, Alignment.MIDDLE_LEFT);
		this.cmdSave.setSizeUndefined();
		this.form.addComponent(this.cmdSave, 1, 3);
		this.form.setComponentAlignment(this.cmdSave, Alignment.MIDDLE_RIGHT);
		this.horizontalLayout.setSizeUndefined();
		this.form.addComponent(this.horizontalLayout, 0, 4, 1, 4);
		this.form.setColumnExpandRatio(1, 1.0F);
		CustomComponent form_vSpacer = new CustomComponent();
		form_vSpacer.setSizeFull();
		this.form.addComponent(form_vSpacer, 0, 5, 1, 5);
		this.form.setRowExpandRatio(5, 1.0F);
		this.form.setWidth(100, Unit.PERCENTAGE);
		this.form.setHeight(400, Unit.PERCENTAGE);
		this.absoluteLayout.addComponent(this.form, "left:0px; top:0px");
		this.absoluteLayout.setSizeFull();
		this.setContent(this.absoluteLayout);
		this.setSizeFull();
	
		cmdReset.addClickListener(event -> this.cmdReset_buttonClick(event));
		cmdSave.addClickListener(event -> this.cmdSave_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevTextField textField, textField2, textField3;
	private XdevLabel label, label2, label3;
	private XdevHorizontalLayout horizontalLayout;
	private XdevFieldGroup<DinerItemCategory> fieldGroup;
	private XdevGridLayout form;
	private XdevAbsoluteLayout absoluteLayout;
	private XdevButton cmdReset, cmdSave; // </generated-code>


}
