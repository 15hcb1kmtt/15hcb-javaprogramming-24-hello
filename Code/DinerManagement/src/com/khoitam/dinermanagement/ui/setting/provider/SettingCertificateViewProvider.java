package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.CertificateDAO;
import com.khoitam.dinermanagement.entities.Certificate;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingCertificateDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingCertificateViewProvider extends SettingViewProviderBase<Certificate> {

	@Override
	public Class<Certificate> GetClassType() {
		return Certificate.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingCertificateDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		Certificate editObjectCert = (Certificate)editObject;
		return new SettingCertificateDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("name", "Tên");
		return map;
	}

	@Override
	public XdevTable<Certificate> GetTable() {
		return new XdevTable<Certificate>();
	}

	@Override
	public Class GetDAOClassType() {
		return CertificateDAO.class;
	}
}
