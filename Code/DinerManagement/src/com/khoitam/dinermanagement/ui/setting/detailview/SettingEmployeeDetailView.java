package com.khoitam.dinermanagement.ui.setting.detailview;

import java.util.Arrays;
import java.util.List;

import com.khoitam.dinermanagement.business.enumeration.EmployeeStatusEnum;
import com.khoitam.dinermanagement.dal.EmployeeDAO;
import com.khoitam.dinermanagement.dal.EmployeeWorkingShiftDAO;
import com.khoitam.dinermanagement.dal.RoleDAO;
import com.khoitam.dinermanagement.dal.WorkingShiftDAO;
import com.khoitam.dinermanagement.entities.Employee;
import com.khoitam.dinermanagement.entities.EmployeeWorkingShift;
import com.khoitam.dinermanagement.entities.Role;
import com.khoitam.dinermanagement.entities.WorkingShift;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.dal.DAOs;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPopupDateField;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.listselect.XdevTwinColSelect;
import com.xdev.ui.entitycomponent.table.XdevTable;
import com.xdev.ui.util.NestedProperty;
import com.xdev.ui.util.wizard.XDEV;

public class SettingEmployeeDetailView extends XdevView {

	/**
	 * 
	 */
	public SettingEmployeeDetailView() {
		super();
		this.initUI();
		PopulateStatusCombobox();
		Employee data = new Employee();
		this.fieldGroup.setItemDataSource(data);
		BindEmployeeWorkingShift(data);
	}
	public SettingEmployeeDetailView(Employee employee) {
		super();
		this.initUI();
		PopulateStatusCombobox();
		this.fieldGroup.setItemDataSource(employee);
		BindEmployeeWorkingShift(employee);
	}
	
	public void PopulateStatusCombobox() {
		List<EmployeeStatusEnum> emStatusEmum = Arrays.asList(EmployeeStatusEnum.values());
		comboBox.addItems(emStatusEmum);
	}
	
	public void BindEmployeeWorkingShift(Employee employee) {
		EmployeeWorkingShiftDAO newShiftDao = new EmployeeWorkingShiftDAO();
		this.tableWorkingShift.setContainerDataSource(
				EmployeeWorkingShift.class,
				employee.getEmployeeWorkingShifts(), 
				NestedProperty.of("workingShift.name", String.class));
		this.tableWorkingShift.setVisibleColumns("workingShift.name", "salary");
		this.tableWorkingShift.setColumnHeader("workingShift.name", "Tên ca");
		this.tableWorkingShift.setColumnHeader("salary", "Lương");
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAddShift}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonAddShift_buttonClick(Button.ClickEvent event) {
		BeanItem<WorkingShift> selectedShiftBean = comboBoxShiftSelection.getSelectedItem();
		if (selectedShiftBean != null) {
			EmployeeWorkingShiftDAO newShiftDao = new EmployeeWorkingShiftDAO();
			EmployeeWorkingShift newShift = new EmployeeWorkingShift();
			newShift.setEmployee(this.fieldGroup.getItemDataSource().getBean());
			newShift.setWorkingShift(selectedShiftBean.getBean());
			newShiftDao.save(newShift);
			this.fieldGroup.save();
			EmployeeDAO emDao = new EmployeeDAO();
			emDao.refresh(fieldGroup.getItemDataSource().getBean());
			BindEmployeeWorkingShift(this.fieldGroup.getItemDataSource().getBean());
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdSave_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.save();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.setData(this.fieldGroup.getItemDataSource().getBean());
			popupWindow.close();
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdReset_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.discard();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.close();
		}
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonEditShift}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonEditShift_buttonClick(Button.ClickEvent event) {
		fieldGroupShift.save();
	}
	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonDeleteShift}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonDeleteShift_buttonClick(Button.ClickEvent event) {
		EmployeeWorkingShiftDAO newShiftDao = new EmployeeWorkingShiftDAO();
		newShiftDao.remove(fieldGroupShift.getItemDataSource().getBean());
		this.fieldGroupShift.discard();
		this.fieldGroup.save();
		EmployeeDAO emDao = new EmployeeDAO();
		emDao.refresh(fieldGroup.getItemDataSource().getBean());
		this.fieldGroup.setItemDataSource(fieldGroup.getItemDataSource().getBean());
		BindEmployeeWorkingShift(this.fieldGroup.getItemDataSource().getBean());
	}
	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.form = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
		this.label2 = new XdevLabel();
		this.textField2 = new XdevTextField();
		this.label3 = new XdevLabel();
		this.textField3 = new XdevTextField();
		this.label5 = new XdevLabel();
		this.popupDateField = new XdevPopupDateField();
		this.label6 = new XdevLabel();
		this.textField5 = new XdevTextField();
		this.label7 = new XdevLabel();
		this.textField6 = new XdevTextField();
		this.label8 = new XdevLabel();
		this.textField7 = new XdevTextField();
		this.label9 = new XdevLabel();
		this.comboBox = new XdevComboBox<>();
		this.label10 = new XdevLabel();
		this.comboBox2 = new XdevComboBox<>();
		this.label12 = new XdevLabel();
		this.gridLayout = new XdevGridLayout();
		this.comboBoxShiftSelection = new XdevComboBox<>();
		this.buttonAddShift = new XdevButton();
		this.tableWorkingShift = new XdevTable<>();
		this.comboBoxEditShift = new XdevComboBox<>();
		this.textFieldShiftSalary = new XdevTextField();
		this.buttonEditShift = new XdevButton();
		this.buttonDeleteShift = new XdevButton();
		this.label11 = new XdevLabel();
		this.twinColSelect = new XdevTwinColSelect<>();
		this.cmdReset = new XdevButton();
		this.cmdSave = new XdevButton();
		this.fieldGroup = new XdevFieldGroup<>(Employee.class);
		this.fieldGroupShift = new XdevFieldGroup<>(EmployeeWorkingShift.class);
	
		this.label.setValue("Mã");
		this.textField.setTabIndex(1);
		this.label2.setValue("Họ tên");
		this.textField2.setTabIndex(2);
		this.label3.setValue("Tên đăng nhập");
		this.textField3.setTabIndex(3);
		this.label5.setValue("Ngày sinh");
		this.popupDateField.setTabIndex(5);
		this.label6.setValue("Điện thoại");
		this.textField5.setTabIndex(6);
		this.label7.setValue("Địa chỉ");
		this.textField6.setTabIndex(7);
		this.label8.setValue("CMND");
		this.textField7.setTabIndex(8);
		this.label9.setValue("Trạng thái");
		this.comboBox.setTabIndex(9);
		this.label10.setValue("Quản lý bởi");
		this.comboBox2.setTabIndex(10);
		this.comboBox2.setContainerDataSource(Employee.class, DAOs.get(EmployeeDAO.class).findAll());
		this.comboBox2.setItemCaptionPropertyId("code");
		this.label12.setValue("Ca làm việc");
		this.gridLayout.setColumns(2);
		this.gridLayout.setMargin(new MarginInfo(false));
		this.gridLayout.setRows(2);
		this.comboBoxShiftSelection.setItemCaptionFromAnnotation(false);
		this.comboBoxShiftSelection.setInputPrompt("Chọn ca để thêm");
		this.comboBoxShiftSelection.setContainerDataSource(WorkingShift.class,
				DAOs.get(WorkingShiftDAO.class).findAll());
		this.comboBoxShiftSelection.setItemCaptionPropertyId("name");
		this.buttonAddShift.setCaption("Thêm");
		this.tableWorkingShift.setContainerDataSource(EmployeeWorkingShift.class, false,
				NestedProperty.of("workingShift.name", String.class));
		this.tableWorkingShift.setVisibleColumns("workingShift.name", "salary");
		this.tableWorkingShift.setColumnHeader("workingShift.name", "Tên ca");
		this.tableWorkingShift.setColumnHeader("salary", "Lương");
		this.comboBoxEditShift.setItemCaptionFromAnnotation(false);
		this.comboBoxEditShift.setInputPrompt("Thay đổi ca");
		this.comboBoxEditShift.setContainerDataSource(WorkingShift.class);
		this.comboBoxEditShift.setItemCaptionPropertyId("name");
		this.textFieldShiftSalary.setInputPrompt("Cập nhật lương");
		this.buttonEditShift.setCaption("Cập nhật");
		this.buttonDeleteShift.setCaption("Xóa");
		this.label11.setValue("Vai trò");
		this.twinColSelect.setTabIndex(11);
		this.twinColSelect.setContainerDataSource(Role.class, DAOs.get(RoleDAO.class).findAll());
		this.twinColSelect.setItemCaptionPropertyId("name");
		this.cmdReset.setCaption("Huỷ");
		this.cmdSave.setCaption("Lưu");
		this.fieldGroup.bind(this.textField, "code");
		this.fieldGroup.bind(this.textField2, "fullName");
		this.fieldGroup.bind(this.textField3, "userName");
		this.fieldGroup.bind(this.textFieldShiftSalary, "password");
		this.fieldGroup.bind(this.popupDateField, "birthday");
		this.fieldGroup.bind(this.textField5, "phone");
		this.fieldGroup.bind(this.textField6, "address");
		this.fieldGroup.bind(this.textField7, "idNumber");
		this.fieldGroup.bind(this.comboBox, "status");
		this.fieldGroup.bind(this.comboBox2, "manageBy");
		this.fieldGroup.bind(this.twinColSelect, "roles");
		this.fieldGroupShift.bind(this.comboBoxEditShift, "workingShift");
		this.fieldGroupShift.bind(this.textFieldShiftSalary, "salary");
	
		XDEV.bindForm(tableWorkingShift, fieldGroupShift);
	
		this.gridLayout.setColumns(3);
		this.gridLayout.setRows(6);
		this.comboBoxShiftSelection.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxShiftSelection.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxShiftSelection, 0, 0);
		this.gridLayout.setComponentAlignment(this.comboBoxShiftSelection, Alignment.MIDDLE_CENTER);
		this.buttonAddShift.setWidth(150, Unit.PIXELS);
		this.buttonAddShift.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.buttonAddShift, 1, 0);
		this.gridLayout.setComponentAlignment(this.buttonAddShift, Alignment.MIDDLE_CENTER);
		this.tableWorkingShift.setSizeFull();
		this.gridLayout.addComponent(this.tableWorkingShift, 0, 1, 1, 4);
		this.comboBoxEditShift.setWidth(150, Unit.PIXELS);
		this.comboBoxEditShift.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.comboBoxEditShift, 2, 1);
		this.textFieldShiftSalary.setWidth(150, Unit.PIXELS);
		this.textFieldShiftSalary.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.textFieldShiftSalary, 2, 2);
		this.buttonEditShift.setWidth(150, Unit.PIXELS);
		this.buttonEditShift.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.buttonEditShift, 2, 3);
		this.gridLayout.setComponentAlignment(this.buttonEditShift, Alignment.MIDDLE_CENTER);
		this.buttonDeleteShift.setWidth(150, Unit.PIXELS);
		this.buttonDeleteShift.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.buttonDeleteShift, 2, 4);
		this.gridLayout.setColumnExpandRatio(0, 0.1F);
		CustomComponent gridLayout_vSpacer = new CustomComponent();
		gridLayout_vSpacer.setSizeFull();
		this.gridLayout.addComponent(gridLayout_vSpacer, 0, 5, 2, 5);
		this.gridLayout.setRowExpandRatio(5, 1.0F);
		this.form.setColumns(2);
		this.form.setRows(12);
		this.label.setSizeUndefined();
		this.form.addComponent(this.label, 0, 0);
		this.textField.setWidth(100, Unit.PERCENTAGE);
		this.textField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField, 1, 0);
		this.label2.setSizeUndefined();
		this.form.addComponent(this.label2, 0, 1);
		this.textField2.setWidth(100, Unit.PERCENTAGE);
		this.textField2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField2, 1, 1);
		this.label3.setSizeUndefined();
		this.form.addComponent(this.label3, 0, 2);
		this.textField3.setWidth(100, Unit.PERCENTAGE);
		this.textField3.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField3, 1, 2);
		this.label5.setSizeUndefined();
		this.form.addComponent(this.label5, 0, 3);
		this.popupDateField.setWidth(100, Unit.PERCENTAGE);
		this.popupDateField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.popupDateField, 1, 3);
		this.label6.setSizeUndefined();
		this.form.addComponent(this.label6, 0, 4);
		this.textField5.setWidth(100, Unit.PERCENTAGE);
		this.textField5.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField5, 1, 4);
		this.label7.setSizeUndefined();
		this.form.addComponent(this.label7, 0, 5);
		this.textField6.setWidth(100, Unit.PERCENTAGE);
		this.textField6.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField6, 1, 5);
		this.label8.setSizeUndefined();
		this.form.addComponent(this.label8, 0, 6);
		this.textField7.setWidth(100, Unit.PERCENTAGE);
		this.textField7.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField7, 1, 6);
		this.label9.setSizeUndefined();
		this.form.addComponent(this.label9, 0, 7);
		this.comboBox.setWidth(100, Unit.PERCENTAGE);
		this.comboBox.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.comboBox, 1, 7);
		this.label10.setSizeUndefined();
		this.form.addComponent(this.label10, 0, 8);
		this.comboBox2.setWidth(100, Unit.PERCENTAGE);
		this.comboBox2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.comboBox2, 1, 8);
		this.label12.setSizeUndefined();
		this.form.addComponent(this.label12, 0, 9);
		this.gridLayout.setWidth(100, Unit.PERCENTAGE);
		this.gridLayout.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.gridLayout, 1, 9);
		this.label11.setSizeUndefined();
		this.form.addComponent(this.label11, 0, 10);
		this.twinColSelect.setWidth(100, Unit.PERCENTAGE);
		this.twinColSelect.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.twinColSelect, 1, 10);
		this.cmdReset.setSizeUndefined();
		this.form.addComponent(this.cmdReset, 0, 11);
		this.form.setComponentAlignment(this.cmdReset, Alignment.MIDDLE_LEFT);
		this.cmdSave.setSizeUndefined();
		this.form.addComponent(this.cmdSave, 1, 11);
		this.form.setComponentAlignment(this.cmdSave, Alignment.MIDDLE_RIGHT);
		this.form.setColumnExpandRatio(1, 0.1F);
		this.form.setRowExpandRatio(9, 0.1F);
		this.form.setSizeFull();
		this.setContent(this.form);
		this.setSizeFull();
	
		buttonAddShift.addClickListener(event -> this.buttonAddShift_buttonClick(event));
		buttonEditShift.addClickListener(event -> this.buttonEditShift_buttonClick(event));
		buttonDeleteShift.addClickListener(event -> this.buttonDeleteShift_buttonClick(event));
		cmdReset.addClickListener(event -> this.cmdReset_buttonClick(event));
		cmdSave.addClickListener(event -> this.cmdSave_buttonClick(event));
	} // </generated-code>
	// <generated-code name="variables">
	private XdevTwinColSelect<Role> twinColSelect;
	private XdevButton buttonAddShift, buttonEditShift, buttonDeleteShift, cmdReset, cmdSave;
	private XdevFieldGroup<EmployeeWorkingShift> fieldGroupShift;
	private XdevGridLayout form, gridLayout;
	private XdevComboBox<Employee> comboBox2;
	private XdevComboBox<WorkingShift> comboBoxShiftSelection, comboBoxEditShift;
	private XdevLabel label, label2, label3, label5, label6, label7, label8, label9, label10, label12, label11;
	private XdevPopupDateField popupDateField;
	private XdevFieldGroup<Employee> fieldGroup;
	private XdevComboBox<EmployeeStatusEnum> comboBox;
	private XdevTextField textField, textField2, textField3, textField5, textField6, textField7,
			textFieldShiftSalary;
	private XdevTable<EmployeeWorkingShift> tableWorkingShift; // </generated-code>


}
