package com.khoitam.dinermanagement.ui.setting.detailview;

import com.khoitam.dinermanagement.entities.Certificate;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Window;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;

public class SettingCertificateDetailView extends XdevView {

	/**
	 * Constructor khi tao moi
	 */
	public SettingCertificateDetailView() {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(new Certificate());
	}
	
	/**
	 * Constructor khi chinh sua
	 */
	public SettingCertificateDetailView(Certificate certificate) {
		super();
		this.initUI();
		this.fieldGroup.setItemDataSource(certificate);
	}


	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdReset}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdReset_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.discard();
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.close();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #cmdSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void cmdSave_buttonClick(Button.ClickEvent event) {
		this.fieldGroup.save();
		// Lay popup window chua man hinh Certificate
		Window popupWindow = (Window)this.getParent();
		if (popupWindow != null) {
			popupWindow.setData(this.fieldGroup.getItemDataSource().getBean());
			// Dong popup
			popupWindow.close();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.form = new XdevGridLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
		this.label2 = new XdevLabel();
		this.textField2 = new XdevTextField();
		this.cmdReset = new XdevButton();
		this.cmdSave = new XdevButton();
		this.fieldGroup = new XdevFieldGroup<>(Certificate.class);
	
		this.label.setValue("Mã");
		this.textField.setTabIndex(1);
		this.label2.setValue("Tên");
		this.textField2.setTabIndex(2);
		this.cmdReset.setCaption("Hủy");
		this.cmdSave.setCaption("Lưu");
		this.fieldGroup.bind(this.textField, "code");
		this.fieldGroup.bind(this.textField2, "name");
	
		this.form.setColumns(2);
		this.form.setRows(4);
		this.label.setSizeUndefined();
		this.form.addComponent(this.label, 0, 0);
		this.textField.setWidth(100, Unit.PERCENTAGE);
		this.textField.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField, 1, 0);
		this.label2.setSizeUndefined();
		this.form.addComponent(this.label2, 0, 1);
		this.textField2.setWidth(100, Unit.PERCENTAGE);
		this.textField2.setHeight(-1, Unit.PIXELS);
		this.form.addComponent(this.textField2, 1, 1);
		this.cmdReset.setSizeUndefined();
		this.form.addComponent(this.cmdReset, 0, 2);
		this.form.setComponentAlignment(this.cmdReset, Alignment.MIDDLE_LEFT);
		this.cmdSave.setSizeUndefined();
		this.form.addComponent(this.cmdSave, 1, 2);
		this.form.setComponentAlignment(this.cmdSave, Alignment.MIDDLE_RIGHT);
		this.form.setColumnExpandRatio(1, 1.0F);
		CustomComponent form_vSpacer = new CustomComponent();
		form_vSpacer.setSizeFull();
		this.form.addComponent(form_vSpacer, 0, 3, 1, 3);
		this.form.setRowExpandRatio(3, 1.0F);
		this.form.setSizeFull();
		this.setContent(this.form);
		this.setSizeFull();
	
		cmdReset.addClickListener(event -> this.cmdReset_buttonClick(event));
		cmdSave.addClickListener(event -> this.cmdSave_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevFieldGroup<Certificate> fieldGroup;
	private XdevTextField textField, textField2;
	private XdevLabel label, label2;
	private XdevGridLayout form;
	private XdevButton cmdReset, cmdSave; // </generated-code>

}
