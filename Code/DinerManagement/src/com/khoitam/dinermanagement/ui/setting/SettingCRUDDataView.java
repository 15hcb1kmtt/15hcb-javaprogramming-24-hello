package com.khoitam.dinermanagement.ui.setting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.khoitam.dinermanagement.ui.setting.provider.SettingCertificateViewProvider;
import com.khoitam.dinermanagement.ui.setting.provider.SettingDinerItemCategoryViewProvider;
import com.khoitam.dinermanagement.ui.setting.provider.SettingDinerItemViewProvider;
import com.khoitam.dinermanagement.ui.setting.provider.SettingDinerTableViewProvider;
import com.khoitam.dinermanagement.ui.setting.provider.SettingEmployeeViewProvider;
import com.khoitam.dinermanagement.ui.setting.provider.SettingToolsAndSuppliesViewProvider;
import com.khoitam.dinermanagement.ui.setting.provider.SettingViewProviderBase;
import com.khoitam.dinermanagement.ui.setting.provider.SettingWorkingShiftViewProvider;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window.CloseEvent;
import com.xdev.dal.DAOs;
import com.xdev.ui.PopupWindow;
import com.xdev.ui.XdevBorderLayout;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;
import com.xdev.ui.navigation.Navigation;

public class SettingCRUDDataView extends XdevView {

	/**
	 * 
	 */
	public SettingCRUDDataView() {
		super();
		this.initUI();
		//initListView();
	}
	
	private SettingViewProviderBase<?> settingViewProvider;
	private XdevTable<?> tableObjects;

	private void initListView() {
		tableObjects = this.settingViewProvider.GetTable();
		borderLayout.addComponent(tableObjects, XdevBorderLayout.Constraint.CENTER);
		tableObjects.setMultiSelect(true);
		
		rebindListView();
	}

	private void rebindListView() {
		@SuppressWarnings("rawtypes")
		Class classType = this.settingViewProvider.GetClassType();
		Class daoClassType = this.settingViewProvider.GetDAOClassType();
		tableObjects.setContainerDataSource(classType, DAOs.get(daoClassType).findAll());
		
		Map<String, String> columns = settingViewProvider.GetListViewColumns();
		List<String> columnsOnly = new ArrayList<String>();
		
		for (Map.Entry<String, String> entry : columns.entrySet()) {
		    String key = entry.getKey();
		    columnsOnly.add(key);
		}
		
		tableObjects.setVisibleColumns(columnsOnly.toArray());
		
		for (Map.Entry<String, String> entry : columns.entrySet()) {
		    String columnField = entry.getKey();
		    String columnCaption = entry.getValue();
		    tableObjects.setColumnHeader(columnField, columnCaption);
		}
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		super.enter(event);
		String crudType = Navigation.getParameter(event, "settingViewProvider", String.class);
		if (crudType.equals("Certificate"))
		{
			this.settingViewProvider = new SettingCertificateViewProvider();
		}
		else if (crudType.equals("DinerItemCategory"))
		{
			this.settingViewProvider = new SettingDinerItemCategoryViewProvider();
		}
		else if (crudType.equals("DinerItem"))
		{
			this.settingViewProvider = new SettingDinerItemViewProvider();
		}
		else if (crudType.equals("DinerTable"))
		{
			this.settingViewProvider = new SettingDinerTableViewProvider();
		}
		else if (crudType.equals("Employee"))
		{
			this.settingViewProvider = new SettingEmployeeViewProvider();
		}
		else if (crudType.equals("ToolsAndSupplies"))
		{
			this.settingViewProvider = new SettingToolsAndSuppliesViewProvider();
		}
		else if (crudType.equals("WorkingShift"))
		{
			this.settingViewProvider = new SettingWorkingShiftViewProvider();
		}
		initListView(); 
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAdd}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonAdd_buttonClick(Button.ClickEvent event) {
		XdevView detailView = settingViewProvider.GetNewDetailView();
		PopupWindow.For(detailView).closable(false).onClose(this::onDetailPopupClosed).draggable(true).resizable(true).modal(true).size(300f, 300f).show();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonEdit}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonEdit_buttonClick(Button.ClickEvent event) {
		if (tableObjects.getSelectedItems().size() == 1) {
			Object objectData = tableObjects.getSelectedItems().get(0).getBean();
			XdevView detailView = settingViewProvider.GetEditDetailView(objectData);
			PopupWindow.For(detailView).closable(false).onClose(this::onDetailPopupClosed).draggable(true).resizable(true).modal(true).size(300f, 300f).show();
		}
		else {
			Notification.show("Lưu ý", "Cần chọn 1 đối tượng để sửa.", Notification.Type.WARNING_MESSAGE);
		}
	}
	
	private void onDetailPopupClosed(CloseEvent event) {
		rebindListView();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonDelete}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonDelete_buttonClick(Button.ClickEvent event) {
	
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.borderLayout = new XdevBorderLayout();
		this.crudAction = new XdevHorizontalLayout();
		this.buttonAdd = new XdevButton();
		this.buttonEdit = new XdevButton();
		this.buttonDelete = new XdevButton();
	
		this.buttonAdd.setCaption("Add");
		this.buttonEdit.setCaption("Edit");
		this.buttonDelete.setCaption("Remove");
	
		this.buttonAdd.setSizeUndefined();
		this.crudAction.addComponent(this.buttonAdd);
		this.crudAction.setComponentAlignment(this.buttonAdd, Alignment.MIDDLE_CENTER);
		this.buttonEdit.setSizeUndefined();
		this.crudAction.addComponent(this.buttonEdit);
		this.crudAction.setComponentAlignment(this.buttonEdit, Alignment.MIDDLE_CENTER);
		this.buttonDelete.setSizeUndefined();
		this.crudAction.addComponent(this.buttonDelete);
		this.crudAction.setComponentAlignment(this.buttonDelete, Alignment.MIDDLE_CENTER);
		CustomComponent crudAction_spacer = new CustomComponent();
		crudAction_spacer.setSizeFull();
		this.crudAction.addComponent(crudAction_spacer);
		this.crudAction.setExpandRatio(crudAction_spacer, 1.0F);
		crudAction.setHeight(100, Unit.PIXELS);
		borderLayout.addComponent(crudAction, XdevBorderLayout.Constraint.NORTH);
		this.borderLayout.setSizeFull();
		this.setContent(this.borderLayout);
		this.setSizeFull();
	
		buttonAdd.addClickListener(event -> this.buttonAdd_buttonClick(event));
		buttonEdit.addClickListener(event -> this.buttonEdit_buttonClick(event));
		buttonDelete.addClickListener(event -> this.buttonDelete_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonAdd, buttonEdit, buttonDelete;
	private XdevBorderLayout borderLayout;
	private XdevHorizontalLayout crudAction; // </generated-code>


}
