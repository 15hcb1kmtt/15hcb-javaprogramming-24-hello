package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.Map;

import com.vaadin.navigator.View;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.XdevBeanItemContainer;
import com.xdev.ui.entitycomponent.table.XdevTable;

public abstract class SettingViewProviderBase<T> implements ISettingViewProvider {

	public abstract XdevTable<T> GetTable();
	
	public abstract Class GetDAOClassType();
	
	public abstract Class<T> GetClassType();
	
	public abstract XdevView GetNewDetailView();
	
	public abstract XdevView GetEditDetailView(Object editObject);
	
	public abstract Map<String, String> GetListViewColumns();
}
