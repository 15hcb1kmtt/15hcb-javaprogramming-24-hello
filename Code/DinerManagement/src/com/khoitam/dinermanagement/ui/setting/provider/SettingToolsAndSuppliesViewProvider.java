package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.DinerTableDAO;
import com.khoitam.dinermanagement.dal.ToolsAndSuppliesDAO;
import com.khoitam.dinermanagement.entities.DinerTable;
import com.khoitam.dinermanagement.entities.ToolsAndSupplies;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingDinerTableDetailView;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingToolsAndSuppliesDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingToolsAndSuppliesViewProvider extends SettingViewProviderBase<ToolsAndSupplies> {

	@Override
	public Class<ToolsAndSupplies> GetClassType() {
		return ToolsAndSupplies.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingToolsAndSuppliesDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		ToolsAndSupplies editObjectCert = (ToolsAndSupplies)editObject;
		return new SettingToolsAndSuppliesDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("name", "Tên");
		return map;
	}

	@Override
	public XdevTable<ToolsAndSupplies> GetTable() {
		return new XdevTable<ToolsAndSupplies>();
	}

	@Override
	public Class GetDAOClassType() {
		return ToolsAndSuppliesDAO.class;
	}
}
