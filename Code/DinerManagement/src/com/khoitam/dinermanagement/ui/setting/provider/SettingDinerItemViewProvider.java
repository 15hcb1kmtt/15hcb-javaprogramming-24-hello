package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.DinerItemDAO;
import com.khoitam.dinermanagement.entities.DinerItem;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingDinerItemDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingDinerItemViewProvider extends SettingViewProviderBase<DinerItem> {

	@Override
	public Class<DinerItem> GetClassType() {
		return DinerItem.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingDinerItemDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		DinerItem editObjectCert = (DinerItem)editObject;
		return new SettingDinerItemDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("name", "Tên");
		map.put("price", "Giá");
		map.put("outOfOrder", "Hết món");
		map.put("isCookable", "Có thể nấu");
		map.put("category", "Loại");
		return map;
	}

	@Override
	public XdevTable<DinerItem> GetTable() {
		return new XdevTable<DinerItem>();
	}

	@Override
	public Class GetDAOClassType() {
		return DinerItemDAO.class;
	}
}
