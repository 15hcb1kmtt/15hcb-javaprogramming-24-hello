package com.khoitam.dinermanagement.ui.setting.provider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.khoitam.dinermanagement.dal.DinerTableDAO;
import com.khoitam.dinermanagement.entities.DinerTable;
import com.khoitam.dinermanagement.ui.setting.detailview.SettingDinerTableDetailView;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class SettingDinerTableViewProvider extends SettingViewProviderBase<DinerTable> {

	@Override
	public Class<DinerTable> GetClassType() {
		return DinerTable.class;
	}

	@Override
	public XdevView GetNewDetailView() {
		return new SettingDinerTableDetailView();
	}
	
	@Override
	public XdevView GetEditDetailView(Object editObject) {
		DinerTable editObjectCert = (DinerTable)editObject;
		return new SettingDinerTableDetailView(editObjectCert);
	}

	@Override
	public Map<String, String> GetListViewColumns() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("code", "Mã");
		map.put("name", "Tên");
		map.put("maximumGuest", "Khách tối đa");
		return map;
	}

	@Override
	public XdevTable<DinerTable> GetTable() {
		return new XdevTable<DinerTable>();
	}

	@Override
	public Class GetDAOClassType() {
		return DinerTableDAO.class;
	}
}
