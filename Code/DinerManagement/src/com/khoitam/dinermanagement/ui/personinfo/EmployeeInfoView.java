package com.khoitam.dinermanagement.ui.personinfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.khoitam.dinermanagement.business.enumeration.EmployeeWorkingScheduleStatusEnum;
import com.khoitam.dinermanagement.business.personinfo.EmployeeScheduleData;
import com.khoitam.dinermanagement.dal.EmployeeDAO;
import com.khoitam.dinermanagement.dal.EmployeeWorkingScheduleDAO;
import com.khoitam.dinermanagement.dal.WorkingShiftDAO;
import com.khoitam.dinermanagement.entities.Employee;
import com.khoitam.dinermanagement.entities.EmployeeWorkingSchedule;
import com.khoitam.dinermanagement.entities.EmployeeWorkingShift;
import com.khoitam.dinermanagement.entities.WorkingShift;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.xdev.security.authentication.ui.Authentication;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevFormLayout;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevPopupDateField;
import com.xdev.ui.XdevTabSheet;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;
import com.xdev.util.ConverterBuilder;

public class EmployeeInfoView extends XdevView {

	/**
	 * 
	 */
	public EmployeeInfoView() {
		super();
		this.initUI();
	}
	
	private Calendar calendarShowData;

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		super.enter(event);
		calendarShowData = Calendar.getInstance();
		String loggedUserName = Authentication.getUser().name();
		
		EmployeeDAO employeeDao = new EmployeeDAO();
		Employee loggedUser = employeeDao.GetEmployeeByUsername(loggedUserName);
		fieldGroupEmployee.setItemDataSource(loggedUser);
		
		GetCurrentUserScheduleData((Calendar)calendarShowData.clone(), loggedUserName);
		GetApproveScheduleData(loggedUserName);
		BindLowerEmployeeCombobox(loggedUserName);
	}
	
	private void BindCurrentUserScheduleData(List<EmployeeScheduleData> dataSource)
	{
		this.tableScheduleData.setContainerDataSource(EmployeeScheduleData.class, dataSource);
		//this.tableScheduleData.setContainerDataSource(dataSource);
		this.tableScheduleData.setVisibleColumns("fromTime", "toTime", "shiftName", "status");
		this.tableScheduleData.setColumnHeader("fromTime", "Giờ vào");
		this.tableScheduleData.setColumnHeader("toTime", "Giờ ra");
		this.tableScheduleData.setColumnHeader("shiftName", "Ca");
		this.tableScheduleData.setColumnHeader("status", "Trạng thái");
	}
	
	private void BindApproveData(List<EmployeeScheduleData> dataSource)
	{
		this.tableLowerEmployeeApprove.setContainerDataSource(EmployeeScheduleData.class, dataSource);
		//this.tableLowerEmployeeApprove.setContainerDataSource(dataSource);
		this.tableLowerEmployeeApprove.setVisibleColumns("employeeName", "fromTime", "toTime", "shiftName", "status");
		this.tableLowerEmployeeApprove.setColumnHeader("employeeName", "Tên nhân viên");
		this.tableLowerEmployeeApprove.setColumnHeader("fromTime", "Giờ vào");
		this.tableLowerEmployeeApprove.setColumnHeader("toTime", "Giờ ra");
		this.tableLowerEmployeeApprove.setColumnHeader("shiftName", "Ca");
		this.tableLowerEmployeeApprove.setColumnHeader("status", "Trạng thái");
	}
	
	private void BindLowerUserScheduleData(List<EmployeeScheduleData> dataSource)
	{
		this.tableLowerEmployeeSchedule.setContainerDataSource(EmployeeScheduleData.class, dataSource);
		//this.tableLowerEmployeeSchedule.setContainerDataSource(dataSource);
		this.tableLowerEmployeeSchedule.setVisibleColumns("employeeName", "fromTime", "toTime", "shiftName", "status");
		this.tableLowerEmployeeSchedule.setColumnHeader("employeeName", "Tên nhân viên");
		this.tableLowerEmployeeSchedule.setColumnHeader("fromTime", "Giờ vào");
		this.tableLowerEmployeeSchedule.setColumnHeader("toTime", "Giờ ra");
		this.tableLowerEmployeeSchedule.setColumnHeader("shiftName", "Ca");
		this.tableLowerEmployeeSchedule.setColumnHeader("status", "Trạng thái");
	}
	
	private void BindLowerEmployeeCombobox(String username) {
		EmployeeDAO employeeDao = new EmployeeDAO();
		Employee loggedUser = employeeDao.GetEmployeeByUsername(username);
		Criteria lowerEmployeeCriteria = employeeDao
				.buildHibernateCriteriaQuery(Employee.class);
		lowerEmployeeCriteria.createCriteria("manageBy").add(Restrictions.eq("id", loggedUser.getId()));
		this.comboBoxSelectLowerEmployeeSchedule.setContainerDataSource(Employee.class, lowerEmployeeCriteria.list());
		this.comboBoxSelectLowerEmployeeSchedule.setItemCaptionPropertyId("fullName");
	}
	
	private void GetApproveScheduleData(String masterUsername) {
		List<EmployeeScheduleData> beans = new ArrayList<EmployeeScheduleData>();
		
		EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
		EmployeeDAO employeeDao = new EmployeeDAO();
		Employee masterUser = employeeDao.GetEmployeeByUsername(masterUsername);
		Criteria lowerEmployeeCriteria = employeeDao
				.buildHibernateCriteriaQuery(Employee.class);
		lowerEmployeeCriteria.createCriteria("manageBy").add(Restrictions.eq("id", masterUser.getId()));
		
		List<Employee> lowerEmployees = lowerEmployeeCriteria.list();
		
		for (Employee employeeToGetData : lowerEmployees) {
			List<EmployeeWorkingSchedule> dataToApprove = workingDataDao.GetApproveDataForEmployee(employeeToGetData.getId());
			
			for (EmployeeWorkingSchedule employeeWorkingSchedule : dataToApprove) {
				EmployeeScheduleData newScheduleData = new EmployeeScheduleData();
				newScheduleData.setDate(employeeWorkingSchedule.getWorkingShift().getFromTime());
				newScheduleData.setFromTime(new SimpleDateFormat("kk:mm (dd/MM)").format(employeeWorkingSchedule.getWorkingShift().getFromTime()));
				newScheduleData.setToTime(new SimpleDateFormat("kk:mm (dd/MM)").format(employeeWorkingSchedule.getWorkingShift().getToTime()));
				newScheduleData.setEmployeeId(employeeToGetData.getId());
				newScheduleData.setEmployeeName(employeeToGetData.getFullName());
				newScheduleData.setScheduleId(employeeWorkingSchedule.getId());
				newScheduleData.setShiftId(employeeWorkingSchedule.getWorkingShift().getId());
				newScheduleData.setShiftName(employeeWorkingSchedule.getWorkingShift().getName());
				newScheduleData.setStatus("Xin nghỉ phép");
				beans.add(newScheduleData);
			}
		}
		
		BindApproveData(beans);
	}
	
	private void GetCurrentUserScheduleData(Calendar dayInWeek, String username) {
		List<EmployeeScheduleData> beans = new ArrayList<EmployeeScheduleData>();
		
		EmployeeDAO employeeDao = new EmployeeDAO();
		EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
		Employee loggedUser = employeeDao.GetEmployeeByUsername(username);
		
		String fromDateStr = "";
		String toDateStr = "";
		
		dayInWeek.set(Calendar.HOUR_OF_DAY, 0);
		dayInWeek.clear(Calendar.MINUTE);
		dayInWeek.clear(Calendar.SECOND);
		dayInWeek.clear(Calendar.MILLISECOND);
		dayInWeek.set(Calendar.DAY_OF_WEEK, dayInWeek.getFirstDayOfWeek());
		
		for (int i = 0; i < 7; i++) {
			dayInWeek.add(Calendar.DAY_OF_MONTH, 1);
			for (EmployeeWorkingShift shift : loggedUser.getEmployeeWorkingShifts()) {
				Date fromData = dayInWeek.getTime();
				fromData.setHours(shift.getWorkingShift().getFromTime().getHours());
				fromData.setMinutes(shift.getWorkingShift().getFromTime().getMinutes());
				Date toData = dayInWeek.getTime();
				toData.setHours(shift.getWorkingShift().getToTime().getHours());
				toData.setMinutes(shift.getWorkingShift().getToTime().getMinutes());
				EmployeeScheduleData newScheduleData = new EmployeeScheduleData();
				newScheduleData.setDate(fromData);
				newScheduleData.setSalary(shift.getSalary());
				newScheduleData.setFromTime(new SimpleDateFormat("kk:mm (dd/MM)").format(fromData));
				newScheduleData.setToTime(new SimpleDateFormat("kk:mm (dd/MM)").format(toData));
				newScheduleData.setEmployeeId(loggedUser.getId());
				newScheduleData.setShiftId(shift.getWorkingShift().getId());
				newScheduleData.setShiftName(shift.getWorkingShift().getName());
				EmployeeWorkingSchedule workingData = workingDataDao.GetScheduleOfDateRange(loggedUser.getId(), fromData, toData);
				if (workingData != null) {
					newScheduleData.setScheduleId(workingData.getId());
					if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.DoneWorking) {
						newScheduleData.setStatus("Có làm việc");
					}
					else if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.AskForAbsent) {
						newScheduleData.setStatus("Xin nghỉ phép");
					}
					else if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.ApprovedAbsent) {
						newScheduleData.setStatus("Cho nghỉ phép");
					}
				} else {
					newScheduleData.setStatus("Chưa có dữ liệu");
				}
				beans.add(newScheduleData);
			}
			if (i == 0) {
				fromDateStr = new SimpleDateFormat("dd/MM").format(dayInWeek.getTime());
			}
			else if (i == 6) {
				toDateStr = new SimpleDateFormat("dd/MM").format(dayInWeek.getTime());
			}
		}
		labelFromToDate.setValue(fromDateStr + " - " + toDateStr);
		BindCurrentUserScheduleData(beans);
	}
	
	public void GetUserScheduleDataInDate(Date date, int userId) {
		List<EmployeeScheduleData> beans = new ArrayList<EmployeeScheduleData>();
		
		EmployeeDAO employeeDao = new EmployeeDAO();
		EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
		Employee userToGetSchedule = employeeDao.find(userId);
		
		for (EmployeeWorkingShift shift : userToGetSchedule.getEmployeeWorkingShifts()) {
			Date fromData = (Date)date.clone();
			fromData.setHours(shift.getWorkingShift().getFromTime().getHours());
			fromData.setMinutes(shift.getWorkingShift().getFromTime().getMinutes());
			Date toData = (Date)date;
			toData.setHours(shift.getWorkingShift().getToTime().getHours());
			toData.setMinutes(shift.getWorkingShift().getToTime().getMinutes());
			EmployeeScheduleData newScheduleData = new EmployeeScheduleData();
			newScheduleData.setDate(fromData);
			newScheduleData.setSalary(shift.getSalary());
			newScheduleData.setFromTime(new SimpleDateFormat("kk:mm (dd/MM)").format(fromData));
			newScheduleData.setToTime(new SimpleDateFormat("kk:mm (dd/MM)").format(toData));
			newScheduleData.setEmployeeId(userToGetSchedule.getId());
			newScheduleData.setEmployeeName(userToGetSchedule.getFullName());
			newScheduleData.setShiftId(shift.getWorkingShift().getId());
			newScheduleData.setShiftName(shift.getWorkingShift().getName());
			EmployeeWorkingSchedule workingData = workingDataDao.GetScheduleOfDateRange(userToGetSchedule.getId(), fromData, toData);
			if (workingData != null) {
				newScheduleData.setScheduleId(workingData.getId());
				if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.DoneWorking) {
					newScheduleData.setStatus("Có làm việc");
				}
				else if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.AskForAbsent) {
					newScheduleData.setStatus("Xin nghỉ phép");
				}
				else if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.ApprovedAbsent) {
					newScheduleData.setStatus("Cho nghỉ phép");
				}
			} else {
				newScheduleData.setStatus("Chưa có dữ liệu");
			}
			beans.add(newScheduleData);
		}
		BindLowerUserScheduleData(beans);
	}
	
	public void GetScheduleDataInDate(Date date) {
		List<EmployeeScheduleData> beans = new ArrayList<EmployeeScheduleData>();
		EmployeeDAO employeeDao = new EmployeeDAO();
		EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
		String loggedUserName = Authentication.getUser().name();
		Employee loggedUser = employeeDao.GetEmployeeByUsername(loggedUserName);
		
		Criteria lowerEmployeeCriteria = employeeDao
				.buildHibernateCriteriaQuery(Employee.class);
		lowerEmployeeCriteria.createCriteria("manageBy").add(Restrictions.eq("id", loggedUser.getId()));

		List<Employee> lowerEmployee = lowerEmployeeCriteria.list();
		
		for (Employee userToGetSchedule : lowerEmployee) {
			for (EmployeeWorkingShift shift : userToGetSchedule.getEmployeeWorkingShifts()) {
				Date fromData = (Date)date.clone();
				fromData.setHours(shift.getWorkingShift().getFromTime().getHours());
				fromData.setMinutes(shift.getWorkingShift().getFromTime().getMinutes());
				Date toData = (Date)date;
				toData.setHours(shift.getWorkingShift().getToTime().getHours());
				toData.setMinutes(shift.getWorkingShift().getToTime().getMinutes());
				EmployeeScheduleData newScheduleData = new EmployeeScheduleData();
				newScheduleData.setDate(fromData);
				newScheduleData.setSalary(shift.getSalary());
				newScheduleData.setFromTime(new SimpleDateFormat("kk:mm (dd/MM)").format(fromData));
				newScheduleData.setToTime(new SimpleDateFormat("kk:mm (dd/MM)").format(toData));
				newScheduleData.setEmployeeId(userToGetSchedule.getId());
				newScheduleData.setEmployeeName(userToGetSchedule.getFullName());
				newScheduleData.setShiftId(shift.getWorkingShift().getId());
				newScheduleData.setShiftName(shift.getWorkingShift().getName());
				EmployeeWorkingSchedule workingData = workingDataDao.GetScheduleOfDateRange(userToGetSchedule.getId(), fromData, toData);
				if (workingData != null) {
					newScheduleData.setScheduleId(workingData.getId());
					if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.DoneWorking) {
						newScheduleData.setStatus("Có làm việc");
					}
					else if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.AskForAbsent) {
						newScheduleData.setStatus("Xin nghỉ phép");
					}
					else if (workingData.getStatus() == EmployeeWorkingScheduleStatusEnum.ApprovedAbsent) {
						newScheduleData.setStatus("Cho nghỉ phép");
					}
				} else {
					newScheduleData.setStatus("Chưa có dữ liệu");
				}
				beans.add(newScheduleData);
			}
		}
		BindLowerUserScheduleData(beans);
	}
	
	private void RefreshLowerEmployeeWorkingData() {
		Date selectedDate = popupDateFieldSelectDate.getValue();
		BeanItem<Employee> selectedEmBean = comboBoxSelectLowerEmployeeSchedule.getSelectedItem();
		if (selectedEmBean != null) {
			GetUserScheduleDataInDate(selectedDate, selectedEmBean.getBean().getId());
		} else {
			GetScheduleDataInDate(selectedDate);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonPreviousWeek}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonPreviousWeek_buttonClick(Button.ClickEvent event) {
		calendarShowData.add(Calendar.DAY_OF_YEAR, -7);
		String loggedUserName = Authentication.getUser().name();
		GetCurrentUserScheduleData((Calendar)calendarShowData.clone(), loggedUserName);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonNextWeek}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonNextWeek_buttonClick(Button.ClickEvent event) {
		calendarShowData.add(Calendar.DAY_OF_YEAR, 7);
		String loggedUserName = Authentication.getUser().name();
		GetCurrentUserScheduleData((Calendar)calendarShowData.clone(),loggedUserName);
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxSelectLowerEmployeeSchedule}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate
	 */
	private void comboBoxSelectLowerEmployeeSchedule_valueChange(Property.ValueChangeEvent event) {
		RefreshLowerEmployeeWorkingData();
	}

	/**
	 * Event handler delegate method for the {@link XdevPopupDateField}
	 * {@link #popupDateFieldSelectDate}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate
	 */
	private void popupDateFieldSelectDate_valueChange(Property.ValueChangeEvent event) {
		RefreshLowerEmployeeWorkingData();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonApproveAbsent}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonApproveAbsent_buttonClick(Button.ClickEvent event) {
		BeanItem<EmployeeScheduleData> selectedScheduleData = tableLowerEmployeeApprove.getSelectedItem();
		if (selectedScheduleData != null) {
			EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
			EmployeeWorkingSchedule workingData = workingDataDao.find(selectedScheduleData.getBean().getScheduleId());
			if (workingData != null) {
				workingData.setStatus(EmployeeWorkingScheduleStatusEnum.ApprovedAbsent);
				workingDataDao.save(workingData);
			}
			String loggedUserName = Authentication.getUser().name();
			GetApproveScheduleData(loggedUserName);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAskForAbsent}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonAskForAbsent_buttonClick(Button.ClickEvent event) {
		BeanItem<EmployeeScheduleData> selectedScheduleData = tableScheduleData.getSelectedItem();
		if (selectedScheduleData != null) {
			EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
			WorkingShiftDAO workingShiftDao = new WorkingShiftDAO();
			EmployeeDAO employeeDao = new EmployeeDAO();
			
			WorkingShift workingShift = workingShiftDao.find(selectedScheduleData.getBean().getShiftId());
			Employee employee = employeeDao.find(selectedScheduleData.getBean().getEmployeeId());
			EmployeeWorkingSchedule workingData = new EmployeeWorkingSchedule();
			workingData.setDate(selectedScheduleData.getBean().getDate());
			workingData.setSalary(selectedScheduleData.getBean().getSalary());
			workingData.setStatus(EmployeeWorkingScheduleStatusEnum.AskForAbsent);
			workingData.setWorkingShift(workingShift);
			workingData.setEmployee(employee);
			workingDataDao.save(workingData);
			
			String loggedUserName = Authentication.getUser().name();
			GetCurrentUserScheduleData((Calendar)calendarShowData.clone(), loggedUserName);
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSubmitWorkingData}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonSubmitWorkingData_buttonClick(Button.ClickEvent event) {
		BeanItem<EmployeeScheduleData> selectedScheduleData = tableLowerEmployeeSchedule.getSelectedItem();
		if (selectedScheduleData != null) {
			EmployeeWorkingScheduleDAO workingDataDao = new EmployeeWorkingScheduleDAO();
			WorkingShiftDAO workingShiftDao = new WorkingShiftDAO();
			EmployeeDAO employeeDao = new EmployeeDAO();
			
			WorkingShift workingShift = workingShiftDao.find(selectedScheduleData.getBean().getShiftId());
			Employee employee = employeeDao.find(selectedScheduleData.getBean().getEmployeeId());
			EmployeeWorkingSchedule workingData = new EmployeeWorkingSchedule();
			workingData.setDate(selectedScheduleData.getBean().getDate());
			workingData.setSalary(selectedScheduleData.getBean().getSalary());
			workingData.setStatus(EmployeeWorkingScheduleStatusEnum.DoneWorking);
			workingData.setWorkingShift(workingShift);
			workingData.setEmployee(employee);
			workingData.setLateInMinutes((int)textFieldLateInMinutes.getConvertedValue());
			workingDataDao.save(workingData);
			
			RefreshLowerEmployeeWorkingData();
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.label = new XdevLabel();
		this.label2 = new XdevLabel();
		this.panel3 = new XdevPanel();
		this.formLayout = new XdevFormLayout();
		this.textFieldFullName = new XdevTextField();
		this.textFieldUserName = new XdevTextField();
		this.popupDateFieldBirthDay = new XdevPopupDateField();
		this.textFieldAddress = new XdevTextField();
		this.textFieldPhone = new XdevTextField();
		this.buttonPreviousWeek = new XdevButton();
		this.labelFromToDate = new XdevLabel();
		this.buttonNextWeek = new XdevButton();
		this.tableScheduleData = new XdevTable<>();
		this.buttonAskForAbsent = new XdevButton();
		this.tabSheet = new XdevTabSheet();
		this.panel = new XdevPanel();
		this.gridLayout2 = new XdevGridLayout();
		this.tableLowerEmployeeApprove = new XdevTable<>();
		this.buttonApproveAbsent = new XdevButton();
		this.panel2 = new XdevPanel();
		this.gridLayout3 = new XdevGridLayout();
		this.popupDateFieldSelectDate = new XdevPopupDateField();
		this.comboBoxSelectLowerEmployeeSchedule = new XdevComboBox<>();
		this.tableLowerEmployeeSchedule = new XdevTable<>();
		this.textFieldLateInMinutes = new XdevTextField();
		this.buttonSubmitWorkingData = new XdevButton();
		this.fieldGroupEmployee = new XdevFieldGroup<>(Employee.class);
	
		this.label.setCaption("");
		this.label.setValue("Thông tin người dùng");
		this.label2.setValue("Lịch làm việc cá nhân");
		this.textFieldFullName.setCaption("Tên");
		this.textFieldFullName.setEnabled(false);
		this.textFieldFullName.setReadOnly(true);
		this.textFieldUserName.setCaption("Tên đăng nhập");
		this.textFieldUserName.setEnabled(false);
		this.textFieldUserName.setReadOnly(true);
		this.popupDateFieldBirthDay.setCaption("Ngày sinh");
		this.popupDateFieldBirthDay.setEnabled(false);
		this.popupDateFieldBirthDay.setReadOnly(true);
		this.textFieldAddress.setCaption("Địa chỉ");
		this.textFieldAddress.setEnabled(false);
		this.textFieldAddress.setReadOnly(true);
		this.textFieldPhone.setCaption("Điện thoại");
		this.textFieldPhone.setEnabled(false);
		this.buttonPreviousWeek.setCaption("Xem tuần trước");
		this.labelFromToDate.setValue("06/06/2016 - 12/06/2016");
		this.buttonNextWeek.setCaption("Xem tuần sau");
		this.buttonAskForAbsent.setCaption("Xin phép vắng");
		this.tabSheet.addStyleName("framed");
		this.buttonApproveAbsent.setCaption("Cho phép nghỉ");
		this.popupDateFieldSelectDate.setInputPrompt("Chọn ngày");
		this.comboBoxSelectLowerEmployeeSchedule.setItemCaptionFromAnnotation(false);
		this.comboBoxSelectLowerEmployeeSchedule.setInputPrompt("Chọn nhân viên");
		this.comboBoxSelectLowerEmployeeSchedule.setContainerDataSource(Employee.class);
		this.comboBoxSelectLowerEmployeeSchedule.setItemCaptionPropertyId("fullName");
		this.textFieldLateInMinutes.setConverter(ConverterBuilder.stringToInteger().build());
		this.textFieldLateInMinutes.setInputPrompt("Nhập số phút đi trễ");
		this.buttonSubmitWorkingData.setCaption("Xác nhận có đi làm");
		this.fieldGroupEmployee.setReadOnly(true);
		this.fieldGroupEmployee.bind(this.textFieldFullName, "fullName");
		this.fieldGroupEmployee.bind(this.textFieldUserName, "userName");
		this.fieldGroupEmployee.bind(this.popupDateFieldBirthDay, "birthday");
		this.fieldGroupEmployee.bind(this.textFieldAddress, "address");
		this.fieldGroupEmployee.bind(this.textFieldPhone, "phone");
	
		this.textFieldFullName.setWidth(100, Unit.PERCENTAGE);
		this.textFieldFullName.setHeight(-1, Unit.PIXELS);
		this.formLayout.addComponent(this.textFieldFullName);
		this.textFieldUserName.setWidth(100, Unit.PERCENTAGE);
		this.textFieldUserName.setHeight(-1, Unit.PIXELS);
		this.formLayout.addComponent(this.textFieldUserName);
		this.popupDateFieldBirthDay.setWidth(100, Unit.PERCENTAGE);
		this.popupDateFieldBirthDay.setHeight(-1, Unit.PIXELS);
		this.formLayout.addComponent(this.popupDateFieldBirthDay);
		this.textFieldAddress.setWidth(100, Unit.PERCENTAGE);
		this.textFieldAddress.setHeight(-1, Unit.PIXELS);
		this.formLayout.addComponent(this.textFieldAddress);
		this.textFieldPhone.setWidth(100, Unit.PERCENTAGE);
		this.textFieldPhone.setHeight(-1, Unit.PIXELS);
		this.formLayout.addComponent(this.textFieldPhone);
		this.formLayout.setWidth(-1, Unit.PIXELS);
		this.formLayout.setHeight(100, Unit.PERCENTAGE);
		this.panel3.setContent(this.formLayout);
		this.gridLayout2.setColumns(1);
		this.gridLayout2.setRows(2);
		this.tableLowerEmployeeApprove.setSizeFull();
		this.gridLayout2.addComponent(this.tableLowerEmployeeApprove, 0, 0);
		this.buttonApproveAbsent.setSizeUndefined();
		this.gridLayout2.addComponent(this.buttonApproveAbsent, 0, 1);
		this.gridLayout2.setColumnExpandRatio(0, 0.1F);
		this.gridLayout2.setRowExpandRatio(0, 0.1F);
		this.gridLayout2.setSizeFull();
		this.panel.setContent(this.gridLayout2);
		this.gridLayout3.setColumns(2);
		this.gridLayout3.setRows(3);
		this.popupDateFieldSelectDate.setWidth(100, Unit.PERCENTAGE);
		this.popupDateFieldSelectDate.setHeight(50, Unit.PIXELS);
		this.gridLayout3.addComponent(this.popupDateFieldSelectDate, 0, 0);
		this.comboBoxSelectLowerEmployeeSchedule.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxSelectLowerEmployeeSchedule.setHeight(50, Unit.PIXELS);
		this.gridLayout3.addComponent(this.comboBoxSelectLowerEmployeeSchedule, 1, 0);
		this.tableLowerEmployeeSchedule.setSizeFull();
		this.gridLayout3.addComponent(this.tableLowerEmployeeSchedule, 0, 1, 1, 1);
		this.textFieldLateInMinutes.setWidth(100, Unit.PERCENTAGE);
		this.textFieldLateInMinutes.setHeight(-1, Unit.PIXELS);
		this.gridLayout3.addComponent(this.textFieldLateInMinutes, 0, 2);
		this.buttonSubmitWorkingData.setSizeUndefined();
		this.gridLayout3.addComponent(this.buttonSubmitWorkingData, 1, 2);
		this.gridLayout3.setColumnExpandRatio(0, 0.1F);
		this.gridLayout3.setColumnExpandRatio(1, 0.1F);
		this.gridLayout3.setRowExpandRatio(1, 0.1F);
		this.gridLayout3.setSizeFull();
		this.panel2.setContent(this.gridLayout3);
		this.panel.setSizeFull();
		this.tabSheet.addTab(this.panel, "Duyệt nghỉ", null);
		this.panel2.setSizeFull();
		this.tabSheet.addTab(this.panel2, "Xem lịch làm", null);
		this.tabSheet.setSelectedTab(this.panel2);
		this.gridLayout.setColumns(4);
		this.gridLayout.setRows(5);
		this.label.setWidth(100, Unit.PERCENTAGE);
		this.label.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label, 0, 0);
		this.gridLayout.setComponentAlignment(this.label, Alignment.MIDDLE_CENTER);
		this.label2.setWidth(100, Unit.PERCENTAGE);
		this.label2.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.label2, 1, 0, 3, 0);
		this.gridLayout.setComponentAlignment(this.label2, Alignment.MIDDLE_CENTER);
		this.panel3.setSizeFull();
		this.gridLayout.addComponent(this.panel3, 0, 1, 0, 3);
		this.buttonPreviousWeek.setSizeUndefined();
		this.gridLayout.addComponent(this.buttonPreviousWeek, 1, 1);
		this.labelFromToDate.setSizeUndefined();
		this.gridLayout.addComponent(this.labelFromToDate, 2, 1);
		this.gridLayout.setComponentAlignment(this.labelFromToDate, Alignment.MIDDLE_CENTER);
		this.buttonNextWeek.setSizeUndefined();
		this.gridLayout.addComponent(this.buttonNextWeek, 3, 1);
		this.tableScheduleData.setWidth(100, Unit.PERCENTAGE);
		this.tableScheduleData.setHeight(200, Unit.PIXELS);
		this.gridLayout.addComponent(this.tableScheduleData, 1, 2, 3, 2);
		this.buttonAskForAbsent.setSizeUndefined();
		this.gridLayout.addComponent(this.buttonAskForAbsent, 1, 3);
		this.tabSheet.setWidth(100, Unit.PERCENTAGE);
		this.tabSheet.setHeight(300, Unit.PIXELS);
		this.gridLayout.addComponent(this.tabSheet, 0, 4, 3, 4);
		this.gridLayout.setColumnExpandRatio(0, 0.1F);
		this.gridLayout.setColumnExpandRatio(2, 0.1F);
		this.gridLayout.setRowExpandRatio(1, 0.1F);
		this.gridLayout.setRowExpandRatio(2, 0.1F);
		this.gridLayout.setRowExpandRatio(4, 0.1F);
		this.gridLayout.setSizeFull();
		this.setContent(this.gridLayout);
		this.setSizeFull();
	
		buttonPreviousWeek.addClickListener(event -> this.buttonPreviousWeek_buttonClick(event));
		buttonNextWeek.addClickListener(event -> this.buttonNextWeek_buttonClick(event));
		buttonAskForAbsent.addClickListener(event -> this.buttonAskForAbsent_buttonClick(event));
		buttonApproveAbsent.addClickListener(event -> this.buttonApproveAbsent_buttonClick(event));
		popupDateFieldSelectDate.addValueChangeListener(event -> this.popupDateFieldSelectDate_valueChange(event));
		this.comboBoxSelectLowerEmployeeSchedule.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				EmployeeInfoView.this.comboBoxSelectLowerEmployeeSchedule_valueChange(event);
			}
		});
		buttonSubmitWorkingData.addClickListener(event -> this.buttonSubmitWorkingData_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonPreviousWeek, buttonNextWeek, buttonAskForAbsent, buttonApproveAbsent,
			buttonSubmitWorkingData;
	private XdevTabSheet tabSheet;
	private XdevGridLayout gridLayout, gridLayout2, gridLayout3;
	private XdevPanel panel3, panel, panel2;
	private XdevFormLayout formLayout;
	private XdevComboBox<Employee> comboBoxSelectLowerEmployeeSchedule;
	private XdevLabel label, label2, labelFromToDate;
	private XdevPopupDateField popupDateFieldBirthDay, popupDateFieldSelectDate;
	private XdevFieldGroup<Employee> fieldGroupEmployee;
	private XdevTable<EmployeeScheduleData> tableScheduleData, tableLowerEmployeeApprove,
			tableLowerEmployeeSchedule;
	private XdevTextField textFieldFullName, textFieldUserName, textFieldAddress, textFieldPhone,
			textFieldLateInMinutes; // </generated-code>


}
