package com.khoitam.dinermanagement.ui.sale;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.vaadin.dialogs.ConfirmDialog;

import com.khoitam.dinermanagement.business.enumeration.DinerSalesOrderItemStatusEnum;
import com.khoitam.dinermanagement.dal.DinerItemCategoryDAO;
import com.khoitam.dinermanagement.dal.DinerItemDAO;
import com.khoitam.dinermanagement.dal.DinerSalesOrderDAO;
import com.khoitam.dinermanagement.dal.DinerSalesOrderItemDAO;
import com.khoitam.dinermanagement.dal.DinerTableDAO;
import com.khoitam.dinermanagement.dal.EmployeeDAO;
import com.khoitam.dinermanagement.entities.DinerItem;
import com.khoitam.dinermanagement.entities.DinerItemCategory;
import com.khoitam.dinermanagement.entities.DinerSalesOrder;
import com.khoitam.dinermanagement.entities.DinerSalesOrderItem;
import com.khoitam.dinermanagement.entities.DinerTable;
import com.khoitam.dinermanagement.entities.Employee;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.xdev.dal.DAOs;
import com.xdev.security.authentication.ui.Authentication;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevFieldGroup;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevHorizontalSplitPanel;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevVerticalSplitPanel;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;
import com.xdev.ui.navigation.Navigation;
import com.xdev.ui.navigation.NavigationParameter;
import com.xdev.ui.util.NestedProperty;
import com.xdev.ui.util.wizard.XDEV;

public class TableOrderView extends XdevView {

	@NavigationParameter
	private Boolean LoadTable;
	/**
	 * 
	 */
	public TableOrderView() {
		super();
		this.initUI();
		PopulateStatusCombobox();
	}
	
	public void PopulateStatusCombobox() {
		List<DinerSalesOrderItemStatusEnum> emStatusEmum = Arrays.asList(DinerSalesOrderItemStatusEnum.values());
		comboBoxOrderItemStatus.addItems(emStatusEmum);
	}

	@Override
	public void enter(ViewChangeListener.ViewChangeEvent event) {
		super.enter(event);
		this.LoadTable = Navigation.getParameter(event, "LoadTable", Boolean.class);
		if (LoadTable) {
			LoadTableToGrid(-1);
		}
		LoadCategoryToGrid();
		
	}
	
	private void LoadCategoryToGrid() {
		DinerItemCategoryDAO itemCategoryDao = new DinerItemCategoryDAO();
		List<DinerItemCategory> allCategory = itemCategoryDao.findAll();
		for (DinerItemCategory category : allCategory) {
			final XdevButton categoryButton = new XdevButton(category.getName());
			categoryButton.setData(category.getId());
			categoryButton.addClickListener(event -> this.buttonAllCategory_buttonClick(event));
			this.horizontalLayoutCategory.addComponent(categoryButton);
			this.horizontalLayoutCategory.setComponentAlignment(categoryButton, Alignment.TOP_CENTER);
		}
	}
	
	private void LoadTableToGrid(int filterStatus) {
		this.gridLayoutTableInfo.removeAllComponents();
		
		DinerTableDAO tableDao = new DinerTableDAO();
		DinerSalesOrderDAO salesOrderDao = new DinerSalesOrderDAO();
		
		List<DinerTable> allTables = tableDao.findAll();
		// TODO: button appearance based on diner sales order status.
		List<DinerSalesOrder> inProgressSalesOrder = salesOrderDao.GetInProgressSalesOrder();
		
		for (DinerTable table : allTables) {
			String status = "Trống";
			int statusTable = 0;
			// 0 : Vacant
			// 1 : Occupied
			// 2 : Wait For Item
			// 3 : Item Cooked
			for (DinerSalesOrder dinerSalesOrder : inProgressSalesOrder) {
				if (dinerSalesOrder.getDinerTable().getId() == table.getId()) {
					for (DinerSalesOrderItem item : dinerSalesOrder.getDinerSalesOrderItemsForSalesOrder()) {
						if (item.getStatus() == DinerSalesOrderItemStatusEnum.WaitingForCooking || 
								item.getStatus() == DinerSalesOrderItemStatusEnum.Cooking) {
							statusTable = 2;
							status = "Đang chờ món";
							break;
						} else if (item.getStatus() == DinerSalesOrderItemStatusEnum.Cooked) {
							statusTable = 3;
							status = "Món đã xong";
							break;
						}
					}
					if (statusTable == 0) {
						status = "Có khách";
						statusTable = 1;
					}
					break;
				}
			}
			if (filterStatus == -1 || filterStatus == statusTable)
			{
				final XdevButton tableButton = new XdevButton(table.getCode() + " - " + table.getName() + " (" + status + ")");
				
				if (statusTable == 0) {
					tableButton.addStyleName("tablevacant");
				} else if (statusTable == 1) {
					tableButton.addStyleName("tablehaveguest");
				} else if (statusTable == 2) {
					tableButton.addStyleName("tablewaititem");
				} else if (statusTable == 3) {
					tableButton.addStyleName("tableitemcomplete");
				}
				
				tableButton.setData(table.getId());
				this.gridLayoutTableInfo.addComponent(tableButton);
				
				tableButton.addClickListener(new Button.ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						int tableId = (int)event.getButton().getData();
						LoadDataForSelectedTable(tableId);
					}
				});
			}
		}
	}
	
	private void LoadDataForSelectedTable(int tableId) {
		DinerTableDAO tableDao = new DinerTableDAO();
		DinerTable selectedTable = tableDao.find(tableId);
		DinerSalesOrderDAO salesOrderDao = new DinerSalesOrderDAO();
		textFieldTableCode.setValue(selectedTable.getCode());
		textFieldTableCode.setData(selectedTable.getId());
		DinerSalesOrder tableSalesOrder = salesOrderDao.GetInProgressSalesOrderForTable(tableId);
		if (tableSalesOrder != null) {
			textFieldOrderCode.setValue(tableSalesOrder.getCode());
			textFieldOrderCode.setData(tableSalesOrder.getId());
			List<DinerSalesOrderItem> items = DAOs.get(DinerSalesOrderItemDAO.class).GetItemFromSalesOrder(tableSalesOrder);
			tableItems.setContainerDataSource(
					DinerSalesOrderItem.class, 
					items,
					NestedProperty.of("item.name", String.class));
			tableItems.setVisibleColumns("item.name", "price", "quantity", "status");
			tableItems.setColumnHeader("item.name", "Món");
			tableItems.setColumnHeader("price", "Giá");
			tableItems.setColumnHeader("quantity", "SL");
			tableItems.setColumnHeader("status", "Trạng thái");
			
			Double total = 0.0;
			for (DinerSalesOrderItem dinerSalesOrderItem : items) {
				if (dinerSalesOrderItem.getStatus() != DinerSalesOrderItemStatusEnum.Canceled) {
					total += dinerSalesOrderItem.getQuantity() * dinerSalesOrderItem.getPrice();
				}
			}
			textFieldTotal.setValue(String.valueOf(total));
		}
		else {
			textFieldOrderCode.setValue("Chưa có Order");
			textFieldOrderCode.setData(-1);
		}
	}
	

	private void RebuildAddItemColumns() {
		this.tableChooseItem.setVisibleColumns("code", "name", "price", "outOfOrder");
		this.tableChooseItem.setColumnHeader("code", "Mã");
		this.tableChooseItem.setColumnHeader("name", "Tên");
		this.tableChooseItem.setColumnHeader("price", "Giá");
		this.tableChooseItem.setColumnHeader("outOfOrder", "Hết món");
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAddItem}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonAddItem_buttonClick(Button.ClickEvent event) {
		int tableId = (int)textFieldTableCode.getData();
		if(tableId == -1) {
			Notification.show("Chưa chọn bàn",
	                  "Vui lòng chọn bàn",
	                  Notification.Type.WARNING_MESSAGE);
		}
		else {
			int orderId = (int)textFieldOrderCode.getData();
			BeanItem<DinerItem> selectedBeanItem = tableChooseItem.getSelectedItem();
			DinerItem selectedItem = null;
			if (selectedBeanItem != null) {
				selectedItem = selectedBeanItem.getBean();
			}
			if (selectedItem != null && selectedBeanItem != null) {
				int itemQuantity = Integer.parseInt(textFieldQuantity.getValue());
				// If item selected
				if (itemQuantity > 0) {
					if (orderId == -1) {
						Date issueDate = new Date();
						// Get table
						DinerTableDAO tableDao = new DinerTableDAO();
						DinerTable table = tableDao.find(tableId);
						// Create new order
						DinerSalesOrderDAO salesOrderDao = new DinerSalesOrderDAO();
						EmployeeDAO employeeDao = new EmployeeDAO();
						DinerSalesOrderItemDAO itemDao = new DinerSalesOrderItemDAO();
						
						DinerSalesOrder newSalesOrder = new DinerSalesOrder();
						newSalesOrder.generateCode(table.getCode());
						newSalesOrder.setDinerTable(table);
						newSalesOrder.setIssueDate(issueDate);
						newSalesOrder.setIsPaid(false);
						newSalesOrder.setIsCancelled(false);
						String loggedUserName = Authentication.getUser().name();
						Employee issueBy = employeeDao.GetEmployeeByUsername(loggedUserName);
						newSalesOrder.setIssueBy(issueBy);
						newSalesOrder.setDinerTable(table);
						DinerSalesOrderItem newItem = newSalesOrder.addItem(selectedItem, itemQuantity);
						if (selectedItem.getIsCookable()) {
							newItem.setStatus(DinerSalesOrderItemStatusEnum.WaitingForCooking);
						} else {
							newItem.setStatus(DinerSalesOrderItemStatusEnum.Delivered);
						}
						salesOrderDao.save(newSalesOrder);
						itemDao.save(newItem);
						salesOrderDao.commit();
						//itemDao.commit();
						LoadDataForSelectedTable(tableId);
					} else {
						// Add item to selected order
						DinerSalesOrderDAO salesOrderDao = new DinerSalesOrderDAO();
						DinerSalesOrderItemDAO itemDao = new DinerSalesOrderItemDAO();
						DinerSalesOrder selectedOrder = salesOrderDao.find(orderId);
						DinerSalesOrderItem newItem = selectedOrder.addItem(selectedItem, itemQuantity);
						if (selectedItem.getIsCookable()) {
							newItem.setStatus(DinerSalesOrderItemStatusEnum.WaitingForCooking);
						} else {
							newItem.setStatus(DinerSalesOrderItemStatusEnum.Delivered);
						}
						itemDao.save(newItem);
						salesOrderDao.save(selectedOrder);
						salesOrderDao.commit();
						LoadDataForSelectedTable(tableId);
					}
				}
				else {
					Notification.show("Số lượng không đúng",
			                  "Số lượng phải lớn hơn 0",
			                  Notification.Type.WARNING_MESSAGE);
				}
			} else {
				// If item not selected
				Notification.show("Chưa chọn món ăn",
		                  "Vui lòng chọn món ăn",
		                  Notification.Type.WARNING_MESSAGE);
			}
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAllCategory}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonAllCategory_buttonClick(Button.ClickEvent event) {
		int categoryId = (int) event.getButton().getData();
		if (categoryId != -1) {
			this.tableChooseItem.setContainerDataSource(DinerItem.class,
					DAOs.get(DinerItemDAO.class).buildHibernateCriteriaQuery(DinerItem.class)
							.add(Restrictions.eq("category.id", categoryId)).list());
			RebuildAddItemColumns();
		} else {
			this.tableChooseItem.setContainerDataSource(DinerItem.class, DAOs.get(DinerItemDAO.class).findAll());
			RebuildAddItemColumns();
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonSave}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonSave_buttonClick(Button.ClickEvent event) {
		fieldGroupSalesOrderItem.save();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonPay}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonPay_buttonClick(Button.ClickEvent event) {
		ConfirmDialog.show(UI.getCurrent(), 
				"Xác nhận",
				"Bạn muốn thanh toán đơn hàng này?",
				"Có",
				"Không",
		        new ConfirmDialog.Listener() {

		            public void onClose(ConfirmDialog dialog) {
		                if (dialog.isConfirmed()) {
		                	
		                	Object salesOrderIdData = textFieldOrderCode.getData();
		                	if (salesOrderIdData != null) {
		                		DinerSalesOrderDAO salesOrderDao = new DinerSalesOrderDAO();
		                		int salesOrderId = (int)salesOrderIdData;
		                		DinerSalesOrder order = salesOrderDao.find(salesOrderId);
		                		order.setIsPaid(true);
		                		salesOrderDao.save(order);
		                		
		                		buttonAll.setEnabled(false);
		                		buttonVacant.setEnabled(true);
		                		buttonOccupied.setEnabled(true);
		                		buttonWaitItem.setEnabled(true);
		                		buttonItemComplete.setEnabled(true);
		                	
		                		LoadTableToGrid(-1);
		                	}
		                }
		            }
		        });
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonAll}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonAll_buttonClick(Button.ClickEvent event) {
		buttonAll.setEnabled(false);
		buttonVacant.setEnabled(true);
		buttonOccupied.setEnabled(true);
		buttonWaitItem.setEnabled(true);
		buttonItemComplete.setEnabled(true);
	
		LoadTableToGrid(-1);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonVacant}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonVacant_buttonClick(Button.ClickEvent event) {
		buttonAll.setEnabled(true);
		buttonVacant.setEnabled(false);
		buttonOccupied.setEnabled(true);
		buttonWaitItem.setEnabled(true);
		buttonItemComplete.setEnabled(true);
	
		LoadTableToGrid(0);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonOccupied}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonOccupied_buttonClick(Button.ClickEvent event) {
		buttonAll.setEnabled(true);
		buttonVacant.setEnabled(true);
		buttonOccupied.setEnabled(false);
		buttonWaitItem.setEnabled(true);
		buttonItemComplete.setEnabled(true);
	
		LoadTableToGrid(1);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonWaitItem}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonWaitItem_buttonClick(Button.ClickEvent event) {
		buttonAll.setEnabled(true);
		buttonVacant.setEnabled(true);
		buttonOccupied.setEnabled(true);
		buttonWaitItem.setEnabled(false);
		buttonItemComplete.setEnabled(true);
	
		LoadTableToGrid(2);
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonItemComplete}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonItemComplete_buttonClick(Button.ClickEvent event) {
		buttonAll.setEnabled(true);
		buttonVacant.setEnabled(true);
		buttonOccupied.setEnabled(true);
		buttonWaitItem.setEnabled(true);
		buttonItemComplete.setEnabled(false);
	
		LoadTableToGrid(3);
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.labelDsBanAn = new XdevLabel();
		this.panel = new XdevPanel();
		this.horizontalLayoutFilter = new XdevHorizontalLayout();
		this.buttonAll = new XdevButton();
		this.buttonVacant = new XdevButton();
		this.buttonOccupied = new XdevButton();
		this.buttonWaitItem = new XdevButton();
		this.buttonItemComplete = new XdevButton();
		this.panelOrder = new XdevPanel();
		this.horizontalSplitPanel = new XdevHorizontalSplitPanel();
		this.gridLayoutTableInfo = new XdevGridLayout();
		this.verticalSplitPanel = new XdevVerticalSplitPanel();
		this.gridLayoutChooseItem = new XdevGridLayout();
		this.horizontalLayoutCategory = new XdevHorizontalLayout();
		this.buttonAllCategory = new XdevButton();
		this.tableChooseItem = new XdevTable<>();
		this.textFieldQuantity = new XdevTextField();
		this.buttonAddItem = new XdevButton();
		this.gridLayoutBillInfo = new XdevGridLayout();
		this.labelSelectedTable = new XdevLabel();
		this.textFieldTableCode = new XdevTextField();
		this.labelOrderCode = new XdevLabel();
		this.textFieldOrderCode = new XdevTextField();
		this.tableItems = new XdevTable<>();
		this.comboBoxOrderItem = new XdevComboBox<>();
		this.textFieldOrderItemQuantity = new XdevTextField();
		this.textFieldOrderItemPrice = new XdevTextField();
		this.comboBoxOrderItemStatus = new XdevComboBox<>();
		this.buttonSave = new XdevButton();
		this.labelTotal = new XdevLabel();
		this.textFieldTotal = new XdevTextField();
		this.buttonPay = new XdevButton();
		this.fieldGroupSalesOrderItem = new XdevFieldGroup<>(DinerSalesOrderItem.class);
	
		this.gridLayout.setRows(2);
		this.labelDsBanAn.setValue("Danh sách bàn ăn");
		this.horizontalLayoutFilter.setMargin(new MarginInfo(false, true, false, true));
		this.buttonAll.setCaption("Tất cả");
		this.buttonAll.setEnabled(false);
		this.buttonVacant.setCaption("Trống");
		this.buttonVacant.setStyleName("tablevacant");
		this.buttonOccupied.setCaption("Có khách");
		this.buttonOccupied.setStyleName("tablehaveguest");
		this.buttonWaitItem.setCaption("Đợi món");
		this.buttonWaitItem.setStyleName("tablewaititem");
		this.buttonItemComplete.setCaption("Món xong");
		this.buttonItemComplete.setStyleName("tableitemcomplete");
		this.panelOrder.setTabIndex(0);
		this.gridLayoutTableInfo.setColumns(3);
		this.gridLayoutTableInfo.setRows(12);
		this.gridLayoutChooseItem.setColumns(2);
		this.gridLayoutChooseItem.setRows(2);
		this.horizontalLayoutCategory.setMargin(new MarginInfo(false));
		this.buttonAllCategory.setData(-1);
		this.buttonAllCategory.setCaption("Tất cả");
		this.tableChooseItem.setContainerDataSource(DinerItem.class, DAOs.get(DinerItemDAO.class).findAll());
		this.tableChooseItem.setVisibleColumns("code", "name", "price", "outOfOrder");
		this.tableChooseItem.setColumnHeader("code", "Mã");
		this.tableChooseItem.setColumnHeader("name", "Tên");
		this.tableChooseItem.setColumnHeader("price", "Giá");
		this.tableChooseItem.setColumnHeader("outOfOrder", "Hết món");
		this.textFieldQuantity.setInputPrompt("Số lượng");
		this.buttonAddItem.setCaption("Thêm món");
		this.gridLayoutBillInfo.setColumns(2);
		this.gridLayoutBillInfo.setRows(2);
		this.labelSelectedTable.setValue("Bàn");
		this.textFieldTableCode.setEnabled(false);
		this.labelOrderCode.setValue("Mã Order");
		this.textFieldOrderCode.setEnabled(false);
		this.tableItems.setContainerDataSource(DinerSalesOrderItem.class, false,
				NestedProperty.of("item.name", String.class));
		this.tableItems.setVisibleColumns("item.name", "price", "quantity", "status");
		this.tableItems.setColumnHeader("item.name", "Món");
		this.tableItems.setColumnHeader("price", "Giá");
		this.tableItems.setColumnHeader("quantity", "SL");
		this.tableItems.setColumnHeader("status", "Trạng thái");
		this.comboBoxOrderItem.setItemCaptionFromAnnotation(false);
		this.comboBoxOrderItem.setContainerDataSource(DinerItem.class, DAOs.get(DinerItemDAO.class).findAll());
		this.comboBoxOrderItem.setItemCaptionPropertyId("name");
		this.buttonSave.setCaption("Lưu");
		this.labelTotal.setValue("Tổng tiền");
		this.textFieldTotal.setEnabled(false);
		this.buttonPay.setCaption("Thanh toán");
		this.fieldGroupSalesOrderItem.bind(this.comboBoxOrderItem, "item");
		this.fieldGroupSalesOrderItem.bind(this.comboBoxOrderItemStatus, "status");
		this.fieldGroupSalesOrderItem.bind(this.textFieldOrderItemQuantity, "quantity");
		this.fieldGroupSalesOrderItem.bind(this.textFieldOrderItemPrice, "price");
	
		XDEV.bindForm(tableItems, fieldGroupSalesOrderItem);
	
		this.buttonAll.setSizeUndefined();
		this.horizontalLayoutFilter.addComponent(this.buttonAll);
		this.horizontalLayoutFilter.setComponentAlignment(this.buttonAll, Alignment.MIDDLE_CENTER);
		this.buttonVacant.setSizeUndefined();
		this.horizontalLayoutFilter.addComponent(this.buttonVacant);
		this.horizontalLayoutFilter.setComponentAlignment(this.buttonVacant, Alignment.MIDDLE_CENTER);
		this.buttonOccupied.setSizeUndefined();
		this.horizontalLayoutFilter.addComponent(this.buttonOccupied);
		this.horizontalLayoutFilter.setComponentAlignment(this.buttonOccupied, Alignment.MIDDLE_CENTER);
		this.buttonWaitItem.setSizeUndefined();
		this.horizontalLayoutFilter.addComponent(this.buttonWaitItem);
		this.horizontalLayoutFilter.setComponentAlignment(this.buttonWaitItem, Alignment.MIDDLE_CENTER);
		this.buttonItemComplete.setSizeUndefined();
		this.horizontalLayoutFilter.addComponent(this.buttonItemComplete);
		this.horizontalLayoutFilter.setComponentAlignment(this.buttonItemComplete, Alignment.MIDDLE_CENTER);
		CustomComponent horizontalLayoutFilter_spacer = new CustomComponent();
		horizontalLayoutFilter_spacer.setSizeFull();
		this.horizontalLayoutFilter.addComponent(horizontalLayoutFilter_spacer);
		this.horizontalLayoutFilter.setExpandRatio(horizontalLayoutFilter_spacer, 1.0F);
		this.horizontalLayoutFilter.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayoutFilter.setHeight(-1, Unit.PIXELS);
		this.panel.setContent(this.horizontalLayoutFilter);
		this.buttonAllCategory.setSizeUndefined();
		this.horizontalLayoutCategory.addComponent(this.buttonAllCategory);
		this.horizontalLayoutCategory.setComponentAlignment(this.buttonAllCategory, Alignment.MIDDLE_CENTER);
		CustomComponent horizontalLayoutCategory_spacer = new CustomComponent();
		horizontalLayoutCategory_spacer.setSizeFull();
		this.horizontalLayoutCategory.addComponent(horizontalLayoutCategory_spacer);
		this.horizontalLayoutCategory.setExpandRatio(horizontalLayoutCategory_spacer, 1.0F);
		this.gridLayoutChooseItem.setColumns(2);
		this.gridLayoutChooseItem.setRows(3);
		this.horizontalLayoutCategory.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayoutCategory.setHeight(-1, Unit.PIXELS);
		this.gridLayoutChooseItem.addComponent(this.horizontalLayoutCategory, 0, 0);
		this.tableChooseItem.setSizeFull();
		this.gridLayoutChooseItem.addComponent(this.tableChooseItem, 0, 1, 1, 1);
		this.gridLayoutChooseItem.setComponentAlignment(this.tableChooseItem, Alignment.MIDDLE_CENTER);
		this.textFieldQuantity.setSizeUndefined();
		this.gridLayoutChooseItem.addComponent(this.textFieldQuantity, 0, 2);
		this.gridLayoutChooseItem.setComponentAlignment(this.textFieldQuantity, Alignment.TOP_RIGHT);
		this.buttonAddItem.setSizeUndefined();
		this.gridLayoutChooseItem.addComponent(this.buttonAddItem, 1, 2);
		this.gridLayoutChooseItem.setColumnExpandRatio(0, 0.1F);
		this.gridLayoutChooseItem.setColumnExpandRatio(1, 0.1F);
		this.gridLayoutChooseItem.setRowExpandRatio(1, 0.1F);
		this.gridLayoutBillInfo.setColumns(3);
		this.gridLayoutBillInfo.setRows(11);
		this.labelSelectedTable.setSizeUndefined();
		this.gridLayoutBillInfo.addComponent(this.labelSelectedTable, 0, 0);
		this.textFieldTableCode.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTableCode.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.textFieldTableCode, 1, 0, 2, 0);
		this.labelOrderCode.setSizeUndefined();
		this.gridLayoutBillInfo.addComponent(this.labelOrderCode, 0, 1);
		this.textFieldOrderCode.setWidth(100, Unit.PERCENTAGE);
		this.textFieldOrderCode.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.textFieldOrderCode, 1, 1, 2, 1);
		this.tableItems.setSizeFull();
		this.gridLayoutBillInfo.addComponent(this.tableItems, 0, 2, 1, 7);
		this.comboBoxOrderItem.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxOrderItem.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.comboBoxOrderItem, 2, 3);
		this.textFieldOrderItemQuantity.setWidth(100, Unit.PERCENTAGE);
		this.textFieldOrderItemQuantity.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.textFieldOrderItemQuantity, 2, 4);
		this.textFieldOrderItemPrice.setWidth(100, Unit.PERCENTAGE);
		this.textFieldOrderItemPrice.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.textFieldOrderItemPrice, 2, 5);
		this.comboBoxOrderItemStatus.setWidth(100, Unit.PERCENTAGE);
		this.comboBoxOrderItemStatus.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.comboBoxOrderItemStatus, 2, 6);
		this.buttonSave.setSizeUndefined();
		this.gridLayoutBillInfo.addComponent(this.buttonSave, 2, 7);
		this.gridLayoutBillInfo.setComponentAlignment(this.buttonSave, Alignment.MIDDLE_RIGHT);
		this.labelTotal.setSizeUndefined();
		this.gridLayoutBillInfo.addComponent(this.labelTotal, 0, 8);
		this.textFieldTotal.setWidth(100, Unit.PERCENTAGE);
		this.textFieldTotal.setHeight(-1, Unit.PIXELS);
		this.gridLayoutBillInfo.addComponent(this.textFieldTotal, 1, 8, 2, 8);
		this.buttonPay.setSizeUndefined();
		this.gridLayoutBillInfo.addComponent(this.buttonPay, 0, 9, 1, 9);
		this.gridLayoutBillInfo.setColumnExpandRatio(0, 0.1F);
		this.gridLayoutBillInfo.setColumnExpandRatio(1, 0.1F);
		this.gridLayoutBillInfo.setColumnExpandRatio(2, 0.1F);
		CustomComponent gridLayoutBillInfo_vSpacer = new CustomComponent();
		gridLayoutBillInfo_vSpacer.setSizeFull();
		this.gridLayoutBillInfo.addComponent(gridLayoutBillInfo_vSpacer, 0, 10, 2, 10);
		this.gridLayoutBillInfo.setRowExpandRatio(10, 1.0F);
		this.gridLayoutChooseItem.setSizeFull();
		this.verticalSplitPanel.setFirstComponent(this.gridLayoutChooseItem);
		this.gridLayoutBillInfo.setSizeFull();
		this.verticalSplitPanel.setSecondComponent(this.gridLayoutBillInfo);
		this.verticalSplitPanel.setSplitPosition(38, Unit.PERCENTAGE);
		this.gridLayoutTableInfo.setSizeFull();
		this.horizontalSplitPanel.setFirstComponent(this.gridLayoutTableInfo);
		this.verticalSplitPanel.setSizeFull();
		this.horizontalSplitPanel.setSecondComponent(this.verticalSplitPanel);
		this.horizontalSplitPanel.setSplitPosition(67, Unit.PERCENTAGE);
		this.horizontalSplitPanel.setSizeFull();
		this.panelOrder.setContent(this.horizontalSplitPanel);
		this.gridLayout.setColumns(1);
		this.gridLayout.setRows(3);
		this.labelDsBanAn.setSizeUndefined();
		this.gridLayout.addComponent(this.labelDsBanAn, 0, 0);
		this.panel.setWidth(100, Unit.PERCENTAGE);
		this.panel.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.panel, 0, 1);
		this.panelOrder.setSizeFull();
		this.gridLayout.addComponent(this.panelOrder, 0, 2);
		this.gridLayout.setColumnExpandRatio(0, 0.1F);
		this.gridLayout.setRowExpandRatio(2, 0.1F);
		this.gridLayout.setSizeFull();
		this.setContent(this.gridLayout);
		this.setSizeFull();
	
		buttonAll.addClickListener(event -> this.buttonAll_buttonClick(event));
		buttonVacant.addClickListener(event -> this.buttonVacant_buttonClick(event));
		buttonOccupied.addClickListener(event -> this.buttonOccupied_buttonClick(event));
		buttonWaitItem.addClickListener(event -> this.buttonWaitItem_buttonClick(event));
		buttonItemComplete.addClickListener(event -> this.buttonItemComplete_buttonClick(event));
		buttonAllCategory.addClickListener(event -> this.buttonAllCategory_buttonClick(event));
		buttonAddItem.addClickListener(event -> this.buttonAddItem_buttonClick(event));
		buttonSave.addClickListener(event -> this.buttonSave_buttonClick(event));
		buttonPay.addClickListener(event -> this.buttonPay_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevComboBox<DinerSalesOrderItemStatusEnum> comboBoxOrderItemStatus;
	private XdevVerticalSplitPanel verticalSplitPanel;
	private XdevComboBox<DinerItem> comboBoxOrderItem;
	private XdevLabel labelDsBanAn, labelSelectedTable, labelOrderCode, labelTotal;
	private XdevHorizontalSplitPanel horizontalSplitPanel;
	private XdevTextField textFieldQuantity, textFieldTableCode, textFieldOrderCode, textFieldOrderItemQuantity,
			textFieldOrderItemPrice, textFieldTotal;
	private XdevButton buttonAll, buttonVacant, buttonOccupied, buttonWaitItem, buttonItemComplete,
			buttonAllCategory, buttonAddItem, buttonSave, buttonPay;
	private XdevFieldGroup<DinerSalesOrderItem> fieldGroupSalesOrderItem;
	private XdevGridLayout gridLayout, gridLayoutTableInfo, gridLayoutChooseItem, gridLayoutBillInfo;
	private XdevPanel panel, panelOrder;
	private XdevTable<DinerItem> tableChooseItem;
	private XdevHorizontalLayout horizontalLayoutFilter, horizontalLayoutCategory;
	private XdevTable<DinerSalesOrderItem> tableItems; // </generated-code>


}
