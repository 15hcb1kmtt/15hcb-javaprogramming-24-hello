package com.khoitam.dinermanagement.ui.report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.khoitam.dinermanagement.business.report.RevenueReportData;
import com.khoitam.dinermanagement.dal.DinerSalesOrderDAO;
import com.khoitam.dinermanagement.entities.DinerSalesOrder;
import com.khoitam.dinermanagement.entities.DinerSalesOrderItem;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevHorizontalLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPopupDateField;
import com.xdev.ui.XdevTextField;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.combobox.XdevComboBox;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class RevenueReportView extends XdevView {

	/**
	 * 
	 */
	public RevenueReportView() {
		super();
		this.initUI();
		PopulateCombobox();
	}
	
	public void PopulateCombobox() {		
		comboBoxMonth.addItem("1");
		comboBoxMonth.addItem("2");
		comboBoxMonth.addItem("3");
		comboBoxMonth.addItem("4");
		comboBoxMonth.addItem("5");
		comboBoxMonth.addItem("6");
		comboBoxMonth.addItem("7");
		comboBoxMonth.addItem("8");
		comboBoxMonth.addItem("9");
		comboBoxMonth.addItem("10");
		comboBoxMonth.addItem("11");
		comboBoxMonth.addItem("12");
		
		comboBoxQuater.addItem("1");
		comboBoxQuater.addItem("2");
		comboBoxQuater.addItem("3");
		comboBoxQuater.addItem("4");
	}
	
	private void PopulateRevenueData(Date fromTime, Date toTime) {
		Double totalAmount = 0.0;
		List<RevenueReportData> beans = new ArrayList<RevenueReportData>();
		DinerSalesOrderDAO salesOrderDAO = new DinerSalesOrderDAO();
		List<DinerSalesOrder> salesOrders = salesOrderDAO.GetCompletedSalesOrder(fromTime, toTime);
		for (DinerSalesOrder dinerSalesOrder : salesOrders) {
			boolean isBreak = false;
			for (RevenueReportData existedBean : beans) {
				if (existedBean.getOrderId() == dinerSalesOrder.getCode()) {
					isBreak = true;
					break;
				}
			}
			if (isBreak) {
				break;
			}
			RevenueReportData bean = new RevenueReportData();
			bean.setOrderId(dinerSalesOrder.getCode());
			Double amount = 0.0;
			for (DinerSalesOrderItem item : dinerSalesOrder.getDinerSalesOrderItemsForSalesOrder()) {
				amount += item.getPrice() * item.getQuantity();
			}
			bean.setAmount(amount);
			bean.setDate(new SimpleDateFormat("dd/MM (kk:mm)").format(dinerSalesOrder.getIssueDate()));
			totalAmount += amount;
			beans.add(bean);
		}
		textField.setValue(totalAmount.toString());
		BindReportData(beans);
	}
	
	private void BindReportData(List<RevenueReportData> dataSource)
	{
		this.tableRevenueData.setContainerDataSource(RevenueReportData.class, dataSource);
		//this.tableScheduleData.setContainerDataSource(dataSource);
		this.tableRevenueData.setVisibleColumns("date", "orderId", "amount");
		this.tableRevenueData.setColumnHeader("date", "Ngày");
		this.tableRevenueData.setColumnHeader("orderId", "Mã order");
		this.tableRevenueData.setColumnHeader("amount", "Số tiền");
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxMonth}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate
	 */
	private void comboBoxMonth_valueChange(Property.ValueChangeEvent event) {
		Date year = popupDateFieldYear.getValue();
		if (year != null) {
			String value = (String)comboBoxMonth.getValue();
			int valueMonth = Integer.parseInt(value) - 1;
			
			Calendar cal = new Calendar.Builder().setInstant(year).build();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.clear(Calendar.MINUTE);
			cal.clear(Calendar.SECOND);
			cal.clear(Calendar.MILLISECOND);
			cal.set(Calendar.MONTH, valueMonth);
			
			Calendar calEnd = (Calendar)cal.clone();
			calEnd.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			calEnd.set(Calendar.HOUR_OF_DAY, 23);
			calEnd.set(Calendar.MINUTE, 59);
			calEnd.set(Calendar.MILLISECOND, 59);
			
			PopulateRevenueData(cal.getTime(), calEnd.getTime());
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevComboBox}
	 * {@link #comboBoxQuater}.
	 *
	 * @see Property.ValueChangeListener#valueChange(Property.ValueChangeEvent)
	 * @eventHandlerDelegate
	 */
	private void comboBoxQuater_valueChange(Property.ValueChangeEvent event) {
		Date year = popupDateFieldYear.getValue();
		if (year != null) {
			String value = (String)comboBoxQuater.getValue();
			int valueQuarter = Integer.parseInt(value) - 1;
			
			Calendar cal = new Calendar.Builder().setInstant(year).build();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.clear(Calendar.MINUTE);
			cal.clear(Calendar.SECOND);
			cal.clear(Calendar.MILLISECOND);
			cal.set(Calendar.MONTH, (valueQuarter * 4));
			
			Calendar calEnd = (Calendar)cal.clone();
			calEnd.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 4);
			calEnd.set(Calendar.DAY_OF_MONTH, 1);
			calEnd.set(Calendar.HOUR_OF_DAY, 0);
			calEnd.set(Calendar.MINUTE, 0);
			calEnd.set(Calendar.MILLISECOND, 0);
			
			PopulateRevenueData(cal.getTime(), calEnd.getTime());
		}
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.horizontalLayout = new XdevHorizontalLayout();
		this.comboBoxMonth = new XdevComboBox<>();
		this.comboBoxQuater = new XdevComboBox<>();
		this.popupDateFieldYear = new XdevPopupDateField();
		this.tableRevenueData = new XdevTable<>();
		this.horizontalLayout2 = new XdevHorizontalLayout();
		this.label = new XdevLabel();
		this.textField = new XdevTextField();
	
		this.comboBoxMonth.setItemCaptionFromAnnotation(false);
		this.comboBoxMonth.setInputPrompt("Chọn tháng");
		this.comboBoxMonth.setItemCaptionValue("");
		this.comboBoxQuater.setInputPrompt("Chọn quý");
		this.popupDateFieldYear.setInputPrompt("Nhập năm");
		this.popupDateFieldYear.setResolution(Resolution.YEAR);
		this.label.setValue("Tổng doanh thu");
		this.textField.setCaption("");
	
		this.comboBoxMonth.setWidth(150, Unit.PIXELS);
		this.comboBoxMonth.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.comboBoxMonth);
		this.comboBoxQuater.setWidth(150, Unit.PIXELS);
		this.comboBoxQuater.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.comboBoxQuater);
		this.popupDateFieldYear.setWidth(150, Unit.PIXELS);
		this.popupDateFieldYear.setHeight(-1, Unit.PIXELS);
		this.horizontalLayout.addComponent(this.popupDateFieldYear);
		CustomComponent horizontalLayout_spacer = new CustomComponent();
		horizontalLayout_spacer.setSizeFull();
		this.horizontalLayout.addComponent(horizontalLayout_spacer);
		this.horizontalLayout.setExpandRatio(horizontalLayout_spacer, 1.0F);
		this.label.setWidth(-1, Unit.PIXELS);
		this.label.setHeight(100, Unit.PERCENTAGE);
		this.horizontalLayout2.addComponent(this.label);
		this.horizontalLayout2.setComponentAlignment(this.label, Alignment.BOTTOM_CENTER);
		this.textField.setSizeFull();
		this.horizontalLayout2.addComponent(this.textField);
		this.horizontalLayout2.setComponentAlignment(this.textField, Alignment.TOP_CENTER);
		this.horizontalLayout2.setExpandRatio(this.textField, 0.1F);
		this.gridLayout.setColumns(1);
		this.gridLayout.setRows(3);
		this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.horizontalLayout, 0, 0);
		this.tableRevenueData.setSizeFull();
		this.gridLayout.addComponent(this.tableRevenueData, 0, 1);
		this.horizontalLayout2.setWidth(100, Unit.PERCENTAGE);
		this.horizontalLayout2.setHeight(-1, Unit.PIXELS);
		this.gridLayout.addComponent(this.horizontalLayout2, 0, 2);
		this.gridLayout.setColumnExpandRatio(0, 0.1F);
		this.gridLayout.setRowExpandRatio(1, 0.1F);
		this.gridLayout.setSizeFull();
		this.setContent(this.gridLayout);
		this.setSizeFull();
	
		comboBoxMonth.addValueChangeListener(event -> this.comboBoxMonth_valueChange(event));
		this.comboBoxQuater.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				RevenueReportView.this.comboBoxQuater_valueChange(event);
			}
		});
	} // </generated-code>

	// <generated-code name="variables">
	private XdevGridLayout gridLayout;
	private XdevComboBox<String> comboBoxMonth, comboBoxQuater;
	private XdevHorizontalLayout horizontalLayout, horizontalLayout2;
	private XdevPopupDateField popupDateFieldYear;
	private XdevTable<RevenueReportData> tableRevenueData;
	private XdevLabel label;
	private XdevTextField textField; // </generated-code>


}
