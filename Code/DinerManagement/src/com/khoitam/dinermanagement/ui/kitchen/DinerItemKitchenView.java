package com.khoitam.dinermanagement.ui.kitchen;

import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevView;
import com.xdev.ui.entitycomponent.table.XdevTable;

public class DinerItemKitchenView extends XdevView {

	/**
	 * 
	 */
	public DinerItemKitchenView() {
		super();
		this.initUI();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.gridLayout = new XdevGridLayout();
		this.table = new XdevTable<>();
		this.button = new XdevButton();
		this.table2 = new XdevTable<>();
		this.button3 = new XdevButton();
		this.table3 = new XdevTable<>();
		this.button2 = new XdevButton();
		this.button4 = new XdevButton();
	
		this.gridLayout.setColumns(5);
		this.gridLayout.setRows(2);
		this.button.setCaption("Button");
		this.button3.setCaption("Button");
		this.button2.setCaption("Button");
		this.button4.setCaption("Button");
	
		this.gridLayout.setColumns(5);
		this.gridLayout.setRows(2);
		this.table.setSizeFull();
		this.gridLayout.addComponent(this.table, 0, 0, 0, 1);
		this.button.setSizeUndefined();
		this.gridLayout.addComponent(this.button, 1, 0);
		this.table2.setSizeFull();
		this.gridLayout.addComponent(this.table2, 2, 0, 2, 1);
		this.button3.setSizeUndefined();
		this.gridLayout.addComponent(this.button3, 3, 0);
		this.table3.setSizeFull();
		this.gridLayout.addComponent(this.table3, 4, 0, 4, 1);
		this.button2.setSizeUndefined();
		this.gridLayout.addComponent(this.button2, 1, 1);
		this.button4.setSizeUndefined();
		this.gridLayout.addComponent(this.button4, 3, 1);
		this.gridLayout.setColumnExpandRatio(0, 1.0F);
		this.gridLayout.setColumnExpandRatio(2, 1.0F);
		this.gridLayout.setColumnExpandRatio(4, 1.0F);
		this.gridLayout.setRowExpandRatio(0, 1.0F);
		this.gridLayout.setSizeFull();
		this.setContent(this.gridLayout);
		this.setSizeFull();
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton button, button3, button2, button4;
	private XdevGridLayout gridLayout;
	private XdevTable<?> table, table2, table3; // </generated-code>


}
