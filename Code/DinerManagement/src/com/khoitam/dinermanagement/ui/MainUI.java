
package com.khoitam.dinermanagement.ui;

import java.security.NoSuchAlgorithmException;

import org.hibernate.criterion.Restrictions;

import com.khoitam.dinermanagement.dal.EmployeeDAO;
import com.khoitam.dinermanagement.entities.Employee;
import com.khoitam.dinermanagement.ui.logon.LogonView;
import com.khoitam.dinermanagement.ui.personinfo.EmployeeInfoView;
import com.khoitam.dinermanagement.ui.report.RevenueReportView;
import com.khoitam.dinermanagement.ui.sale.TableOrderView;
import com.khoitam.dinermanagement.ui.setting.SettingCRUDDataView;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HasComponents;
import com.xdev.security.authentication.ui.Authentication;
import com.xdev.security.authentication.ui.XdevAuthenticationNavigator;
import com.xdev.ui.XdevAccordion;
import com.xdev.ui.XdevBorderLayout;
import com.xdev.ui.XdevButton;
import com.xdev.ui.XdevGridLayout;
import com.xdev.ui.XdevLabel;
import com.xdev.ui.XdevPanel;
import com.xdev.ui.XdevUI;
import com.xdev.ui.XdevVerticalLayout;
import com.xdev.ui.navigation.Navigation;

@Push
@Theme("DinerManagement")
public class MainUI extends XdevUI {
	public MainUI() {
		super();
	}
	
	private void updateEmployeeDatabase() {
		EmployeeDAO eD = new EmployeeDAO();
		Employee existedAdmin = (Employee)eD.buildHibernateCriteriaQuery(Employee.class)
			.add( Restrictions.eq("userName", "Admin") )
		    .uniqueResult();
		
		if (existedAdmin == null) {
			existedAdmin = new Employee();
			existedAdmin.setUserName("Admin");
			try {
				existedAdmin.setPassword(existedAdmin.hash256("123456"));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			eD.save(existedAdmin);
			eD.commit();
		}	
	}
	
	private void updateGUIMargin() {
		logonLayout.setMargin(new MarginInfo(false, true, false, true));
		userMenu.setMargin(new MarginInfo(true, true, true, true));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(VaadinRequest request) {
		this.initUI();
		updateEmployeeDatabase();
		updateGUIMargin();
	}

	/**
	 * Event handler delegate method for the {@link XdevGridLayout}
	 * {@link #viewPlaceholder}.
	 *
	 * @see HasComponents.ComponentAttachListener#componentAttachedToContainer(HasComponents.ComponentAttachEvent)
	 * @eventHandlerDelegate
	 */
	private void viewPlaceholder_componentAttachedToContainer(HasComponents.ComponentAttachEvent event) {
		if (event.getAttachedComponent() instanceof LoginView) {
			if (!Authentication.isUserLoggedIn()) {
				panelMenu.setVisible(false);
			}
		}
		else {
			if (!panelMenu.isVisible()) {
				labelHelloUser.setValue("Xin chào " + Authentication.getUser().name());
				panelMenu.setVisible(true);
			}
		}
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonLogout}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonLogout_buttonClick(Button.ClickEvent event) {
		Authentication.logout();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonMyInfo}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonMyInfo_buttonClick(Button.ClickEvent event) {
		Navigation.to("EmployeeInfoView").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonPOS}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonPOS_buttonClick(Button.ClickEvent event) {
		Navigation.to("TableOrderView").parameter("LoadTable", true).navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonKitchen}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonKitchen_buttonClick(Button.ClickEvent event) {
	
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigItem}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigItem_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "DinerItem").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigItemCategory}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigItemCategory_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "DinerItemCategory").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigEmployee}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigEmployee_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "Employee").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigWorkingShift}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigWorkingShift_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "WorkingShift").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigDinerTable}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigDinerTable_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "DinerTable").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigCertificate}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigCertificate_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "Certificate").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigTandS}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigTandS_buttonClick(Button.ClickEvent event) {
		Navigation.to("CRUDView").parameter("settingViewProvider", "ToolsAndSupplies").navigate();
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonConfigSystemOther}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonConfigSystemOther_buttonClick(Button.ClickEvent event) {
		
	}

	/**
	 * Event handler delegate method for the {@link XdevButton}
	 * {@link #buttonRevenueReport}.
	 *
	 * @see Button.ClickListener#buttonClick(Button.ClickEvent)
	 * @eventHandlerDelegate
	 */
	private void buttonRevenueReport_buttonClick(Button.ClickEvent event) {
		Navigation.to("RevenueReport").navigate();
	}

	/*
	 * WARNING: Do NOT edit!<br>The content of this method is always regenerated
	 * by the UI designer.
	 */
	// <generated-code name="initUI">
	private void initUI() {
		this.logonLayout = new XdevBorderLayout();
		this.viewPlaceholder = new XdevGridLayout();
		this.panelMenu = new XdevPanel();
		this.userMenu = new XdevVerticalLayout();
		this.labelHelloUser = new XdevLabel();
		this.accordionNavigation = new XdevAccordion();
		this.layoutProfile = new XdevVerticalLayout();
		this.buttonMyInfo = new XdevButton();
		this.layoutDiner = new XdevVerticalLayout();
		this.buttonPOS = new XdevButton();
		this.buttonKitchen = new XdevButton();
		this.layoutReport = new XdevVerticalLayout();
		this.buttonRevenueReport = new XdevButton();
		this.layoutConfigMenu = new XdevVerticalLayout();
		this.buttonConfigItem = new XdevButton();
		this.buttonConfigItemCategory = new XdevButton();
		this.layoutHumanResource = new XdevVerticalLayout();
		this.buttonConfigEmployee = new XdevButton();
		this.buttonConfigWorkingShift = new XdevButton();
		this.layoutConfigOther = new XdevVerticalLayout();
		this.buttonConfigDinerTable = new XdevButton();
		this.buttonConfigCertificate = new XdevButton();
		this.buttonConfigTandS = new XdevButton();
		this.buttonConfigSystemOther = new XdevButton();
		this.buttonLogout = new XdevButton();
		this.navigation = new XdevAuthenticationNavigator(this, this.viewPlaceholder);
	
		this.setNavigator(this.navigation);
		this.viewPlaceholder.setMargin(new MarginInfo(false, true, false, true));
		this.labelHelloUser.setValue("Xin chào User");
		this.buttonMyInfo.setCaption("Cá nhân");
		this.buttonPOS.setCaption("Bán hàng");
		this.buttonKitchen.setCaption("Bếp");
		this.buttonRevenueReport.setCaption("Doanh thu");
		this.buttonConfigItem.setCaption("Món ăn");
		this.buttonConfigItemCategory.setCaption("Loại món");
		this.buttonConfigEmployee.setCaption("Nhân viên");
		this.buttonConfigWorkingShift.setCaption("Ca làm việc");
		this.buttonConfigDinerTable.setCaption("Bàn ăn");
		this.buttonConfigCertificate.setCaption("Bằng cấp");
		this.buttonConfigTandS.setCaption("Dụng cụ");
		this.buttonConfigSystemOther.setCaption("Hệ thống");
		this.buttonLogout.setCaption("Đăng xuất");
		this.navigation.setRedirectViewName("LogonView");
		this.navigation.addView("", LoginView.class);
		this.navigation.addView("CRUDView", SettingCRUDDataView.class);
		this.navigation.addView("LogonView", LogonView.class);
		this.navigation.addView("TableOrderView", TableOrderView.class);
		this.navigation.addView("EmployeeInfoView", EmployeeInfoView.class);
		this.navigation.addView("RevenueReport", RevenueReportView.class);
	
		this.buttonMyInfo.setWidth(100, Unit.PERCENTAGE);
		this.buttonMyInfo.setHeight(-1, Unit.PIXELS);
		this.layoutProfile.addComponent(this.buttonMyInfo);
		this.layoutProfile.setComponentAlignment(this.buttonMyInfo, Alignment.MIDDLE_CENTER);
		CustomComponent layoutProfile_spacer = new CustomComponent();
		layoutProfile_spacer.setSizeFull();
		this.layoutProfile.addComponent(layoutProfile_spacer);
		this.layoutProfile.setExpandRatio(layoutProfile_spacer, 1.0F);
		this.buttonPOS.setWidth(100, Unit.PERCENTAGE);
		this.buttonPOS.setHeight(-1, Unit.PIXELS);
		this.layoutDiner.addComponent(this.buttonPOS);
		this.layoutDiner.setComponentAlignment(this.buttonPOS, Alignment.MIDDLE_CENTER);
		this.buttonKitchen.setWidth(100, Unit.PERCENTAGE);
		this.buttonKitchen.setHeight(-1, Unit.PIXELS);
		this.layoutDiner.addComponent(this.buttonKitchen);
		this.layoutDiner.setComponentAlignment(this.buttonKitchen, Alignment.MIDDLE_CENTER);
		CustomComponent layoutDiner_spacer = new CustomComponent();
		layoutDiner_spacer.setSizeFull();
		this.layoutDiner.addComponent(layoutDiner_spacer);
		this.layoutDiner.setExpandRatio(layoutDiner_spacer, 1.0F);
		this.buttonRevenueReport.setWidth(100, Unit.PERCENTAGE);
		this.buttonRevenueReport.setHeight(-1, Unit.PIXELS);
		this.layoutReport.addComponent(this.buttonRevenueReport);
		this.layoutReport.setComponentAlignment(this.buttonRevenueReport, Alignment.MIDDLE_CENTER);
		CustomComponent layoutReport_spacer = new CustomComponent();
		layoutReport_spacer.setSizeFull();
		this.layoutReport.addComponent(layoutReport_spacer);
		this.layoutReport.setExpandRatio(layoutReport_spacer, 1.0F);
		this.buttonConfigItem.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigItem.setHeight(-1, Unit.PIXELS);
		this.layoutConfigMenu.addComponent(this.buttonConfigItem);
		this.layoutConfigMenu.setComponentAlignment(this.buttonConfigItem, Alignment.MIDDLE_CENTER);
		this.buttonConfigItemCategory.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigItemCategory.setHeight(-1, Unit.PIXELS);
		this.layoutConfigMenu.addComponent(this.buttonConfigItemCategory);
		this.layoutConfigMenu.setComponentAlignment(this.buttonConfigItemCategory, Alignment.MIDDLE_CENTER);
		CustomComponent layoutConfigMenu_spacer = new CustomComponent();
		layoutConfigMenu_spacer.setSizeFull();
		this.layoutConfigMenu.addComponent(layoutConfigMenu_spacer);
		this.layoutConfigMenu.setExpandRatio(layoutConfigMenu_spacer, 1.0F);
		this.buttonConfigEmployee.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigEmployee.setHeight(-1, Unit.PIXELS);
		this.layoutHumanResource.addComponent(this.buttonConfigEmployee);
		this.layoutHumanResource.setComponentAlignment(this.buttonConfigEmployee, Alignment.MIDDLE_CENTER);
		this.buttonConfigWorkingShift.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigWorkingShift.setHeight(-1, Unit.PIXELS);
		this.layoutHumanResource.addComponent(this.buttonConfigWorkingShift);
		this.layoutHumanResource.setComponentAlignment(this.buttonConfigWorkingShift, Alignment.MIDDLE_CENTER);
		CustomComponent layoutHumanResource_spacer = new CustomComponent();
		layoutHumanResource_spacer.setSizeFull();
		this.layoutHumanResource.addComponent(layoutHumanResource_spacer);
		this.layoutHumanResource.setExpandRatio(layoutHumanResource_spacer, 1.0F);
		this.buttonConfigDinerTable.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigDinerTable.setHeight(-1, Unit.PIXELS);
		this.layoutConfigOther.addComponent(this.buttonConfigDinerTable);
		this.layoutConfigOther.setComponentAlignment(this.buttonConfigDinerTable, Alignment.MIDDLE_CENTER);
		this.buttonConfigCertificate.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigCertificate.setHeight(-1, Unit.PIXELS);
		this.layoutConfigOther.addComponent(this.buttonConfigCertificate);
		this.layoutConfigOther.setComponentAlignment(this.buttonConfigCertificate, Alignment.MIDDLE_CENTER);
		this.buttonConfigTandS.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigTandS.setHeight(-1, Unit.PIXELS);
		this.layoutConfigOther.addComponent(this.buttonConfigTandS);
		this.layoutConfigOther.setComponentAlignment(this.buttonConfigTandS, Alignment.MIDDLE_CENTER);
		this.buttonConfigSystemOther.setWidth(100, Unit.PERCENTAGE);
		this.buttonConfigSystemOther.setHeight(-1, Unit.PIXELS);
		this.layoutConfigOther.addComponent(this.buttonConfigSystemOther);
		this.layoutConfigOther.setComponentAlignment(this.buttonConfigSystemOther, Alignment.MIDDLE_CENTER);
		CustomComponent layoutConfigOther_spacer = new CustomComponent();
		layoutConfigOther_spacer.setSizeFull();
		this.layoutConfigOther.addComponent(layoutConfigOther_spacer);
		this.layoutConfigOther.setExpandRatio(layoutConfigOther_spacer, 1.0F);
		this.layoutProfile.setSizeFull();
		this.accordionNavigation.addTab(this.layoutProfile, "Hồ sơ", null);
		this.layoutDiner.setSizeFull();
		this.accordionNavigation.addTab(this.layoutDiner, "Quán ăn", null);
		this.layoutReport.setSizeFull();
		this.accordionNavigation.addTab(this.layoutReport, "Báo cáo", null);
		this.layoutConfigMenu.setSizeFull();
		this.accordionNavigation.addTab(this.layoutConfigMenu, "Cấu hình Menu", null);
		this.layoutHumanResource.setSizeFull();
		this.accordionNavigation.addTab(this.layoutHumanResource, "Nhân sự", null);
		this.layoutConfigOther.setSizeFull();
		this.accordionNavigation.addTab(this.layoutConfigOther, "Cấu hình khác", null);
		this.accordionNavigation.setSelectedTab(this.layoutProfile);
		this.labelHelloUser.setSizeUndefined();
		this.userMenu.addComponent(this.labelHelloUser);
		this.userMenu.setComponentAlignment(this.labelHelloUser, Alignment.MIDDLE_CENTER);
		this.accordionNavigation.setWidth(100, Unit.PERCENTAGE);
		this.accordionNavigation.setHeight(-1, Unit.PIXELS);
		this.userMenu.addComponent(this.accordionNavigation);
		this.userMenu.setComponentAlignment(this.accordionNavigation, Alignment.TOP_CENTER);
		this.userMenu.setExpandRatio(this.accordionNavigation, 1.0F);
		this.buttonLogout.setSizeUndefined();
		this.userMenu.addComponent(this.buttonLogout);
		this.userMenu.setComponentAlignment(this.buttonLogout, Alignment.MIDDLE_CENTER);
		this.userMenu.setWidth(-1, Unit.PIXELS);
		this.userMenu.setHeight(100, Unit.PERCENTAGE);
		this.panelMenu.setContent(this.userMenu);
		logonLayout.addComponent(viewPlaceholder, XdevBorderLayout.Constraint.CENTER);
		panelMenu.setWidth(189, Unit.PIXELS);
		logonLayout.addComponent(panelMenu, XdevBorderLayout.Constraint.WEST);
		this.logonLayout.setSizeFull();
		this.setContent(this.logonLayout);
		this.setSizeFull();
	
		viewPlaceholder.addComponentAttachListener(event -> this.viewPlaceholder_componentAttachedToContainer(event));
		buttonMyInfo.addClickListener(event -> this.buttonMyInfo_buttonClick(event));
		buttonPOS.addClickListener(event -> this.buttonPOS_buttonClick(event));
		buttonKitchen.addClickListener(event -> this.buttonKitchen_buttonClick(event));
		buttonRevenueReport.addClickListener(event -> this.buttonRevenueReport_buttonClick(event));
		buttonConfigItem.addClickListener(event -> this.buttonConfigItem_buttonClick(event));
		buttonConfigItemCategory.addClickListener(event -> this.buttonConfigItemCategory_buttonClick(event));
		buttonConfigEmployee.addClickListener(event -> this.buttonConfigEmployee_buttonClick(event));
		buttonConfigWorkingShift.addClickListener(event -> this.buttonConfigWorkingShift_buttonClick(event));
		buttonConfigDinerTable.addClickListener(event -> this.buttonConfigDinerTable_buttonClick(event));
		buttonConfigCertificate.addClickListener(event -> this.buttonConfigCertificate_buttonClick(event));
		buttonConfigTandS.addClickListener(event -> this.buttonConfigTandS_buttonClick(event));
		buttonConfigSystemOther.addClickListener(event -> this.buttonConfigSystemOther_buttonClick(event));
		buttonLogout.addClickListener(event -> this.buttonLogout_buttonClick(event));
	} // </generated-code>

	// <generated-code name="variables">
	private XdevButton buttonMyInfo, buttonPOS, buttonKitchen, buttonRevenueReport, buttonConfigItem, buttonConfigItemCategory,
			buttonConfigEmployee, buttonConfigWorkingShift, buttonConfigDinerTable, buttonConfigCertificate,
			buttonConfigTandS, buttonConfigSystemOther, buttonLogout;
	private XdevAccordion accordionNavigation;
	private XdevBorderLayout logonLayout;
	private XdevGridLayout viewPlaceholder;
	private XdevPanel panelMenu;
	private XdevAuthenticationNavigator navigation;
	private XdevLabel labelHelloUser;
	private XdevVerticalLayout userMenu, layoutProfile, layoutDiner, layoutReport, layoutConfigMenu,
			layoutHumanResource, layoutConfigOther; // </generated-code>
	
}