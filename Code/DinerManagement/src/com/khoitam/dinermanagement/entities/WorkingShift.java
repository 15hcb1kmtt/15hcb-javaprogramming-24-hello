package com.khoitam.dinermanagement.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.WorkingShiftDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;

@Caption("{%code} - {%name}")
@Table(name = "WorkingShift")
@Entity
@DAO(daoClass = WorkingShiftDAO.class)
public class WorkingShift {

	private int id;
	private String code;
	private String name;
	private Date fromTime;
	private Date toTime;
	private List<EmployeeWorkingShift> employeeWorkingShifts;
	private List<EmployeeWorkingSchedule> employeeWorkingSchedules;
	private List<DinerSalesOrder> dinerSalesOrders;
	private Double Salary;

	public WorkingShift() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public String getName() {
		return name;
	}

	public void setName(String attribute) {
		this.name = attribute;
	}

	public Date getFromTime() {
		return fromTime;
	}

	public void setFromTime(Date attribute) {
		this.fromTime = attribute;
	}

	public Date getToTime() {
		return toTime;
	}

	public void setToTime(Date attribute) {
		this.toTime = attribute;
	}

	@OneToMany(mappedBy = "workingShift")
	public List<EmployeeWorkingShift> getEmployeeWorkingShifts() {
		return employeeWorkingShifts;
	}

	public void setEmployeeWorkingShifts(List<EmployeeWorkingShift> employeeWorkingShifts) {
		this.employeeWorkingShifts = employeeWorkingShifts;
	}

	public EmployeeWorkingShift addEmployeeWorkingShift(EmployeeWorkingShift employeeWorkingShift) {
	getEmployeeWorkingShifts().add(employeeWorkingShift);
	employeeWorkingShift.setWorkingShift(this);
	return employeeWorkingShift;
	}

	public EmployeeWorkingShift removeEmployeeWorkingShift(EmployeeWorkingShift employeeWorkingShift) {
	getEmployeeWorkingShifts().remove(employeeWorkingShift);
	employeeWorkingShift.setWorkingShift(null);
	return employeeWorkingShift;
	}

	@OneToMany(mappedBy = "workingShift")
	public List<EmployeeWorkingSchedule> getEmployeeWorkingSchedules() {
		return employeeWorkingSchedules;
	}

	public void setEmployeeWorkingSchedules(List<EmployeeWorkingSchedule> employeeWorkingSchedules) {
		this.employeeWorkingSchedules = employeeWorkingSchedules;
	}

	public EmployeeWorkingSchedule addEmployeeWorkingSchedule(EmployeeWorkingSchedule employeeWorkingSchedule) {
	getEmployeeWorkingSchedules().add(employeeWorkingSchedule);
	employeeWorkingSchedule.setWorkingShift(this);
	return employeeWorkingSchedule;
	}

	public EmployeeWorkingSchedule removeEmployeeWorkingSchedule(EmployeeWorkingSchedule employeeWorkingSchedule) {
	getEmployeeWorkingSchedules().remove(employeeWorkingSchedule);
	employeeWorkingSchedule.setWorkingShift(null);
	return employeeWorkingSchedule;
	}

	@OneToMany(mappedBy = "workingShift")
	public List<DinerSalesOrder> getDinerSalesOrders() {
		return dinerSalesOrders;
	}

	public void setDinerSalesOrders(List<DinerSalesOrder> dinerSalesOrders) {
		this.dinerSalesOrders = dinerSalesOrders;
	}

	public DinerSalesOrder addDinerSalesOrder(DinerSalesOrder dinerSalesOrder) {
	getDinerSalesOrders().add(dinerSalesOrder);
	dinerSalesOrder.setWorkingShift(this);
	return dinerSalesOrder;
	}

	public DinerSalesOrder removeDinerSalesOrder(DinerSalesOrder dinerSalesOrder) {
	getDinerSalesOrders().remove(dinerSalesOrder);
	dinerSalesOrder.setWorkingShift(null);
	return dinerSalesOrder;
	}

	@Caption("Lương tham khảo")
	public Double getSalary() {
		return Salary;
	}

	public void setSalary(Double attribute) {
		this.Salary = attribute;
	}

}
