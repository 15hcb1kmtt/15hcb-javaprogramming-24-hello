package com.khoitam.dinermanagement.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.khoitam.dinermanagement.business.enumeration.EmployeeWorkingScheduleStatusEnum;
import com.khoitam.dinermanagement.dal.EmployeeWorkingScheduleDAO;
import com.xdev.dal.DAO;

@Table(name = "EmployeeWorkingSchedule")
@Entity
@DAO(daoClass = EmployeeWorkingScheduleDAO.class)
public class EmployeeWorkingSchedule {

	private int id;
	private Employee employee;
	private Date date;
	private Integer lateInMinutes;
	private Double salary;
	private WorkingShift workingShift;
	private EmployeeWorkingScheduleStatusEnum status;

	public EmployeeWorkingSchedule() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee attribute) {
		this.employee = attribute;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date attribute) {
		this.date = attribute;
	}

	public Integer getLateInMinutes() {
		return lateInMinutes;
	}

	public void setLateInMinutes(Integer attribute) {
		this.lateInMinutes = attribute;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double attribute) {
		this.salary = attribute;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public WorkingShift getWorkingShift() {
		return workingShift;
	}

	public void setWorkingShift(WorkingShift attribute) {
		this.workingShift = attribute;
	}

	@Enumerated(EnumType.ORDINAL)
	public EmployeeWorkingScheduleStatusEnum getStatus() {
		return status;
	}

	public void setStatus(EmployeeWorkingScheduleStatusEnum attribute) {
		this.status = attribute;
	}

}
