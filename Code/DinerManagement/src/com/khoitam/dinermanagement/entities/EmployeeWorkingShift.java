package com.khoitam.dinermanagement.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.EmployeeWorkingShiftDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;

@Table(name = "EmployeeWorkingShift")
@Entity
@DAO(daoClass = EmployeeWorkingShiftDAO.class)
public class EmployeeWorkingShift {

	private int id;
	private Employee employee;
	private WorkingShift workingShift;
	private Double salary;

	public EmployeeWorkingShift() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee attribute) {
		this.employee = attribute;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	public WorkingShift getWorkingShift() {
		return workingShift;
	}

	public void setWorkingShift(WorkingShift attribute2) {
		this.workingShift = attribute2;
	}

	@Caption("Lương")
	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double attribute) {
		this.salary = attribute;
	}

}
