package com.khoitam.dinermanagement.entities;

import java.util.Collection;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.RoleDAO;
import com.xdev.dal.DAO;
import com.xdev.security.authorization.jpa.AuthorizationResource;
import com.xdev.security.authorization.jpa.AuthorizationRole;

@Entity
@DAO(daoClass = RoleDAO.class)
@Table(name = "Role")
public class Role implements AuthorizationRole {

	private String name;
	private Set<Resource> resources;
	private Set<Role> childRoles;
	private Set<Role> parentRoles;
	private Set<Employee> users;

	public Role() {
		super();
	}

	@Id
	@Column(unique = true, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "roleresourcenm", joinColumns = @JoinColumn(name = "role", nullable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "resource", nullable = false, updatable = false))
	public Set<Resource> getResources() {
		return resources;
	}

	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "rolerolenm", joinColumns = @JoinColumn(name = "parentrole", nullable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "childrole", nullable = false, updatable = false))
	public Set<Role> getChildRoles() {
		return childRoles;
	}

	public void setChildRoles(Set<Role> childRoles) {
		this.childRoles = childRoles;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "rolerolenm", joinColumns = @JoinColumn(name = "childrole", nullable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "parentrole", nullable = false, updatable = false))
	public Set<Role> getParentRoles() {
		return parentRoles;
	}

	public void setParentRoles(Set<Role> parentRoles) {
		this.parentRoles = parentRoles;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "userrolenm", joinColumns = @JoinColumn(name = "role", nullable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "user", nullable = false, updatable = false))
	public Set<Employee> getUsers() {
		return users;
	}

	public void setUsers(Set<Employee> users) {
		this.users = users;
	}

	@Override
	public String roleName() {
		return this.getName();
	}

	@Override
	public Collection<? extends AuthorizationResource> resources() {
		return this.getResources();
	}

	@Override
	public Collection<? extends AuthorizationRole> roles() {
		return this.getChildRoles();
	}

}
