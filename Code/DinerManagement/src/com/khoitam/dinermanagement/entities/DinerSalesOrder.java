package com.khoitam.dinermanagement.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.DinerSalesOrderDAO;
import com.xdev.dal.DAO;

@Table(name = "DinerSalesOrder")
@Entity
@DAO(daoClass = DinerSalesOrderDAO.class)
public class DinerSalesOrder {

	private int id;
	private String code;
	private Date issueDate;
	private Employee issueBy;
	private Double total;
	private String cancelReason;
	private Boolean isPaid;
	private Boolean isCancelled;
	private Integer numOfGuest;
	private DinerTable dinerTable;
	private WorkingShift workingShift;
	private List<DinerSalesOrderItem> dinerSalesOrderItemsForSalesOrder;

	public DinerSalesOrder() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date attribute) {
		this.issueDate = attribute;
	}

	@ManyToOne
	public Employee getIssueBy() {
		return issueBy;
	}

	public void setIssueBy(Employee attribute) {
		this.issueBy = attribute;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double attribute) {
		this.total = attribute;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String attribute) {
		this.cancelReason = attribute;
	}

	public Boolean getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(Boolean attribute) {
		this.isPaid = attribute;
	}
	
	public Boolean getIsCancelled() {
		return isCancelled;
	}

	public void setIsCancelled(Boolean attribute) {
		this.isCancelled = attribute;
	}

	public Integer getNumOfGuest() {
		return numOfGuest;
	}

	public void setNumOfGuest(Integer attribute) {
		this.numOfGuest = attribute;
	}

	@ManyToOne
	public DinerTable getDinerTable() {
		return dinerTable;
	}

	public void setDinerTable(DinerTable attribute) {
		this.dinerTable = attribute;
	}

	@ManyToOne
	public WorkingShift getWorkingShift() {
		return workingShift;
	}

	public void setWorkingShift(WorkingShift attribute) {
		this.workingShift = attribute;
	}

	@OneToMany(mappedBy = "salesOrder")
	public List<DinerSalesOrderItem> getDinerSalesOrderItemsForSalesOrder() {
		return dinerSalesOrderItemsForSalesOrder;
	}

	public void setDinerSalesOrderItemsForSalesOrder(List<DinerSalesOrderItem> dinerSalesOrderItemsForSalesOrder) {
		this.dinerSalesOrderItemsForSalesOrder = dinerSalesOrderItemsForSalesOrder;
	}

	public DinerSalesOrderItem addDinerSalesOrderItemForSalesOrder(DinerSalesOrderItem dinerSalesOrderItemForSalesOrder) {
	getDinerSalesOrderItemsForSalesOrder().add(dinerSalesOrderItemForSalesOrder);
	dinerSalesOrderItemForSalesOrder.setSalesOrder(this);
	return dinerSalesOrderItemForSalesOrder;
	}

	public DinerSalesOrderItem removeDinerSalesOrderItemForSalesOrder(DinerSalesOrderItem dinerSalesOrderItemForSalesOrder) {
	getDinerSalesOrderItemsForSalesOrder().remove(dinerSalesOrderItemForSalesOrder);
	dinerSalesOrderItemForSalesOrder.setSalesOrder(null);
	return dinerSalesOrderItemForSalesOrder;
	}

	public void generateCode(String tableCode) {
		Date currentTime = new Date();
		String code = tableCode + "-";
		code += new SimpleDateFormat("ddMMyyyykms").format(currentTime);
		this.setCode(code);
	}
	
	public DinerSalesOrderItem addItem(DinerItem item, double quantity) {
		Date issueDate = new Date();
		DinerSalesOrderItem newItem = new DinerSalesOrderItem();
		newItem.setIssueDate(issueDate);
		newItem.setItem(item);
		newItem.setQuantity(quantity);
		newItem.setIssueDate(issueDate);
		newItem.setPrice(quantity * item.getPrice());
		newItem.setSalesOrder(this);
		return newItem;
	}
}
