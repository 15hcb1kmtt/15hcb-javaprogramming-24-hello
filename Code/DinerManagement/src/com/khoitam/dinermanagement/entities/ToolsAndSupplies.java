package com.khoitam.dinermanagement.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.ToolsAndSuppliesDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;

@Caption("{%code} - {%name}")
@Table(name = "ToolsAndSupplies")
@Entity
@DAO(daoClass = ToolsAndSuppliesDAO.class)
public class ToolsAndSupplies {

	private int id;
	private String code;
	private String name;
	private List<EmployeeToolsAndSupplies> employeeToolsAndSupplies;

	public ToolsAndSupplies() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false, unique = true)
	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public String getName() {
		return name;
	}

	public void setName(String attribute) {
		this.name = attribute;
	}

	@OneToMany(mappedBy = "toolsAndSupplies")
	public List<EmployeeToolsAndSupplies> getEmployeeToolsAndSupplies() {
		return employeeToolsAndSupplies;
	}

	public void setEmployeeToolsAndSupplies(List<EmployeeToolsAndSupplies> employeeToolsAndSupplies) {
		this.employeeToolsAndSupplies = employeeToolsAndSupplies;
	}

	public EmployeeToolsAndSupplies addEmployeeToolsAndSupplies(EmployeeToolsAndSupplies employeeToolsAndSupplies) {
	getEmployeeToolsAndSupplies().add(employeeToolsAndSupplies);
	employeeToolsAndSupplies.setToolsAndSupplies(this);
	return employeeToolsAndSupplies;
	}

	public EmployeeToolsAndSupplies removeEmployeeToolsAndSupplies(EmployeeToolsAndSupplies employeeToolsAndSupplies) {
	getEmployeeToolsAndSupplies().remove(employeeToolsAndSupplies);
	employeeToolsAndSupplies.setToolsAndSupplies(null);
	return employeeToolsAndSupplies;
	}

}
