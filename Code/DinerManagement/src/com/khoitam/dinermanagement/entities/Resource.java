package com.khoitam.dinermanagement.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.ResourceDAO;
import com.xdev.dal.DAO;
import com.xdev.security.authorization.jpa.AuthorizationResource;

@Table(name = "Resource")
@Entity
@DAO(daoClass = ResourceDAO.class)
public class Resource implements AuthorizationResource {

	private String name;
	private Set<Role> roles;

	public Resource() {
		super();
	}

	@Id
	@Column(unique = true, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "roleresourcenm", joinColumns = @JoinColumn(name = "resource", nullable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "role", nullable = false, updatable = false))
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String resourceName() {
		return this.getName();
	}

}
