package com.khoitam.dinermanagement.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.EmployeeCertificateDAO;
import com.xdev.dal.DAO;

@Table(name = "EmployeeCertificate")
@Entity
@DAO(daoClass = EmployeeCertificateDAO.class)
public class EmployeeCertificate {

	private int id;
	private Employee employee;
	private Certificate certificate;

	public EmployeeCertificate() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee attribute) {
		this.employee = attribute;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Certificate getCertificate() {
		return certificate;
	}

	public void setCertificate(Certificate attribute) {
		this.certificate = attribute;
	}

}
