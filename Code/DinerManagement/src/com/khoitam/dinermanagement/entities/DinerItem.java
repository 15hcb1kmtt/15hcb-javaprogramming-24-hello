package com.khoitam.dinermanagement.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.DinerItemDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;


@Table(name = "DinerItem")
@Caption("{%code} - {%name}")
@Entity
@DAO(daoClass = DinerItemDAO.class)
public class DinerItem {

	private int id;
	private String code;
	private String name;
	private Double price;
	private Boolean outOfOrder;
	private DinerItemCategory category;
	private List<DinerSalesOrderItem> dinerSalesOrderItemsForItem;
	private Boolean isCookable;

	public DinerItem() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public String getName() {
		return name;
	}

	public void setName(String attribute) {
		this.name = attribute;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double attribute) {
		this.price = attribute;
	}

	public Boolean getOutOfOrder() {
		return outOfOrder;
	}

	public void setOutOfOrder(Boolean attribute) {
		this.outOfOrder = attribute;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public DinerItemCategory getCategory() {
		return category;
	}

	public void setCategory(DinerItemCategory attribute) {
		this.category = attribute;
	}

	@OneToMany(mappedBy = "item")
	public List<DinerSalesOrderItem> getDinerSalesOrderItemsForItem() {
		return dinerSalesOrderItemsForItem;
	}

	public void setDinerSalesOrderItemsForItem(List<DinerSalesOrderItem> dinerSalesOrderItemsForItem) {
		this.dinerSalesOrderItemsForItem = dinerSalesOrderItemsForItem;
	}

	public DinerSalesOrderItem addDinerSalesOrderItemForItem(DinerSalesOrderItem dinerSalesOrderItemForItem) {
	getDinerSalesOrderItemsForItem().add(dinerSalesOrderItemForItem);
	dinerSalesOrderItemForItem.setItem(this);
	return dinerSalesOrderItemForItem;
	}

	public DinerSalesOrderItem removeDinerSalesOrderItemForItem(DinerSalesOrderItem dinerSalesOrderItemForItem) {
	getDinerSalesOrderItemsForItem().remove(dinerSalesOrderItemForItem);
	dinerSalesOrderItemForItem.setItem(null);
	return dinerSalesOrderItemForItem;
	}

	public Boolean getIsCookable() {
		return isCookable;
	}

	public void setIsCookable(Boolean attribute) {
		this.isCookable = attribute;
	}

}
