package com.khoitam.dinermanagement.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.CertificateDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;

@Caption("{%code} - {%name}")
@Table(name = "Certificate")
@Entity
@DAO(daoClass = CertificateDAO.class)
public class Certificate {

	private int id;
	private String code;
	private String name;
	private List<EmployeeCertificate> employeeCertificates;

	public Certificate() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public String getName() {
		return name;
	}

	public void setName(String attribute) {
		this.name = attribute;
	}

	@OneToMany(mappedBy = "certificate")
	public List<EmployeeCertificate> getEmployeeCertificates() {
		return employeeCertificates;
	}

	public void setEmployeeCertificates(List<EmployeeCertificate> employeeCertificates) {
		this.employeeCertificates = employeeCertificates;
	}

	public EmployeeCertificate addEmployeeCertificate(EmployeeCertificate employeeCertificate) {
	getEmployeeCertificates().add(employeeCertificate);
	employeeCertificate.setCertificate(this);
	return employeeCertificate;
	}

	public EmployeeCertificate removeEmployeeCertificate(EmployeeCertificate employeeCertificate) {
	getEmployeeCertificates().remove(employeeCertificate);
	employeeCertificate.setCertificate(null);
	return employeeCertificate;
	}

}
