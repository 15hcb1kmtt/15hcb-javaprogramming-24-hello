package com.khoitam.dinermanagement.entities;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.business.enumeration.EmployeeStatusEnum;
import com.khoitam.dinermanagement.dal.EmployeeDAO;
import com.xdev.dal.DAO;
import com.xdev.security.authentication.CredentialsUsernamePassword;
import com.xdev.security.authorization.jpa.AuthorizationRole;
import com.xdev.security.authorization.jpa.AuthorizationSubject;
import com.xdev.util.Caption;

@Caption("{%userName}")
@Table(name = "Employee")
@Entity
@DAO(daoClass = EmployeeDAO.class)
public class Employee implements CredentialsUsernamePassword, AuthorizationSubject {

	private int id;
	private String code;
	private String fullName;
	private String userName;
	private String password;
	private Date birthday;
	private String phone;
	private String address;
	private String idNumber;
	private EmployeeStatusEnum status;
	private Set<EmployeeToolsAndSupplies> employeeToolsAndSupplies;
	private Set<EmployeeWorkingShift> employeeWorkingShifts;
	private Employee manageBy;
	private Set<Employee> employeesForManageBy;
	private Set<EmployeeCertificate> employeeCertificates;
	private Set<EmployeeWorkingSchedule> employeeWorkingSchedules;
	private Set<DinerSalesOrder> dinerSalesOrdersForIssueBy;
	private Set<Role> roles;
	public Employee() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String attribute) {
		this.fullName = attribute;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String attribute) {
		this.userName = attribute;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String attribute) {
		this.password = attribute;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date attribute) {
		this.birthday = attribute;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String attribute) {
		this.phone = attribute;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String attribute) {
		this.address = attribute;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String attribute) {
		this.idNumber = attribute;
	}

	@Enumerated(EnumType.STRING)
	public EmployeeStatusEnum getStatus() {
		return status;
	}

	public void setStatus(EmployeeStatusEnum attribute) {
		this.status = attribute;
	}

	@OneToMany(mappedBy = "employee")
	public Set<EmployeeToolsAndSupplies> getEmployeeToolsAndSupplies() {
		return employeeToolsAndSupplies;
	}

	public void setEmployeeToolsAndSupplies(Set<EmployeeToolsAndSupplies> employeeToolsAndSupplies) {
		this.employeeToolsAndSupplies = employeeToolsAndSupplies;
	}

	public EmployeeToolsAndSupplies addEmployeeToolsAndSupplies(EmployeeToolsAndSupplies employeeToolsAndSupplies) {
	getEmployeeToolsAndSupplies().add(employeeToolsAndSupplies);
	employeeToolsAndSupplies.setEmployee(this);
	return employeeToolsAndSupplies;
	}

	public EmployeeToolsAndSupplies removeEmployeeToolsAndSupplies(EmployeeToolsAndSupplies employeeToolsAndSupplies) {
	getEmployeeToolsAndSupplies().remove(employeeToolsAndSupplies);
	employeeToolsAndSupplies.setEmployee(null);
	return employeeToolsAndSupplies;
	}

	@OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
	public Set<EmployeeWorkingShift> getEmployeeWorkingShifts() {
		return employeeWorkingShifts;
	}

	public void setEmployeeWorkingShifts(Set<EmployeeWorkingShift> employeeWorkingShifts) {
		this.employeeWorkingShifts = employeeWorkingShifts;
	}

	public EmployeeWorkingShift addEmployeeWorkingShift(EmployeeWorkingShift employeeWorkingShift) {
	getEmployeeWorkingShifts().add(employeeWorkingShift);
	employeeWorkingShift.setEmployee(this);
	return employeeWorkingShift;
	}

	public EmployeeWorkingShift removeEmployeeWorkingShift(EmployeeWorkingShift employeeWorkingShift) {
	getEmployeeWorkingShifts().remove(employeeWorkingShift);
	employeeWorkingShift.setEmployee(null);
	return employeeWorkingShift;
	}

	@ManyToOne
	public Employee getManageBy() {
		return manageBy;
	}

	public void setManageBy(Employee attribute) {
		this.manageBy = attribute;
	}

	@OneToMany(mappedBy = "manageBy")
	public Set<Employee> getEmployeesForManageBy() {
		return employeesForManageBy;
	}

	public void setEmployeesForManageBy(Set<Employee> employeesForManageBy) {
		this.employeesForManageBy = employeesForManageBy;
	}

	public Employee addEmployeeForManageBy(Employee employeeForManageBy) {
	getEmployeesForManageBy().add(employeeForManageBy);
	employeeForManageBy.setManageBy(this);
	return employeeForManageBy;
	}

	public Employee removeEmployeeForManageBy(Employee employeeForManageBy) {
	getEmployeesForManageBy().remove(employeeForManageBy);
	employeeForManageBy.setManageBy(null);
	return employeeForManageBy;
	}

	@OneToMany(mappedBy = "employee")
	public Set<EmployeeCertificate> getEmployeeCertificates() {
		return employeeCertificates;
	}

	public void setEmployeeCertificates(Set<EmployeeCertificate> employeeCertificates) {
		this.employeeCertificates = employeeCertificates;
	}

	public EmployeeCertificate addEmployeeCertificate(EmployeeCertificate employeeCertificate) {
	getEmployeeCertificates().add(employeeCertificate);
	employeeCertificate.setEmployee(this);
	return employeeCertificate;
	}

	public EmployeeCertificate removeEmployeeCertificate(EmployeeCertificate employeeCertificate) {
	getEmployeeCertificates().remove(employeeCertificate);
	employeeCertificate.setEmployee(null);
	return employeeCertificate;
	}

	@OneToMany(mappedBy = "employee")
	public Set<EmployeeWorkingSchedule> getEmployeeWorkingSchedules() {
		return employeeWorkingSchedules;
	}

	public void setEmployeeWorkingSchedules(Set<EmployeeWorkingSchedule> employeeWorkingSchedules) {
		this.employeeWorkingSchedules = employeeWorkingSchedules;
	}

	public EmployeeWorkingSchedule addEmployeeWorkingSchedule(EmployeeWorkingSchedule employeeWorkingSchedule) {
	getEmployeeWorkingSchedules().add(employeeWorkingSchedule);
	employeeWorkingSchedule.setEmployee(this);
	return employeeWorkingSchedule;
	}

	public EmployeeWorkingSchedule removeEmployeeWorkingSchedule(EmployeeWorkingSchedule employeeWorkingSchedule) {
	getEmployeeWorkingSchedules().remove(employeeWorkingSchedule);
	employeeWorkingSchedule.setEmployee(null);
	return employeeWorkingSchedule;
	}

	@OneToMany(mappedBy = "issueBy")
	public Set<DinerSalesOrder> getDinerSalesOrdersForIssueBy() {
		return dinerSalesOrdersForIssueBy;
	}

	public void setDinerSalesOrdersForIssueBy(Set<DinerSalesOrder> dinerSalesOrdersForIssueBy) {
		this.dinerSalesOrdersForIssueBy = dinerSalesOrdersForIssueBy;
	}

	public DinerSalesOrder addDinerSalesOrderForIssueBy(DinerSalesOrder dinerSalesOrderForIssueBy) {
	getDinerSalesOrdersForIssueBy().add(dinerSalesOrderForIssueBy);
	dinerSalesOrderForIssueBy.setIssueBy(this);
	return dinerSalesOrderForIssueBy;
	}

	public DinerSalesOrder removeDinerSalesOrderForIssueBy(DinerSalesOrder dinerSalesOrderForIssueBy) {
	getDinerSalesOrdersForIssueBy().remove(dinerSalesOrderForIssueBy);
	dinerSalesOrderForIssueBy.setIssueBy(null);
	return dinerSalesOrderForIssueBy;
	}

	@Override
	public String username() {
		return this.getUserName();
	}

	@Override
	public byte[] password() throws RuntimeException {
		try {
			return this.getPassword().getBytes("ISO-8859-1");
		} catch (final UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "userrolenm", joinColumns = @JoinColumn(name = "user", nullable = false, updatable = false), inverseJoinColumns = @JoinColumn(name = "role", nullable = false, updatable = false))
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String subjectName() {
		return this.getUserName();
	}

	@Override
	public Collection<? extends AuthorizationRole> roles() {
		return this.getRoles();
	}
	
    public String hash256(String data) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        try {
			md.update(data.getBytes("ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
        
        byte byteData[] = md.digest();
        
        String s = new String(byteData, Charset.forName("ISO-8859-1"));
  
        return s;
    }
}
