package com.khoitam.dinermanagement.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.DinerTableDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;

@Caption("{%code} - {%name}")
@Table(name = "DinerTable")
@Entity
@DAO(daoClass = DinerTableDAO.class)
public class DinerTable {

	private int id;
	private String code;
	private String name;
	private Integer maximumGuest;
	private List<DinerSalesOrder> dinerSalesOrders;

	public DinerTable() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(unique = true, nullable = false)
	public String getCode() {
		return code;
	}

	public void setCode(String attribute) {
		this.code = attribute;
	}

	public String getName() {
		return name;
	}

	public void setName(String attribute) {
		this.name = attribute;
	}

	public Integer getMaximumGuest() {
		return maximumGuest;
	}

	public void setMaximumGuest(Integer attribute) {
		this.maximumGuest = attribute;
	}

	@OneToMany(mappedBy = "dinerTable")
	public List<DinerSalesOrder> getDinerSalesOrders() {
		return dinerSalesOrders;
	}

	public void setDinerSalesOrders(List<DinerSalesOrder> dinerSalesOrders) {
		this.dinerSalesOrders = dinerSalesOrders;
	}

	public DinerSalesOrder addDinerSalesOrder(DinerSalesOrder dinerSalesOrder) {
	getDinerSalesOrders().add(dinerSalesOrder);
	dinerSalesOrder.setDinerTable(this);
	return dinerSalesOrder;
	}

	public DinerSalesOrder removeDinerSalesOrder(DinerSalesOrder dinerSalesOrder) {
	getDinerSalesOrders().remove(dinerSalesOrder);
	dinerSalesOrder.setDinerTable(null);
	return dinerSalesOrder;
	}

}
