package com.khoitam.dinermanagement.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.khoitam.dinermanagement.business.enumeration.DinerSalesOrderItemStatusEnum;
import com.khoitam.dinermanagement.dal.DinerSalesOrderItemDAO;
import com.xdev.dal.DAO;

@Table(name = "DinerSalesOrderItem")
@Entity
@DAO(daoClass = DinerSalesOrderItemDAO.class)
public class DinerSalesOrderItem {

	private int id;
	private DinerItem item;
	private Double quantity;
	private Double price;
	private Date issueDate;
	private String cancelReason;
	private DinerSalesOrder salesOrder;
	private DinerSalesOrderItemStatusEnum status;

	public DinerSalesOrderItem() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	public DinerItem getItem() {
		return item;
	}

	public void setItem(DinerItem attribute) {
		this.item = attribute;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double attribute) {
		this.quantity = attribute;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double attribute) {
		this.price = attribute;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date attribute) {
		this.issueDate = attribute;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String attribute) {
		this.cancelReason = attribute;
	}

	@ManyToOne
	public DinerSalesOrder getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(DinerSalesOrder attribute) {
		this.salesOrder = attribute;
	}

	@Enumerated(EnumType.ORDINAL)
	public DinerSalesOrderItemStatusEnum getStatus() {
		return status;
	}

	public void setStatus(DinerSalesOrderItemStatusEnum attribute) {
		this.status = attribute;
	}

}
