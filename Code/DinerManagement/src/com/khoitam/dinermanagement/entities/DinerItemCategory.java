package com.khoitam.dinermanagement.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.DinerItemCategoryDAO;
import com.xdev.dal.DAO;
import com.xdev.util.Caption;

@Table(name = "DinerItemCategory")
@Caption("{%code} - {%name}")
@Entity
@DAO(daoClass = DinerItemCategoryDAO.class)
public class DinerItemCategory {

	private int id;
	private String Code;
	private String name;
	private Integer menuDisplayOrder;
	private List<DinerItem> dinerItemsForCategory;

	public DinerItemCategory() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Caption("Code")
	@Column(nullable = false, unique = true)
	public String getCode() {
		return Code;
	}

	public void setCode(String attribute) {
		this.Code = attribute;
	}

	@Caption("Name")
	public String getName() {
		return name;
	}

	public void setName(String attribute) {
		this.name = attribute;
	}

	@Caption("Menu Display Order")
	public Integer getMenuDisplayOrder() {
		return menuDisplayOrder;
	}

	public void setMenuDisplayOrder(Integer attribute) {
		this.menuDisplayOrder = attribute;
	}

	@OneToMany(mappedBy = "category")
	public List<DinerItem> getDinerItemsForCategory() {
		return dinerItemsForCategory;
	}

	public void setDinerItemsForCategory(List<DinerItem> dinerItemsForCategory) {
		this.dinerItemsForCategory = dinerItemsForCategory;
	}

	public DinerItem addDinerItemForCategory(DinerItem dinerItemForCategory) {
	getDinerItemsForCategory().add(dinerItemForCategory);
	dinerItemForCategory.setCategory(this);
	return dinerItemForCategory;
	}

	public DinerItem removeDinerItemForCategory(DinerItem dinerItemForCategory) {
	getDinerItemsForCategory().remove(dinerItemForCategory);
	dinerItemForCategory.setCategory(null);
	return dinerItemForCategory;
	}

	@Override
	public String toString() {
		return Code + " - " + name;
	}
}
