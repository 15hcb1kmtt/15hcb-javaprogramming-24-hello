package com.khoitam.dinermanagement.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.khoitam.dinermanagement.dal.EmployeeToolsAndSuppliesDAO;
import com.xdev.dal.DAO;

@Table(name = "EmployeeToolsAndSupplies")
@Entity
@DAO(daoClass = EmployeeToolsAndSuppliesDAO.class)
public class EmployeeToolsAndSupplies {

	private int id;
	private Employee employee;
	private ToolsAndSupplies toolsAndSupplies;
	private Boolean returned;

	public EmployeeToolsAndSupplies() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee attribute) {
		this.employee = attribute;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public ToolsAndSupplies getToolsAndSupplies() {
		return toolsAndSupplies;
	}

	public void setToolsAndSupplies(ToolsAndSupplies attribute) {
		this.toolsAndSupplies = attribute;
	}

	public Boolean getReturned() {
		return returned;
	}

	public void setReturned(Boolean attribute) {
		this.returned = attribute;
	}

}
