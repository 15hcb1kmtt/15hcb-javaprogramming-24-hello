
package com.khoitam.dinermanagement.business;

import com.khoitam.dinermanagement.entities.Employee;
import com.khoitam.dinermanagement.entities.Resource;
import com.khoitam.dinermanagement.entities.Role;
import com.xdev.security.authorization.AuthorizationConfiguration;
import com.xdev.security.authorization.AuthorizationConfigurationProvider;
import com.xdev.security.authorization.jpa.JPAAuthorizationConfigurationProvider;

public class DinerAuthorizationConfigurationProvider implements AuthorizationConfigurationProvider {
	private static DinerAuthorizationConfigurationProvider INSTANCE;

	public static DinerAuthorizationConfigurationProvider getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DinerAuthorizationConfigurationProvider();
		}

		return INSTANCE;
	}

	private JPAAuthorizationConfigurationProvider provider;

	private DinerAuthorizationConfigurationProvider() {
	}

	@Override
	public AuthorizationConfiguration provideConfiguration() {
		if (this.provider == null) {
			this.provider = new JPAAuthorizationConfigurationProvider(Employee.class, Role.class, Resource.class);
		}

		return this.provider.provideConfiguration();
	}
}
