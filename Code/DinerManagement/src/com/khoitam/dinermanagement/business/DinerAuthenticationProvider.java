
package com.khoitam.dinermanagement.business;

import com.khoitam.dinermanagement.entities.Employee;
import com.xdev.security.authentication.Authenticator;
import com.xdev.security.authentication.AuthenticatorProvider;
import com.xdev.security.authentication.CredentialsUsernamePassword;
import com.xdev.security.authentication.jpa.JPAAuthenticator;
import com.xdev.security.authentication.jpa.HashStrategy.SHA2;

public class DinerAuthenticationProvider
		implements AuthenticatorProvider<CredentialsUsernamePassword, CredentialsUsernamePassword> {
	private static DinerAuthenticationProvider INSTANCE;

	public static DinerAuthenticationProvider getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DinerAuthenticationProvider();
		}

		return INSTANCE;
	}

	private JPAAuthenticator authenticator;

	private DinerAuthenticationProvider() {
	}

	@Override
	public Authenticator<CredentialsUsernamePassword, CredentialsUsernamePassword> provideAuthenticator() {
		if (this.authenticator == null) {
			this.authenticator = new JPAAuthenticator(Employee.class);
			this.authenticator.setHashStrategy(new SHA2());
		}

		return this.authenticator;
	}
}
