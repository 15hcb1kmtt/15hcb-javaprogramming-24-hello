package com.khoitam.dinermanagement.business.personinfo;

import java.util.Date;
import java.util.UUID;

public class EmployeeScheduleData {
	public EmployeeScheduleData() {
		id = java.util.UUID.randomUUID();
	}
	
	private UUID id;
	private Date date;
	private String fromTime;
	private String toTime;
	private int employeeId;
	private String employeeName;
	private int shiftId;
	private String shiftName;
	private String status;
	private int scheduleId;
	private Double salary;
	
	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public UUID getId() {
		return id;
	}
	
	public String getShiftName() {
		return shiftName;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	
	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public int getShiftId() {
		return shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}