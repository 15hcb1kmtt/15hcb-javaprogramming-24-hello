package com.khoitam.dinermanagement.business.enumeration;

public enum DinerSalesOrderItemStatusEnum {
	Ordered,
	WaitingForCooking,
	Cooking,
	Cooked,
	Delivered,
	Canceled
}
