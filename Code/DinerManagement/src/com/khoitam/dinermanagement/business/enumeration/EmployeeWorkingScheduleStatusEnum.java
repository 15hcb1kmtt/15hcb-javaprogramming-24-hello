package com.khoitam.dinermanagement.business.enumeration;

public enum EmployeeWorkingScheduleStatusEnum {
	DoneWorking,
	AskForAbsent,
	ApprovedAbsent,
	Absent
}
